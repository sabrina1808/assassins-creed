<h2>Assassin's Creed III</h2>

<section class="contexte">
    <h3>Contexte</h3>
    <img src="<?= ASSETS ?>img/logo/Logo AC III.png" alt="Logo AC III">
    <p>L’action se déroule pendant la guerre d’indépendance des futurs États-Unis (1775 – 1783), sur le sol nord-américain, dans le conflit où les colons et envoyés de la Couronne britannique se livrent des batailles rangées et où l’on incarne un Assassin amérindien.</p>
</section>

<section class="perso">
    <h3>Personnages Principaux</h3>
    <article>
        <h4>Ratonhnhaké :ton (Connor)</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC III - Connor.png" alt="Connor">
            <div>
                <p><strong>Dates :</strong> 1756 – inconnu</p>
                <p><strong>Origine :</strong> Vallée iroquoise, Amérique du Nord</p>
                <p><strong>Période Historique :</strong> Amérique coloniale, révolution américaine</p>
                <p><strong>Guilde :</strong> Confrérie coloniale</p>
            </div>
        </div>
        <p>Fils d’Haytham Kenway, un Templier britannique, et de Kaniehtí :io, Ratonhnhaké :ton pensait que les Templiers étaient responsables de la destruction de son village et de la mort de sa mère pendant la Guerre de Sept Ans.</p>
        <p>Adolescent, il découvrit un Fragment d’Éden, une Boule de Cristal qui déclencha une vision dans laquelle il se vit s’élancer comme un aigle et qui lui commanda de se mettre à la recherche d’un certain symbole. La vision le persuada que son village était un site sacré qui devait être protégé coûte que coûte et Ratonhnhaké :ton se jura d’empêcher les Templiers de dominer l’Amérique du Nord.</p>
        <p>À la recherche du symbole que lui avait indiqué la vision, Ratonhnhaké :ton retrouva un ancien chef Assassin, Achilles, et insista pour qu’il le forme afin qu’il puisse combattre les Templiers et libérer l’Amérique du Nord de leur emprise. En 1770, Ratonhnhaké :ton termina sa formation et devenant Assassin, se rebaptisa Connor.</p>
        <p>Connor fit tout ce qu’il put pour débarrasser la jeune nation des Templiers et soutint notamment les Patriotes dans leur révolution contre les Britanniques. Il entretient une relation trouble avec George Washington et œuvra à ses côtés jusqu’à ce qu’il réalise amèrement que le traitement que les Patriotes réservaient aux Indiens. Il proposa à son père, Haytham Kenway, Grand Maître du Rite colonial, de s’allier avec lui, en espérant que leurs différents pourraient être mis de côté le temps de servir les meilleurs intérêts de leur nation. Au final, des divergences fondamentales conduisirent Connor à assassiner son propre père.</p>
        <p>Connor s’assura que la révolution garantirait au peuple des droits préservant sa liberté, loin du contrôle des Templiers, mais il réalisa que ses efforts avaient été vains, puisque ces mêmes droits ne profitaient pas aux minorités et aux indigènes. Son adhésion au Crédo permit à une majorité de gagner sa liberté, au détriment d’une minorité.</p>
        <p>Bien qu’Abstergo ait tenté de discréditer Connor en le décrivant comme un raté, il vécut un mariage heureux avec une Indienne qui lui donna trois enfants. Bien que sa tribu le désapprouve, Connor décida de former sa fille cadette, Io :nhiòte au pistage et à la chasse, car il perçut en elle le même genre de perception sensorielle que la sienne.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> George Washington, Benjamin Franklin, Achilles Davenport, Samuel Adams et le Marquis de Lafayette</p>
            <p><strong>Ennemis principaux :</strong> Haytham Kenway et Charles Lee</p>
            <q>J’ai pour ennemi une notion, pas une nation.</q>
            <cite> - Ratonhnhaké :ton (Connor)</cite>
        </div>
    </article>

    <article>
        <h4>Haytham Kenway</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC III - Haytham Kenway.png" alt="Haytham Kenway">
            <div>
                <p><strong>Dates :</strong> 1725 – 1781</p>
                <p><strong>Lieu de naissance :</strong> Londres</p>
                <p><strong>Activité :</strong> Amérique britannique</p>
                <p><strong>Période Historique :</strong> Amérique coloniale, révolution américaine</p>
                <p><strong>Guilde :</strong> Rite colonial</p>
            </div>
        </div>
        <p>Haytham fut élevé par son père, l’Assassin Edward Kenway, pour penser librement et remettre en question les opinions des autres. Mais son éducation fut complétée par Reginald Birch, un ami de la famille, qui attendit le meurtre d’Edward pour lui dévoiler qu’il était Templier. Haytham s’immergea alors dans l’enseignement des Templiers et  fut réconforté par leur manière de voir le monde. Il fut intronisé à l’Ordre en 1744.</p>
        <p>En 1754, il fut envoyé en Amérique, dans les colonies britanniques, pour localiser un supposé dépôt des Précurseurs. Là-bas, il fonda le Rite colonial, installant la première présence durable de l’Ordre en Amérique du Nord. Haytham développa une relation avec la population native locale, qui lui permit de localiser le site des Précurseurs qu’il cherchait grâce à l’aide de Kaniethí :io, une femme Kanien’kehá :ka qui lui offrit un fils. Quand Kaniethí :io découvrit qu’il lui avait menti en lui assurant qu’il avait tué Edward Braddock à la fin de son expédition en 1755, elle le répudia et lui cacha l’existence de son fils.</p>
        <p>Incapable d’accéder au site des Précurseurs et désormais détaché des Indiens, Haytham chercha à renforcer la présence des Templiers  dans les colonies. Sous son règne, le Rite colonial s’étendit considérablement et la guilde naissante des Assassins fut quasiment éradiquée.</p>
        <p>En 1778, il croisa Ratonhnhaké :ton, alias Connor, qu’il reconnut comme son fils. Les années suivantes, alors que les colonies luttaient pour gagner leur indépendance de l’Angleterre, le père et le fils tentèrent de s’allier, réalisant qu’ils partageaient les mêmes objectifs. Malgré leur mésentente concernant les tactiques et les chefs à suivre, Haytham et Connor collaborèrent sur plusieurs missions importantes pendant la Guerre d’indépendance, essayant chacun de rallier l’autre à sa cause. Finalement, les dissensions furent trop importantes et les deux s’affrontèrent à Fort George, où Haytham fut tué par son propre fils.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Reginald Birch, Charles Lee et Shay Cormac</p>
            <p><strong>Ennemis principaux :</strong> Achilles Davenport et Ratonhnhaké :ton (Connor)</p>
            <q>Le peuple ne possède jamais le pouvoir, seulement son illusion. Et voilà le véritable secret : ils n’en veulent pas. La responsabilité est trop lourde à porter. C’est pour cela qu’ils sont si prompts à se mettre en ligne dès que quelqu’un s’en charge. Ils VEULENT qu’on leur dise quoi faire. Ils N’ATTENDENT que ça. Ça n’a rien d’étonnant, étant donné que l’Humanité a été FONDÉE pour SERVIR</q>
            <cite> - Haytham Kenway</cite>
        </div>
    </article>
</section>