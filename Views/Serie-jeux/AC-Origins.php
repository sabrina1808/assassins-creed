<h2>Assassin's Creed Origins</h2>

<section class="contexte">
    <h3>Contexte</h3>
    <img src="<?= ASSETS ?>img/logo/Logo AC Origins.png" alt="Logo AC Origins">
    <p>Dans le présent, Layla Hassan, une employée d’Abstergo, est envoyée à la recherche d’artefacts en Egypte, aidée à distance par sa collègue et amie Deanna Geary. Elle découvre les momies de deux précurseurs des Assassins et décide d’explorer leurs mémoires génétiques.</p>
    <p>Le jeu prend ainsi place en Egypte antique, en 49 avant. J-C. On suit ainsi les mémoires de Bayek, le dernier Medjaÿ de Siwa et fondateur de la Confrérie des Assassins, qui souhaite se venger des personnes qui ont tué son fils.</p>
</section>

<section class="perso">
    <h3>Personnages Principaux</h3>
    <article>
        <h4>Bayek et Aya de Siwa</h4>
        <div class="info origins">
            <img src="<?= ASSETS ?>img/perso/AC Origins - Bayek.png" alt="Bayek de Siwa">
            <img src="<?= ASSETS ?>img/perso/AC Origins - Aya.png" alt="Aya de Siwa">
            <div>
                <p><strong>Dates :</strong> Vers 80 av. JC - inconnu</p>
                <p><strong>Lieu :</strong> Égypte, Rome</p>
                <p><strong>Période Historique :</strong> Égypte et Rome antique</p>
                <p><strong>Affiliation :</strong> « Ceux qu’on ne voit pas »</p>
            </div>
        </div>
        <p>Les principes et l’éthique de Bayek et Aya donnèrent lieu à la création d’une organisation dédiée à la justice : « Ceux que l’on ne voit pas ». Cette organisation évolua plus tard pour donner naissance à la Confrérie des Assassins.</p>
        <p>Égyptien berbère né et élevé à Siwa, une oasis reculée près de la frontière égypto-libanaise, Bayek fut le dernier des medjaÿ, des protecteurs de l’ancien royaume d’Égypte, ses temples, son peuple et ses coutumes. Il épousa Aya d’Alexandrie, qui fut formée à l’art des medjaÿ elle aussi, bien qu’elle n’ait pas été descendante de medjaÿ elle-même.</p>
        <p>Quand les soldats exigèrent l’accès au temple sacré de Siwa, le refus de Bayek provoqua le meurtre de leur jeune fils. Leur tristesse poussa Bayek et Aya à se séparer. Intérieurement, Bayek se persuada qu’en tuant chacun des bourreaux de son fils, il pourrait le ressusciter, et retrouver Aya afin qu’ils reprennent la vie qu’ils avaient, à trois. La mission qu’il s’était donnée le ramena malgré lui vers Aya, à Alexandrie et elle le persuada de voir plus loin que sa vengeance pour se concentrer sur un but bien plus important. Aya était pour sa part convaincue que Cléopâtre était la clé de la libération de l’Égypte, et elle était prête à tout pour lui faire regagner son trône. Bayek accepta de la rejoindre au service de Cléopâtre et se mit à pourchasse l’Ordre des Anciens. Cependant, au fil de sa mission, il réalisa que le peuple d’Égypte était en droit de disposer de lui-même et de choisir sa voie. Ils avaient besoin d’un héraut pour combattre l’oppression croissante causée par un régime injuste. Sa détermination première s’effaça pour laisser placer à un combat pour faire prévaloir le Bien.</p>
        <p>Mais lorsque Cléopâtre décida de s’allier à César – secrètement allié de l’Ordre des Anciens – et de se séparer d’Aya et de Bayek, Aya se sentit trahit. Elle n’accepta pas cette traitrise envers les valeurs qui lui étaient chères : la sûreté et la liberté de l’Égypte et de son peuple. Malgré la douleur, Aya resta fidèle aux objectifs qu’elle s’était fixés. Elle continuerait de combattre les dictateurs, les tyrans et les manipulateurs de tous bords. Et s’il fallait pour cela aller contre les alliés de Cléopâtre, ainsi soit-il.</p>
        <p>Ayant tout donné pour Cléopâtre et leur combat contre l’Ordre des Anciens, Bayek et Aya définirent précisément le sens de leur lutte – liberté du peuple, résistance contre la dictature – en tirant des objectifs concrets. Ils savaient qu’en poursuivant qu’en poursuivant une quête personnelle, ils aideraient aussi plus largement l’Égypte en frappant le cœur de l’Ordre des Anciens.</p>
        <p>Bayek et Aya fondèrent une nouvelle organisation basée sur des principes plutôt qu’une lignée strictement familiale. Une organisation qui pourrait perdurer pendant des siècles… « Ceux qu’on ne voit pas », l’organisation qui deviendrait plus tard la Confrérie des Assassins. Aya décida de changer de nom en se baptisant Amunet, et fut reconnue par la Confrérie comme l’une de ses plus célèbres membres.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Phoxidas, Brutus, Cassius, Hepzeta, Apollodorus, Tahira</p>
            <p><strong>Ennemis principaux :</strong> L’Ordre des anciens, Jules César, Flavius Metellus, Lucius Septimius, Martellus, Bion</p>
            <q>Nos principes doivent être transmis à ceux qui prendront notre relève afin qu’ils puissent s’appuyer sur un vrai Crédo.</q>
            <cite> - Amunet</cite>
            <br>
            <q>« Ceux qu’on ne voit pas » le transmettront à Petra et en Judée. À jamais. Le Crédo doit survivre à ceux qui le portent.</q>
            <cite> - Bayek</cite>
        </div>
    </article>

    <article>
        <h4>Flavius Metellus – Le Lion</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC Origins - Flavius.png" alt="Flavius Metellus">
            <div>
                <p><strong>Dates :</strong> Vers 90 av. JC – 47 av. JC</p>
                <p><strong>Lieu :</strong> Cyrène, Cyrénaïque</p>
                <p><strong>Période Historique :</strong> Période lagide et romaine de l’Egypte</p>
                <p><strong>Affiliation :</strong> Ordre des Anciens</p>
            </div>
        </div>
        <p>Flavius était un puissant soldat romain au bras long et à la carrière distinguée. Bien qu’il fût l’un des alliés de César, leur alliance s’était bâtie sur la richesse et la force de Flavius plus que sur la confiance, l’honnêteté et le respect. Flavius savait que César avait besoin de lui et il utilisait lui-même l’empereur pour monter en grade.</p>
        <p>Flavius était extrêmement ambitieux – personnellement comme politiquement – et il tirait parti de toutes les opportunités possibles pour arriver à ses fins. Il vit en les Égyptiens une bonne ressource à exploiter – pour Rome et pour lui-même. Il avait la réputation d’être quelqu’un d’aussi brutal qu’impitoyable et il trouvait que César était trop indulgent avec ses ennemis.</p>
        <p>Flavius devint proconsul de la province romaine de Cyrénaïque. Il était aussi le chef de l’Ordre des Anciens pendant le règne de Cléopâtre. Avec Lucius Septimius, il utilisa le Bâton d’Éden du tombeau d’Alexandre combiné avec une Pomme d’Éden pour ouvrir le Sanctuaire sous le Temple d’Amon, à Siwa. C’est là que Bayek l’assassina, vengeant ainsi la mort de son fils, Khemu, victime de Flavius des années auparavant.</p>
        <div class="citation">
            <q>Voilà une chose que je ne regrette pas dans ma vie. La mort de ton fils a forcé l’Ordre à se prosterner devant moi. Même César. Rome m’appartenait.</q>
            <cite> - Flavius Metellus</cite>
        </div>
    </article>
</section>