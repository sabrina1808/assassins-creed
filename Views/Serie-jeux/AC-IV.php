<h2>Assassin's Creed IV - Black Flag</h2>

<section class="contexte">
    <h3>Contexte</h3>
    <img src="<?= ASSETS ?>img/logo/Logo AC Black Flag.png" alt="Logo AC IV Black Flag">
    <p>Dans le présent, le joueur incarne désormais un chercheur d’Abstergo Entertainment chargé d’explorer la mémoire génétique du pirate et Assassin Edward Kenway – père d’Haytham Kenway, grand-père de Connor, rencontrés dans le précédent opus.</p>
    <p>C’est histoire d’Edward Kenway, un jeune britannique qui a soif de danger et d’aventure. Pirate féroce et combattant aguerri, sa recherche de gloire et de fortune lui fait gagner le respect des pirates légendaires mais derrière la guerre entre Britanniques et Espagnols, entre Empires coloniaux et Pirates, il est plongé aussi au cœur de la guerre antique entre Assassins et Templiers. Se déroulant à l’aube du XVIIIe siècle, le jeu met en scène les plus légendaire pirates de l’Histoire, tels que Barbe Noire et Charles Vane, et emmène les joueurs dans un périple à travers les Antilles pendant une période turbulente et violente qui sera connue plus tard comme l’Âge d’or de la piraterie (1715 – 1725).</p>
</section>

<section class="perso">
    <h3>Personnages Principaux</h3>
    <article>
        <h4>Edward Kenway</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC Black Flag - Edward Kenway.png" alt="Edward Kenway">
            <div>
                <p><strong>Dates :</strong> 1693 – 1735</p>
                <p><strong>Origine :</strong> Swansea, Pays de Galles</p>
                <p><strong>Activité :</strong> Caraïbes</p>
                <p><strong>Période Historique :</strong> Age d’Or de la Piraterie</p>
                <p><strong>Guilde :</strong> Confrérie britannique</p>
            </div>
        </div>
        <p>Flibustier gallois cherchant la gloire et la richesse, Edward Kenway découvrit l’existence des Assassins après s’être fait passer pour Duncan Walpole, un Assassin qu’il avait tué. En travaillant avec la Confrérie, Edward utilisa son savoir-faire et son navire, le <em>JackDaw</em>, pour empêcher les Templiers espagnols et britanniques de mettre un terme à la piraterie dans les Caraïbes.</p>
        <p>Ce faisant, il découvrit que les Templiers cherchaient à localiser l’Observatoire, un site des Précurseurs, et le pirate Bartholomew Roberts, incarnation actuelle du Précurseur Aita. Edward finit par trouver l’Observatoire.</p>
        <p>Alors qu’il collaborait avec les Assassins, Edward adopta leur philosophie et leurs armes. Il ne rejoint véritablement la Confrérie que bien plus tard, après avoir perdu tout ce qui lui était cher. Il décida de devenir Assassin pour se racheter de toutes ses mauvaises actions passées.</p>
        <p>Après avoir été pardonné de ses actes de pirateries, Edward put revenir en Grande-Bretagne où il devint co-responsable de la Confrérie britannique. Son entreprise lui permit de couvrir son activité pour les Assassins, et notamment la découverte de plusieurs Temples Précurseurs qui lui offrirent de nombreuses informations concernant le Grand Temple. Il découvrit le Suaire d’Éden, un Fragment d’Éden si puissant que Kenway décida de le dissimuler aux Templiers mais aussi aux Assassins.</p>
        <p>Edward entraîna son fils, Haytham, à devenir Assassin dès son plus jeune âge. Mais l’ancien pirate fini par se faire assassiner par des Templiers qui s’introduisirent chez lui et kidnappèrent sa fille, Jenny.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Adéwalé, Barbe Noire, James Kidd/Mary Read, Benjamin Hornigold, Caliko Jack et Ah Tabai</p>
            <p><strong>Ennemis principaux :</strong> Bartholomew Roberts, Laureano de Torres y Ayala, Julien du Casse et Woodes Rogers</p>
            <q>Ce n'est pas facile de voir en moi un ami, pas vrai ?</q>
            <cite> - Edward Kenway</cite>
        </div>
    </article>

    <article>
        <h4>Laureano de Torres y Ayala</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC Black Flag - Laureano Torres.png" alt="Laureano Torres">
            <div>
                <p><strong>Dates :</strong> 1645 – 1722</p>
                <p><strong>Lieu de naissance :</strong> Havane, Cuba</p>
                <p><strong>Activité :</strong> Floride, Cuba</p>
                <p><strong>Période Historique :</strong> Age d’Or de la Piraterie</p>
                <p><strong>Guilde :</strong> Rite des Indes Occidentales</p>
            </div>
        </div>
        <p>Élevé en Espagne, Torres rejoignit les Templiers alors qu’il avait une vingtaine d’années et fonda le Rite des Indes Occidentales. Il fut missionné pour localiser le site des Précurseurs connu sous le nom de l’Observatoire. Cette mission l’occupa pendant plus de 20 ans.</p>
        <p>Avec l’aide des Templiers, il fut nommé gouverneur des territoires espagnoles en Floride en 1693 et resta en poste jusqu’en 1699. En 1708, il fut nommée gouverneur de Cuba jusqu’à être accusé de corruption en 1711. Acquitté et réintégré en 1713, il remporta les élections deux ans plus tard et prit sa retraite en 1716.</p>
        <p>Torres intronisa malgré lui, Edward Kenway au sein de l’Ordre des Templiers en étant convaincu qu’il s’agissait du traître Assassin Duncan Walpole. Quand il découvrit son erreur, il condamna Kenway à la galère.</p>
        <p>En vue de s’installer dans l’Observatoire, Torres força tous les Templiers sous son commandement à verser une goutte de sang qui lui permettrait de les suivre et les espionner grâce à la technologie du site des Précurseurs.</p>
        <p>Torres finit par localiser le site en 1722. Il ordonna à ses troupes de massacrer la tribu des Gardiens qui protégeaient l’Observatoire, et parvint à accéder au centre du Temple. Après avoir enclenché le système de défense du site, il se fit tuer par Edward Kenway.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> El Tiburón</p>
            <p><strong>Ennemis principaux :</strong> Edward Kenway et Adéwalé</p>
            <q>Un corps soumis en esclavage inspire la révolte. Mais soumettez un esprit, et le corps suivra naturellement. Efficacement.</q>
            <cite> - Laureano de Torres y Ayala</cite>
        </div>
    </article>
</section>