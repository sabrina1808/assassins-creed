<h2>Assassin's Creed Syndicate</h2>

<section class="contexte">
    <h3>Contexte</h3>
    <img src="<?= ASSETS ?>img/logo/Logo AC Syndicate.png" alt="Logo AC Syndicate">
    <p>L’intrigue se déroule dans le Londres des années 1860, où Jacob et Evie Frye arrivent à Londres dans le but de vaincre  le templier Crawford Starrick et son organisation, qui contrôle divers rouages importants de la société londonienne (usines, transports, médecine, politique, criminalité, ...). Ils sont assistés par Henry Green, le Grand Maître Assassin de la Confrérie de Londres. Mais le frère et la sœur sont divisés quant à la manière de procéder. Jacob souhaite libérer la ville et former son gang. Quant à Evie, elle est à la recherche d’un fragment d’Eden caché dans la ville.</p>
</section>

<section class="perso">
    <h3>Personnages Principaux</h3>
    <article>
        <h4>Jacob et Evie Frye</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC Syndicate - Les jumeaux.png" alt="Jacob et Evie Frye">
            <div>
                <p><strong>Dates :</strong> 1847 – inconnu</p>
                <p><strong>Activité :</strong> 1847 – inconnu</p>
                <p><strong>Période Historique :</strong> Ère victorienne</p>
                <p><strong>Guilde :</strong> Confrérie britannique</p>
            </div>
        </div>
        <p>Enfants des Assassins Cecily et Ethan Frye, Jacob et Evie furent formés dès leur plus jeune âge. Evie écouta consciencieusement les enseignements de son père, mais Jacob était moins convaincu de l’efficacité d’une approche furtive. En tant qu’Assassins, le frère et la sœur appliquèrent des méthodes bien distinctes à leur approche. Evie se concentrait sur la recherche et la furtivité, convaincue qu’il était plus important de chercher les Fragments d’Éden que de s’opposer activement aux Templiers. De son côté, Jacob sautait sur toutes les occasions qui se présentaient à lui sans plus de réflexion. Les jumeaux formaient ainsi une équipe très complémentaire.</p>
        <p>Après avoir déménagé à Londres sur les conseils de leur Mentor, ils rencontrèrent l’Assassin Henry Green, et se lancèrent d’eux-mêmes dans l’éradication du contrôle des Templiers sur la ville.</p>
        <p>Bien qu’ils furent assignés à des missions différentes, Jacob et Evie réussirent à libérer Londres de l’emprise séculaire des Templiers en travaillant de concert, et en sauvant la reine Victoria. Les jumeaux furent faits chevaliers et leur succès permit à la Confrérie britannique de se redresser.</p>
        <p>Evie partit pour l’Inde après avoir épousé Henry Green et commença à travailler pour la Confrérie indienne. De passage à Londres en 1872, Evie et Henry tentèrent d’aider Frederick Abberline à empêcher – en vain – les Templiers de voler les pages du Manuscrit de Voynich au British Museum. Ils retournèrent ensuite en Inde où ils enseignèrent à plusieurs Assassins britanniques les méthodes de la Confrérie indienne.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Henry Green (né Jayadeep Mir), Alexander Graham Bell et Frederick Abberline</p>
            <p><strong>Ennemis principaux :</strong> Crawford Starrick et Lucy Thorne</p>
            <q>Ah, encore une bonne soirée au coin du feu pour Evie Frye</q>
            <cite> - Jacob Frye</cite>
            <br>
            <q>A vrai dire, j’allais sortir, j’ai trouvé le Fragment d’Éden.</q>
            <cite> - Evie Frye</cite>
        </div>
    </article>

    <article>
        <h4>Crawford Starrick</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC Syndicate - Crawford Starrick.png" alt="Crawford Starrick">
            <div>
                <p><strong>Dates :</strong> 1827 – 1868</p>
                <p><strong>Lieu de naissance :</strong> Londres</p>
                <p><strong>Activité :</strong> Londres</p>
                <p><strong>Période Historique :</strong> Ère victorienne</p>
                <p><strong>Guilde :</strong> Rite britannique</p>
            </div>
        </div>
        <p>Conservateur au plus haut point, Crawford Starrick ne vivait que pour l’ordre et le contrôle. Il bâtit un empire industriel et assura sa mainmise sur Londres en éliminant la concurrence. Son réseau de Templiers œuvrant pour opprimer les classes populaires, trop affaiblies pour trouver la motivation de lutter contre leurs inflexibles maîtres.</p>
        <p>Starrick ignora l’action des Assassins Jacob et Evie Frye ainsi que celle de leur confrère, Henry Green. Il était, en effet, convaincu qu’elle serait inefficace face à l’omniprésence des Templiers britanniques à tous les niveaux de la société londonienne. Cependant, quand Jacob élimina sa cousine, Pearl Attaway, Starrick décida d’éliminer les jumeaux du décor. Peu de temps après, Lucy Thorne, sa seconde, fut assassiné par Evie Frye, renforçant ainsi la détermination de celui qui était désormais bien décidé à défendre son contrôle sur Londres.</p>
        <p>Starrick infiltra Buckingham Palace pendant une fête afin de mettre la main sur le Suaire d’Éden renfermé dans une crypte sous le palais. Il assassina les responsables de l’Église et de l’État qui étaient présents. Il accéda au Suaire et s’enveloppa dedans en utilisant ses pouvoirs contre les jumeaux Frye. Mais ils réussirent malgré tout à maîtriser puis à tuer Starrick quand Evie lui retira le Suaire des épaules.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Lucy Thorne</p>
            <p><strong>Ennemis principaux :</strong> Jacob et Evie Frye</p>
            <q>Gentlemen. Ce thé m’a été rapporté d’Inde par bateau, puis des docks à l’usine, où il a été empaqueté et livré à ma porte, dépaqueté dans le garde-manger et remonté pour m’être servi. Tout ça par des hommes et des femmes à mon service. Qui ont une dette envers moi, Crawford Starrick, pour leur travail, leur temps et la vie qu’ils mènent. Ils travailleront dans mes usines, et leurs enfants après eux.</q>
            <cite> - Crawford Starrick</cite>
        </div>
    </article>

    <article>
        <h4>Lucy Thorne</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC Syndicate - Lucy Thorne.png" alt="Lucy Thorne">
            <div>
                <p><strong>Dates :</strong> 1837 – 1868</p>
                <p><strong>Lieu de naissance :</strong> Londres</p>
                <p><strong>Activité :</strong> Londres</p>
                <p><strong>Période Historique :</strong> Ère victorienne</p>
                <p><strong>Guilde :</strong> Rite britannique</p>
            </div>
        </div>
        <p>Dès son plus jeune âge, Lucy Thorne se plongea dans l’étude de l’occulte, de la philosophie, de la magie et des religions obscures. En grandissant, elle commença à collectionner les livres rares et dépensait sans compter lors des ventes aux enchères de papiers anciens. C’est lors de l’une de ces ventes qu’elle croisa Crawford Starrick, qui admira sa passion et son dévouement. Il la présenta à l’Ordre des Templiers, et une fois intronisée, elle devient sa seconde.</p>
        <p>Sa connaissance des objets occultes et mystiques la guida naturellement vers l’étude des Fragments d’Éden et d’autres artefacts des Précurseurs. Elle concentra tous ces efforts sur la localisation du Suaire d’ Éden et prit ses quartiers chez les Kenway.</p>
        <p>Ses recherches lui permirent d’acquérir de nombreux documents liés à l’histoire des Assassins qu’elle fit envoyer à Londres. Mais les Frye interceptèrent le colis. Bien qu’elle dut laisser derrière elle un bonne partie de l’inestimable collection, Evie Frye réussit à sauver un carnet dont la disparition fit particulièrement enrager Lucy Thorne. Evie utilisa les indices qu’il contenait pour s’infiltrer chez les Kenway et localisa une pièce secrète qui contenait des souvenirs d’Edward Kenway. Lucy arriva trop tard pour y accéder : Evie l’avait déjà scellée.</p>
        <p>Celle-ci prit encore une longueur d’avance en obtenant la clé qui lui permettrait d’ouvrir le Sanctuaire qui contenait le Suaire, clé qui se trouvait au sommet de la Cathédrale Saint-Paul. Lucy réussit à la lui arracher et se rendit à la Tour de Londres, où elle pensait qu’était le Fragment tant convoité. Evie Frye le retrouva là-bas et l’assassinat lors d’un ultime affrontement.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Crawford Starrick</p>
            <p><strong>Ennemis principaux :</strong> Evie Frye</p>
            <q>Cela devra se dérouler sans l’ombre. Miss Frye sera suspendue à la potence et j’écorcherai son frère quand il viendra la sauver.</q>
            <cite> - Lucy Thorne</cite>
        </div>
    </article>
</section>