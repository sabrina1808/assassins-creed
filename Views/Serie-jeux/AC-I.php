<h2>Assassin's Creed I</h2>

<section class="contexte">
    <h3>Contexte</h3>
    <img src="<?= ASSETS ?>img/logo/Logo AC.png" alt="Logo AC">
    <p>Dans le présent, Desmond Miles est retenu dans les laboratoires d’Abstergo Industries, qui développe le projet Animus, une machine capable de lire et de décoder la mémoire génétique pour accéder à la mémoire d’un des aïeux de la personne qui se trouve dans la machine, et donc aux souvenirs de cet ancêtre.</p>
    <p>Dans le passé, Altaïr Ibn La-Ahad, membre de la Confrérie des Assassins, évoluant en Palestine à l’époque de la Troisième Croisade (1189 – 1192). Il parcourt les villes de Damas, Jérusalem, Masyaf et Acre. Son objectif est d’assassiner neuf personnages capitaux de l’histoire de la Troisième Croisade.</p>
</section>

<section class="perso">
    <h3>Personnages Principaux</h3>
    <article>
        <h4>Altaïr Ibn La-Ahad</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC I - Altair.png" alt="Altaïr Ibn La-Ahad">
            <div>
                <p><strong>Dates :</strong> 1165 – 1257</p>
                <p><strong>Origine :</strong> Masyaf, Syrie</p>
                <p><strong>Période Historique :</strong> Troisième Croisade</p>
                <p><strong>Guilde :</strong> Confrérie du Levant</p>
            </div>
        </div>
        <p>Altaïr est né dans une famille d’Assassins et fut nommé Maître Assassin à 25 ans, faisant de lui le plus jeune Assassin à atteindre ce rang – à l’époque. Lors de la Troisième Croisade, Altaïr fut envoyé à Jérusalem pour récupérer la Pomme d’Eden – surnommé de Trésor des Templiers – qui s’y trouvait.</p>
        <p>Après avoir trahi les Trois Principes du Crédo et exposé la communauté des Assassins de Masyaf à une attaque des Templiers, désireux de retrouver leur Pomme, Altaïr fut relégué au rang de Novice et dut s’amender en effectuant une série de destinées à empêcher les Chevaliers du Temple de prendre le contrôle de la Terre-Sainte en liguant les Sarrasins et les Croisés les uns contre les autres.</p>
        <p>C’est alors qu’il découvrit que son Mentor, Al Mualim, s’était allié aux Templiers pour utiliser la Pomme à ses propres fins. Altaïr assassina alors Al Mualim et récupéra l’artefact qui lui révéla l’emplacement d’autres Fragments d’Éden dans le monde. Il lui communiqua aussi des informations propres aux Précurseurs et l’avertit du danger représenté par la Seconde Catastrophe.</p>
        <p>Plus âgé et plus sage, Altaïr devint le Mentor de la Confrérie du Levant et reforma l’organisation, la rendant moins isolée et plus égalitaire. C’est pour répandre la parole des Assassins autour du monde qu’il prit la direction de la Mongolie avec sa femme Maria et son fils Darim, afin d’interrompre la folle conquête de Gengis Khan. Si Altaïr fut blessé lors de l’assaut du camp de Gengis Khan, Darim réussit quant à lui à assassiner le tyran d’un tir d’arbalète. De retour à Masyaf, le nouveau Mentor de la Confrérie repensa l’équipement des Assassins en étudiant la Pomme. Il écrivit le Codex, un journal codé qui rassemblait des notes et des réflexions accumulées aux cours de sa vie, et notamment les connaissances acquises grâce à son Fragment d’Éden.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Maria Thorpe (femme), Darim et Sef (fils) et Malik Al-Sayf</p>
            <p><strong>Ennemis principaux :</strong> Robert de Sablé, Abbas Sofian, Al Mualim et Gengis Khan</p>
            <q>Il n’est pas de plus grande gloire que de se battre pour exhumer la vérité.</q>
            <cite> - Altaïr Ibn La-Ahad</cite>
        </div>
    </article>

    <article>
        <h4>Robert de Sablé</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC I - Robert de Sable.png" alt="Robert de Sablé">
            <div>
                <p><strong>Dates :</strong> 1150 – 1191</p>
                <p><strong>Lieu de naissance :</strong> Angevin, France</p>
                <p><strong>Activité :</strong> Jérusalem, Terre Sainte</p>
                <p><strong>Période Historique :</strong> Troisième Croisade</p>
                <p><strong>Guilde :</strong> Rite levantin</p>
            </div>
        </div>
        <p>Issu de la noblesse, Robert de Sablé gouvernait la région de l’Anjou. En 1190, il fut nommé Grand Maître de l’Ordre des Chevaliers du Temple, alors qu’il était proche conseillé du roi Richard Ier  et de son armée de Croisés. Son objectif était de prendre le contrôle de la Terre Sainte afin de permettre aux Templiers de s’y installer. Il rallia de nombreux Sarrasins et Croisés à la cause des Templiers et ils œuvrèrent de concert contre la Confrérie des Assassins.</p>
        <p>En cherchant à localiser une Pomme d’Éden, de Sablé découvrit avant Altaïr le Sanctuaire sous le Temple de Salomon, mais c’est l’Assassin qui repartit avec la Pomme. Il pourchassa son adversaire jusqu’au bastion de Masyaf où il fut aidé par un traître à la Confrérie qui ouvrit l’enceinte de la forteresse à ses troupes.</p>
        <p>Robert de Sablé périt dans le duel contre Altaïr, organisé par le roi Richard pour déterminer quel camp méritait son soutien. Avant de mourir, il dévoila à son adversaire la trahison d’Al Mualim. La mort de Robert de Sablé mit un terme aux projets des Templiers de contrôler la Terre Sainte.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Roi Richard Ier et Saladin</p>
            <p><strong>Ennemis principaux :</strong> Altaïr Ibn-La’Ahad</p>
            <q>Tu ne sais point où tu t’es engouffré Assassin. Je t’épargne uniquement pour que tu puisses retourner auprès de ton Maître et lui livré un message : la Terre Sainte n’appartient plus ni à lui ni aux siens. Il devrait s’enfuir tant qu’il en a l’occasion. Restez, et vous mourrez tous.</q>
            <cite> - Robert de Sablé</cite>
        </div>
    </article>
</section>