<h2>Assassin's Creed Odyssey</h2>

<section class="contexte">
    <h3>Contexte</h3>
    <img src="<?= ASSETS ?>img/logo/Logo AC Odyssey.png" alt="Logo AC Odyssey">
    <p>Dans le présent, on suit toujours Layla Hassan, un membre des Assassins qui explore la mémoire des premiers Assassins.</p>
    <p>Le jeu prend place pendant la guerre du Péloponnèse (431 – 404 avant JC) , soit 382 ans avant les évènements relatés dans Origins et est centrée sur la première civilisation. Le joueur incarne l’un des descendants de Léonidas, Kassandra ou Alexios, un misthios (un mercenaire) qui pourra combattre pour Athènes et la Ligue de Délos ou pour la Ligue du Péloponnèse dirigée par Sparte.</p>
</section>

<section class="perso">
    <h3>Personnages Principaux</h3>
    <article>
        <h4>Kassandra</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC Odyssey - Kassandra.png" alt="Kassandra">
            <div>
                <p><strong>Dates :</strong> Vers 475 av. JC – 2018</p>
                <p><strong>Lieu :</strong> Grèce</p>
                <p><strong>Période Historique :</strong> Âge d’Or d’Athènes, Guerre du Péloponnèse</p>
                <p><strong>Affiliation :</strong> Misthios</p>
            </div>
        </div>
        <p>Kassandra était une descendante de Léonidas Ier, roi guerrier de Sparte. Elle fut élevée dans la maison de Nikolaos, un général spartiate, et formée au combat comme un homme. Le petit frère de Kassandra, Alexios, naquit alors qu’elle avait 7 ans. Lors d’un pèlerinage familial à Delphes pour connaître la destinée du nouveau-né, l’Oracle décréta que l’enfant était maudit et qu’il plongerait le monde dans les ténèbres.</p>
        <p>Suivant l’avertissement de l’Oracle, le Spartiate Ephors déclara que l’enfant représentait une menace et ordonna qu’on le jette des falaises du Mont Taygète. En bon spartiate, Nikolaos se plia aux ordres, mais Kassandra se démena pour sauver son frère. Ce faisant, elle poussa un Spartiate plus âgé de la falaise et fut à son tour jetée de la falaise en guise de peine. Elle survécut miraculeusement et s’échappa pour s’installer sur Kephallonia, une île de parias et de marginaux où elle fut forcée de se battre pour sa survie. Privée de sa jeunesse, Kassandra grandit seule en s’entrainant pour devenir une mercenaire : une <em>misthios</em>.</p>
        <p>Détentrice d’un Fragment d’Éden légendaire, la Lance de Léonidas, Kassandra plongea dans la guerre du Péloponnèse, assistant frontalement au combat entre le Vieux Monde représenté par des idéaux spartiates brutaux et agressifs et un avenir éveillé représenté par la morale athénienne progressiste, démocratique et intellectuelle. Guidée par la volonté de retrouver les membres de sa famille, Kassandra découvrit l’existence du Culte de Kosmos, une société secrète qui soutenait un mode de vie chaotique et encourageait la population à craindre les dieux et le progrès. Le héraut du Culte, Deimos, avait été formé pour devenir leur bras armé contre ceux qui voudraient contrecarrer leurs plans. Le Culte de Kosmos utilisait un artefact des Précurseurs en forme de pyramide, qui leur offrait les visions de différents avenirs potentiels et leur permettait de manipuler l’Oracle de Delphes, insufflant la peur dans le cœur de ceux qui venait lui demander conseil et asseyant ainsi leur domination.</p>
        <p>Pythagore dévoila à Kassandra qu’Alexios et elle étaient destinés à perpétuer la lignée des Isus. En l’absence d’Alexios, Kassandra devint la tenante de la science des Isus conservée dans l’Atlantide et fut chargée de réunir différents artefacts afin de les étudier. Pythagore légua le Bâton d’Hermès à Kassandra, lui permettant ainsi de vivre assez longtemps pour s’assurer que l’équilibre entre l’ordre et le chaos soit bien conservé dans le monde.</p>
        <p>Son activité ayant précédé la fondation de la Confrérie, Kassandra est une proto-Assassine. Elle croyait néanmoins à la famille, au libre-arbitre et au fait que l’humanité méritait de prendre ses propres décisions plutôt que de subir le contrôle de quelques individus. Ces valeurs étant proches de celles qui dirigeraient la Confrérie des siècles plus tard.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Myrrine, Socrate, Hérodote, Barnabas et Ikaros</p>
            <p><strong>Ennemis principaux :</strong> Le Culte de Kosmos et Deimos</p>
            <q>On m’a jeté de cette montagne. Ma vie commence et finit sur cette montagne.</q>
            <cite> - Kassandra</cite>
        </div>
    </article>

    <article>
        <h4>Deimos</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC Odyssey - Deimos.png" alt="Deimos">
            <div>
                <p><strong>Dates :</strong> 451 av. JC</p>
                <p><strong>Lieu :</strong> Grèce</p>
                <p><strong>Période Historique :</strong> Âge d’Or d’Athènes, Guerre du Péloponnèse</p>
                <p><strong>Affiliation :</strong> Culte de Kosmos</p>
            </div>
        </div>
        <p>Enlevé a Myrrine sur l’Argolide alors qu’il était encore un bébé, à la suite d’une prophétie annonçant que l’enfant mènerait Sparte à la perte, Alexios fut élevé par le Culte de Kosmos et rebaptisé Deimos, l’incarnation grecque de la terreur. Le Culte le forma pour qu’il devienne le guerrier le plus puissant de toute la Grèce et qu’il serve leur cause.</p>
        <p>Le Culte de Kosmos avait besoin de quelqu’un possédant un niveau élevé d’ADN Isu pour utiliser la Pyramide, un Fragment d’Éden en leur possession. Pour arriver à  leurs fins, ils créèrent une prophétie qui le condamnerait à être jeté du haut d’une montagne. Ils le recueillirent alors et le petit Alexios fut confié à Chrysis. Il devient Deimos, leur bras armé et leur interface avec la Pyramide. Ainsi, le Culte était libre d’accéder aux différents futurs qui s’offraient à lui et de choisir celui qui conviendrait le mieux à ses plans.</p>
        <p>Deimos était assoiffé de pouvoir et pensait être un demi-dieu. Le Culte le persuada qu’il avait été abandonné par sa famille afin qu’il puisse accomplir son destin et devenir le plus puissant guerrier du monde grec. Bien qu’il ait été un membre du Culte de Kosmos, les cultistes commencèrent à trembler en sa présence, car au contact de Kassandra et de Myrrine, sa confiance en l’organisation commença à s’émousser.</p>
        <div class="citation">
            <q>Mon épée est ma famille. Le champ de bataille est ma demeure.</q>
            <cite> - Deimos</cite>
        </div>
    </article>
</section>