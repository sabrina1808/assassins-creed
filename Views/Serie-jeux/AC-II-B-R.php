<h2>Assassin's Creed II, Brotherhood, Révélations</h2>

<section class="contexte">
    <h3>Contexte</h3>
    <img src="<?= ASSETS ?>img/logo/Logo AC.png" alt="Logo AC">
    <h4>Assassin's Creed II</h4>
    <p>Desmond Miles se retrouve maintenant en Italie durant le Renaissance Italienne. Cette époque est l’aube d’une nouvelle ère où de nouveaux courants de pensées, tels que l’Humanisme commencent à voir le jour, mais aussi où les agitations politiques et les violentes rivalités familiales font rage.</p>
    <h4>Assassin's Creed Brotherhood</h4>
    <p>Suite directe d’Assassin’s Creed II, le jeu reprend l’histoire d’Ezio Auditore da Firenze, devenu un légendaire Maître Assassin, dans son combat face au règne tyrannique de la famille Borgia à Rome, ouvrant ainsi la ville aux richesses de la Renaissance.</p>
    <h4>Assassin's Creed Révélation</h4>
    <p>Suite d’Assassin’s Creed Brotherhood, cet épisode conclut la trilogie de jeu consacré à Ezio. Dans cet épisode, on retrouve donc le Maître Assassin Ezio Auditore da Firenze à la recherche des secrets du Grand Maître Assassin Altaïr Ibn La-Ahad se situant dans la bibliothèque de la forteresse de Masyaf. Cependant, il découvre que l’entrée de celle-ci est scellée à l’aide de 5 clés qui ont été cachées par Niccolo Polo dans la ville de Constantinople. Il se rend donc dans cette ville, alors capitale de l’Empire Byzantin pour les trouver. Afin de déchiffrer les écrits, Ezio fera appel à Sofia Sartor, une libraire d’origine italienne.</p>
</section>

<section class="perso">
    <h3>Personnages Principaux</h3>
    <article>
        <h4>Ezio Auditore da Firenze</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC II - Ezio Auditore.png" alt="Ezio Auditore">
            <div>
                <p><strong>Dates :</strong> 1459 – 1524</p>
                <p><strong>Origine :</strong> Italie</p>
                <p><strong>Période Historique :</strong> Renaissance italienne</p>
                <p><strong>Guilde :</strong> Confrérie italienne</p>
            </div>
        </div>
        <p>La passion d’Ezio Auditore da Firenze transforma un adolescent oisif et irresponsable en chef Assassin accompli.</p>
        <p>Né dans une famille d’Assassins mais ignorant l’existence de la Confrérie, Ezio découvrit son héritage à 17 ans, à l’occasion de l’exécution de son père et de ses frères aînés.</p>
        <p>Son oncle, Mario, le prit sous son aile et lui enseigna les pratiques des Assassins à Monteriggioni. Quand un Templier tua son oncle et s’empara de la Pomme d’Éden qui appartenait à sa famille, Ezio rassembla quelques personnes de confiance pour s’attaquer à la corruption des Templiers qui rongeait Rome.</p>
        <p>Ezio mit des années à éliminer les Borgia. Il s’occupa aussi de localiser et de rassembler les pages du Codex d’Altaïr, cherchant à améliorer les techniques et la technologie dont disposaient les Assassins.</p>
        <p>Ezio finit par partir au Moyen-Orient pour retrouver la bibliothèque perdue d’Altaïr et les trésors qu’elle refermait. Mais il n’était pas seul : les Templiers et les Assassins étaient à sa recherche. Quand Ezio y pénétra enfin en 1512, la bibliothèque ne contenait aucun livre. Il trouva les restes d’Altaïr, une Pomme d’Éden et un Sceau de la Mémoire dans lequel étaient encodés les derniers souvenirs d’Altaïr.</p>
        <p>Une fois activée, la Pomme retranscrivit un message des Précurseurs, et Ezio apprit qu’il servait de lien entre les Précurseurs et un futur Assassin, une connexion transmettant le savoir et les informations de l’un à l’autre. Ezio retranscrivit son expérience et le message des Précurseurs dans le Codex du Prophète.</p>
        <p>En revenant de la bibliothèque de Masyaf, Ezio décida de se retirer de la Confrérie et d’épouser Sofia Sartor pour fonder une famille.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Mario Auditore, Claudia Auditore, Sofia Sartor, Leonardo da Vinci, Bartolomeo d’Alviano, Niccolò Machiavelli, Caterina Sforza, La Volpe, Yusuf Tazim, Lorenzo de ’Medici, Suleiman I et Piri Reis</p>
            <p><strong>Ennemis principaux :</strong> Rodrigo Borgia, Cesare Borgia, Juan Borgia, la famille Pazzi, la famille Barbarigo, le prince Ahmet et Manuel Palaiologos</p>
            <q>Quand j’étais un jeune homme, j’avais la liberté, mais je ne la voyais pas ; J’avais le temps, mais je ne le savais pas. Et j’avais l’amour, mais je ne le ressentais pas</q>
            <q>Requiescat in Pace</q>
            <cite> - Ezio Auditore da Firenze</cite>
        </div>
    </article>

    <article>
        <h4>Rodrigo Borgia (Pape Alexandre VI)</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC II - Rodrigo Borgia.png" alt="Rodrigo Borgia">
            <div>
                <p><strong>Dates :</strong> 1431 – 1503</p>
                <p><strong>Lieu de naissance :</strong> Espagne</p>
                <p><strong>Activité :</strong> Italie</p>
                <p><strong>Période Historique :</strong> Renaissance Italienne</p>
                <p><strong>Guilde :</strong> Rite romain</p>
            </div>
        </div>
        <p>Nommé Grand Maître de l’Ordre des Templiers en Italie en 1476, Rodrigo usa de son influence sur l’Église pour se débarrasser des politiciens en désaccord avec la philosophie des Templiers, affirmant ainsi l’influence sur l’Italie. Son pouvoir grandit jusqu’à faire de lui l’un des Templiers les plus puissants de l’Europe.</p>
        <p>À force d’élections truquées et de pots-de-vin, Rodrigo mit la main sur l’Église catholique en devenant le pape Alexandre VI en 1492. Son élection lui donna accès au Sanctuaire des Précurseurs qui se trouvait sous le Vatican ainsi qu’au Bâton Papal, un Fragment d’Éden. Pendant des années, il chercha une Pomme d’Éden à lui adjoindre afin de tirer in immense pouvoir du Sanctuaire, en proie à une soif de puissance personnelle plutôt qu’au service de la cause de l’Ordre des Templiers.</p>
        <p>Rodrigo affronta Ezio dans la Chapelle Sixtine. Il récupéra la Pomme qu’Ezio avait emportée avec lui et l’utilisa pour ouvrir le Sanctuaire du Vatican qui se trouvait sous la Chapelle. Rodrigo se retrouva cependant bloqué devant les portes intérieures et après un nouveau combat contre Ezio, il assuma n’avoir aucun respect, ni pour Dieu, ni pour l’Église. Il avait uniquement suivi cette voie pour accéder aux Fragments d’Éden et au Sanctuaire, et pour accomplir ce qu’il croyait être un destin prophétique.</p>
        <p>Après sa défaite dans le Sanctuaire, son fils, Cesare se retourna contre lui et prit le pouvoir à Rome. Rodrigo eu peur que Cesare ne compromette son grand œuvre et planifia de l’empoisonner. Au courant du projet, Cesare recracha le bout de pomme empoisonnée qu’il avait croqué et força son père à manger le bout de pomme qui lui fit fatal. Avec le temps, le règne des Borgia sur l’Ordre finit par être baptisé « l’Age des ténèbres de l’Ordre ».</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Maison Pazzi, Maison Barbarigo et Uberto Albertini</p>
            <p><strong>Ennemis principaux :</strong> Lorenzo de’ Medici, Mario Auditore et Ezio Auditore</p>
            <q>Ezio, nous autres, les Templiers, nous comprenons l’Humanité. C’est pour ça que nous la méprisons au plus haut point !</q>
            <cite> - Rodrigo Borgia (Pape Alexandre VI)</cite>
        </div>
    </article>

    <article>
        <h4>Cesare Borgia</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC Brotherhood - Cesare Borgia.png" alt="Cesare Borgia">
            <div>
                <p><strong>Dates :</strong> 1475 – 1507</p>
                <p><strong>Lieu de naissance :</strong> Rome</p>
                <p><strong>Activité :</strong> Italie</p>
                <p><strong>Période Historique :</strong> Renaissance Italienne</p>
                <p><strong>Guilde :</strong> Rite romain</p>
            </div>
        </div>
        <p>Cesare naquit au sein de l’Ordre des Templiers. Fils illégitime de Rodrigo Borgia, cet ambitieux fit assassiner son frère, Juan, en 1497, afin de s’assurer le poste de général des armées papales, devenant ainsi l’un des plus puissants hommes de l’Italie. En 1500, il mena l’assaut contre le bastion des Assassins de Monterriggioni, tuant par la même occasion Mario Auditore et mettant la main sur la Pomme d’Éden de sa famille. Il fallut plusieurs années à la Confrérie pour la récupérer.</p>
        <p>Leader d’excellence et épéiste accompli, Cesare était un manipulateur hors pair à la soif de pouvoir dévorante. Quand sa sœur, Lucrezia, tomba enceinte de l’Assassin Perotto Calderon en 1498, il lui enleva son enfant pour l’élever lui-même.</p>
        <p>En 1503, l’activité et l’ingérence des Assassins avaient largement entamé l’influence de Cesare, au point qu’il alla réclamer de l’aide à son père. Les deux hommes se disputèrent à propos de la soif de pouvoir de Cesare, et il exigea la Pomme d’Éden que son père possédait pour pouvoir l’étudier. Lucrezia prévint Cesare que Rodrigo cherchait à l’empoisonner. Il se retourna contre son père et l’assassinat en le forçant à avaler la pomme empoisonnée qui lui était destinée. C’est ainsi qu’il devient le Grand Maître de l’Ordre des Templiers en Italie.</p>
        <p>Cesare tenta par la suite de mettre la main sur la Pomme d’Éden cachée sous la basilique St Pierre, mais Ezio y arriva avant lui. L’influence et le pourvoir de Cesare s’affaiblirent encore et il fut arrêté par la garde papale sur ordre du nouveau pape, Jules II, adversaire notoire des Borgia. Après s’être évadé, Cesare tenta d’assembler une armée et finit par rejoindre celle de son beau-frère, le roi Jean III, en Navarre. Ezio l’affronta pendant le siège du château de Viana et le tua en le jetant des remparts.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Ramiro d’Orco, Oliverotto de Fermo et Vitellozzo Vitelli</p>
            <p><strong>Ennemis principaux :</strong> Ezio Auditore</p>
            <q>Ne vois-tu pas, père ? Je contrôle tout ça. Si je veux vivre, je vivrai. Si je veux prendre, je prendrai ! Si je veux ta mort, tu mourras.</q>
            <cite> - Cesare Borgia</cite>
        </div>
    </article>

    <article>
        <h4>Prince Ahmet</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC Revelations - Ahmet.png" alt="Prince Ahmet">
            <div>
                <p><strong>Dates :</strong> 1465 – 1512</p>
                <p><strong>Lieu de naissance :</strong> Amasya, Empire ottoman</p>
                <p><strong>Activité :</strong> Constantinople</p>
                <p><strong>Période Historique :</strong> Empire ottoman</p>
                <p><strong>Guilde :</strong> Rite byzantin</p>
            </div>
        </div>
        <p>Fils aîné et héritier présumé de l’Empire ottoman, Ahmet n’était pas féru de guerre et s’allia secrètement avec la rébellion des Templiers pour contrôler l’Empire. La philosophie des Templiers selon laquelle une collaboration pour la paix vaut mieux que des bains de sang perpétués par des factions isolées attira Ahmet. Ses sympathies l’empêchaient néanmoins de soutenir les Janussaires, les troupes militaires qui défendaient le sultanat, dont les objectifs contrariaient ceux des Templiers. Cet antagonisme plaça Ahmet dans une position délicate.</p>
        <p>L’influence d’Ahmet continua de grandir au sein de l’Ordre des Templiers et il finit par devenir le Grand Maître du Rite byzantin. En 1511, Ahmet se mit en quête des Clés de Masyaf, convaincu que la Bibliothèque d’Altaïr lui dévoilerait l’emplacement du légendaire Grand Temple des Précurseurs, ce qui lui offrirait assez de pouvoir pour prendre le contrôle de l’Empire de son père et l’aligner sur les objectifs des Templiers.</p>
        <p>Finalement, c’est son fils cadet, Selim, qui fut mis sur le trône par le père d’Ahmet. Après un ultime affrontement contre Ezio Auditore, Ahmet fit face à son frère qui l’étrangla de ses mains avant de le jeter d’une falaise.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Manuel Palaiologos</p>
            <p><strong>Ennemis principaux :</strong> Ezio Auditore et Selim</p>
            <q>Je suis fatigué des tous ces bains de sang insensés qui opposent les pères à leurs fils, les frères à leurs frères. Afin d’obtenir une paix durable, l’Humanité doit s’unir autour d’un cerveau unique.</q>
            <cite> - Prince Ahmet</cite>
        </div>
    </article>                
</section>