<h2>Assassin's Creed Rogue</h2>

<section class="contexte">
    <h3>Contexte</h3>
    <img src="<?= ASSETS ?>img/logo/Logo AC Rogue.png" alt="Logo AC Rogue">
    <p>L’histoire se déroule pendant la Guerre de Sept Ans (1756 – 1763). On y incarne Shay Patrick Cormac, un jeune membre de la Confrérie des Assassins qui va sombrer dans les ténèbres et changer le destin des colonies Américaines. Après une mission ayant tourné au drame, Shay renie les Assassins qui tenteront à leur tour de le tuer. Trahi par ses frères d’armes, Shay se lance dans une quête de vengeance et décide de traquer ceux qui se sont retournés contre lui. Il deviendra le chasseur d’Assassins le plus redouté de l’Histoire.</p>
</section>

<section class="perso">
    <h3>Personnages Principaux</h3>
    <article>
        <h4>Shay Patrick Cormac</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC Rogue - Shay.png" alt="Shay Patrick Cormac">
            <div>
                <p><strong>Dates :</strong> 1731 – inconnu</p>
                <p><strong>Lieu de naissance :</strong> New York</p>
                <p><strong>Activité :</strong> Amérique coloniale</p>
                <p><strong>Période Historique :</strong> Guerre de Sept Ans</p>
                <p><strong>Guilde :</strong> Rite colonial</p>
            </div>
        </div>
        <p>Shay rejoignit la Confrérie en 1749. Son ami Liam O’Brien et lui furent formés par Achilles Davenport. En 1755, Shay fut envoyé à Lisbonne pour dénicher ce qu’Achilles pensait être un Fragment d’Éden. Au lieu de ça, en pénétrant dans le Temple, Shay découvrit une technologie des Précurseurs qui se désintégra dans ses mains en déclenchant un violent tremblement de terre.</p>
        <p>À son retour dans les colonies, Shay accusa Achilles d’avoir été au courant des conséquences qu’entraînerait la découverte du Fragment et du danger qu’il avait fait courir aux victimes de la catastrophe. Il quitta la Confrérie et vola le Manuscrit du Voynich pour empêcher les Assassins de localiser d’autres sites de Précurseurs. Il faut poursuivi et blessé par balle. En s’échappant, il tomba d’une falaise, passant pour mort aux yeux des Assassins.</p>
        <p>Shay fut récupéré par le colonel George Monro, un Templier qui le remit sur pied. Après avoir travaillé à ses côtés, il fut intronisé chez les Templiers par Haytham Kenway et lui confia l’emplacement des sites des Précurseurs, craignant que la quête des Assassins provoque la mort de nouveaux innocents.</p>
        <p>Shay affronta l’Assassin Adéwalé lors d’une bataille navale pendant la Guerre de Sept Ans. Il poursuivit son navire et assassina lui-même son adversaire, mais pas avant qu’Adéwalé ne l’informe qu’Achilles et la Confrérie coloniale avaient les moyens de localiser d’autres sites de Précurseurs.</p>
        <p>Malgré des directions hasardeuses, Shay finit par retrouver Achilles et Liam dans un Temple des Précurseurs, dans l’Arctique. Le Temple renfermait un artefact similaire à celui qui avait déclenché le tremblement de terre de Lisbonne. Celui-ci fut déplacé lors de l’altercation entre Shay et Liam, déclenchant un tremblement de terre qui détruisit le Temple et blessa mortellement Liam.</p>
        <p>Grâce à lui, les Templiers coloniaux se débarrassèrent de la Confrérie coloniale, mais Shay empêcha Haytham de tuer Achilles, prétextant que son Mentor devait vivre pour expliquer aux futurs Assassins les risques encourus par la recherches des Temples des Précurseurs. En 1776, Shay partit pour Paris, où il tua Charles Dorian et récupéra une Boite des Précurseurs.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> George Monro, Christopher Gist et Haytham Kenway</p>
            <p><strong>Ennemis principaux :</strong> Achilles Davenport, Hope Jensen et Liam O’Brien</p>
            <q>La chasse m’a guidé aux confins oubliés de ce monde. Les saisons passent et me plongent inlassablement dans les ténèbres qui m’ont dévoilé la vérité : c’est dans les cendres que je trouverai ma rédemption ; je dois brûler le passé pour rétablir le bien. J’étais Assassin, je les poursuis désormais. Je dois détruire ceux qui je ne pus appeler mes « frères ». Le vent ne souffle pas… et je suis un chasseur.</q>
            <cite> - Shay Patrick Cormac</cite>
        </div>
    </article>

    <article>
        <h4>Adéwalé</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC Rogue - Adewale.png" alt="Adéwalé">
            <div>
                <p><strong>Dates :</strong> 1692 – 1758</p>
                <p><strong>Origine :</strong> Caraïbes</p>
                <p><strong>Période Historique :</strong> Age d’Or de la Piraterie</p>
                <p><strong>Guilde :</strong> Confrérie des Indes Occidentales</p>
            </div>
        </div>
        <p>Ancien esclave grave et sévère originaire de Trinidad et secouru par Edward Kenway, Adéwalé fut le quartier-maître de pirate sur son navire, le <em>JackDaw</em>. Bien qu’il s’engageât dans la piraterie afin de semer un peu de chaos dans l’ordre et la tyrannie, Adéwalé découvrit qu’il ne possédait pas réellement un cœur de pirate et trouvait cette activité malhonnête. Il assista à la rébellion des Marrons à Port-au-Prince en manœuvrant l’<em>Experto Crede</em>, un négrier dont il s’était emparé ; après avoir libéré les esclaves, il s’appropria le navire.</p>
        <p>Adéwalé s’engagea auprès de la Confrérie des Indes Occidentales autour de 1720, en s’entraînant avec son Mentor, Ah Tabai. Il enjoignit Edward Kenway à rejoindre lui aussi les Assassins et son ami finit par accepter cette proposition. Adéwalé servit la Confrérie dans les indes Occidentales en repoussant les attaques des Templiers, en allant délivrer Edward à Port Royal et en aidant les Assassins à empêcher leurs antagonistes de s’emparer de l’Observatoire.</p>
        <p>Adéwalé soutint aussi les alliés français des Assassins au début de la Guerre de Sept Ans en attaquant la Marine Royale à Louisbourg, attirant sur lui l’attention des Templier. Quand Shay Cormac et Haytham Kenway se mirent à la poursuite de l’Experto Crede, Adéwalé fut forcé d’échouer son bateau pour sauver son équipage. Il affronta alors Haytham et Shay Cormac qui réussirent à le distraire et à le tuer.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Edward Kenway, Ah Tabai, Achilles Davenport, Bastienne Josèphe et Augustin Dieufort</p>
            <p><strong>Ennemis principaux :</strong> Marquis de Fayet</p>
            <q>C’est Adéwalé. Il était esclave et s’est libéré, lui, et des centaines de ses frères dans les Indes Occidentales. Cet homme personnifie à lui seul le Crédo.</q>
            <cite> - Liam O’Brien</cite>
        </div>
    </article>
</section>