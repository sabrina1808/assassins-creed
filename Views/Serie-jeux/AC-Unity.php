<h2>Assassin's Creed Unity</h2>

<section class="contexte">
    <h3>Contexte</h3>
    <img src="<?= ASSETS ?>img/logo/Logo AC Unity.png" alt="Logo AC Unity">
    <p>L’histoire se déroule en pleine France révolutionnaire (1789 – 1799) et retrace le parcours d’Arno Dorian, un jeune noble rongé par la mort de son père adoptif, pour laquelle il se sent responsable. Il se lance alors dans une quête pour retrouver les responsables de ce drame et entre dans la Confrérie des Assassins. Sa quête va l’emmener au milieu d’une impitoyable lutte de pouvoir pour le sort de la nation, et lui faire découvrir une nouvelle branche radicale des Templiers qui ont tiré les ficelles de la Révolution dans l’ombre à leur profit pour préparer l’avènement de leur Nouvel Ordre Mondial. Cette quête fera de lui un véritable Maître Assassin.</p>
</section>

<section class="perso">
    <h3>Personnages Principaux</h3>
    <article>
        <h4>Arno Dorian</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC Unity - Arno Dorian.png" alt="Arno Dorian">
            <div>
                <p><strong>Dates :</strong> 1768 – inconnu</p>
                <p><strong>Activité :</strong> France</p>
                <p><strong>Période Historique :</strong> Révolution française</p>
                <p><strong>Guilde :</strong> Confrérie parisienne</p>
            </div>
        </div>
        <p>Né d’un père Assassin, Arno Victor Dorian, voyagea seul avec son père lorsque sa mère, découvrant les activités secrètes de son mari, Charles Dorian, quitta le domicile familial ; Arno vécut le traumatisme de découvrir le cadavre de son père assassiné à Versailles par Shay Cormac. Adopté et élevé par un Grand Maître Templier, Arno grandit aux côtés d’Élise de la Serre. Arrivés à l’âge adulte, leur relation d’amitié s’était secrètement transformée en histoire d’amour.</p>
        <p>Accusé à tort du meurtre de son père adoptif en 1789, Arno fut emprisonné à la Bastille où Pierre Bellec, un Maître Assassin, lui dévoila la vérité sur son héritage familial, et décida de l’entraîner pour en faire un Assassin. Après leur évasion, Arno rejoignit les Assassins pour venger ses deux pères.</p>
        <p>Bien qu’Élise ait été membre de l’Ordre des Templiers, elle repéra en son sein des factions qu’elle considérait comme dangereuses et s’allia à Arno pour découvrir l’identité de ceux qui manipulaient la Révolution française dans l’intérêt des Templiers.</p>
        <p>Après que Bellec eût empoisonné Mirabeau, Mentor parisien que l’Assassin trouvait trop faible, Arno le défia et le tua.</p>
        <p>La Confrérie perçut ce geste comme une vengeance personnelle et écarta Arno qui retrouva rapidement Élise. Leur enquête les mena au nouveau Grand Maître de l’Ordre, François-Thomas Germain. Lors de la confrontation sous le Temple Parisien – au cours de laquelle fut utilisée une Épée d’Éden – Germain et Élise trouvèrent la mort et l’Épée fut gravement endommagée.</p>
        <p>Remué par la mort d’Élise, Arno s’éloigna des Assassins un certain temps, avant de replonger dans le chaos de la France révolutionnaire. Il se mobilisa pour empêcher les troupes de Napoléon Bonaparte de localiser un Temple des Précurseurs caché sous la Basilique de Saint-Denis et de mettre la main sur la Pomme d’Éden qu’il renfermait.</p>
        <p>Arno tenta de respecter l’ultime vœu d’Élise, qui lui demanda de faire le pont entre les Templiers et les Assassins.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Élise de la Serre et Honoré Mirabeau</p>
            <p><strong>Ennemis principaux :</strong> François-Thomas Germain, Pierre Bellec et Robespierre</p>
            <q>Tout ce que nous faisons, tout ce que nous sommes, commence et termine avec nous.</q>
            <cite> - Arno Dorian</cite>
        </div>
    </article>

    <article>
        <h4>Élise de la Serre</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC Unity - Elise.png" alt="Élise de la Serre">
            <div>
                <p><strong>Dates :</strong> 1768 – 1794</p>
                <p><strong>Lieu de naissance :</strong> Versailles</p>
                <p><strong>Activité :</strong> France</p>
                <p><strong>Période Historique :</strong> Révolution française</p>
                <p><strong>Guilde :</strong> Rite parisien</p>
            </div>
        </div>
        <p>Élevée dans une famille de Templiers en ayant suivi des cours d’histoire, d’observation et de combat, Élise fut formée pour un jour prétendre au rang de Grand Maître du Rite parisien. Son frère adoptif, Arno Dorian, venait d’une famille d’Assassins sans le savoir. Quand son père demanda à Élise de guider Arno sur la voie des Templiers, elle refusa.</p>
        <p>Élise fut officiellement intronisée en 1789. Elle retrouva Arno après avoir été séparée de lui quelques années, mais la joie des retrouvailles fut ternie par l’assassinat de son père, dont on soupçonna rapidement Arno. Alors que la Révolution atteignait son paroxysme, Élise continua d’enquêter sur la mort de son père et découvrit une faction radicalisée au sein de l’Ordre. Elle s’allia alors à Arno, qui avait rejoint la Confrérie des Assassins de son côté.</p>
        <p>Le désir de vengeance qui animait Élise était en contradiction avec l’esprit de justice qui motivait Arno, et ces différences nuisirent à leur relation. Ils continuèrent néanmoins à travailler ensemble, découvrirent le groupe dissident radical de François-Thomas Germain et décidèrent de s’attaquer directement à ce dernier. Élise fut tuée dans l’explosion qu’elle provoqua en endommageant l’Épée d’Éden détenue par Germain. Avant de mourir, elle avait écrit une lettre à Arno dans laquelle elle lui expliquait qu’elle aimerait plus que tout voir la Confrérie et l’Ordre travailler main dans la main pour offrir un meilleur avenir au monde. Il respecta ce souhait et œuvra pour une unification.</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Arno Dorian et Bernard Ruddock</p>
            <p><strong>Ennemis principaux :</strong> François-Thomas Germain</p>
            <q>Assassins. Templiers. Bah. Ils possèdent assez de dogmes pour nourrir 10 000 églises et deux fois plus de croyances tordues. Ça fait des siècles qu’ils se bagarrent et dans quel but ? L’Humanité suit son cours, avec ou sans eux.</q>
            <cite> - Élise de la Serre</cite>
        </div>
    </article>

    <article>
        <h4>François-Thomas Germain</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC Unity - Francois-Thomas Germain.jpg" alt="François-Thomas Germain">
            <div>
                <p><strong>Dates :</strong> 1726 – 1794</p>
                <p><strong>Lieu de naissance :</strong> Paris</p>
                <p><strong>Activité :</strong> Paris</p>
                <p><strong>Période Historique :</strong> Révolution française</p>
                <p><strong>Guilde :</strong> Rite parisien</p>
            </div>
        </div>
        <p>Orfèvre français et Sage, Germain avait des visions des Précurseurs et chercha à reformer l’Ordre des Templiers pour l’aligner sur la vision de l’Ordre que proposait Jacques de Molay dans son Codex <em>Pater Intellectus</em>. Ses idées furent considérées trop radicales par le Grand Maître François de la Serre, qui l’exclut de l’Ordre. Germain décida alors de rallier tous les conseillers de la Serre à sa cause avant de la faire assassiner en 1789.</p>
        <p>Devenu Grand Maître, François-Thomas Germain orchestra certains événements de la Révolution française afin d’affaiblir le pouvoir détenu par l’aristocratie en condamnant la monarchie et stimulant le chaos. Germain comptait renverser l’aristocratie en plaçant le pouvoir aux mains de la classe moyenne, créant ainsi un système économique capitaliste que les Templiers auraient plus de facilité à contrôler. La famine et la Terreur ne l’inquiétèrent pas le moins du monde. Il savait qu’à terme, l’ordre reprendrait le dessus et améliorerait le système en place.</p>
        <p>Germain affronta Élise de la Serre et l’Assassin Arno Dorian dans le Sanctuaire des Templiers sous la forteresse du Temple à Paris, où il s’était enfermé avec une Épée d’Éden pour échapper à la Terreur et pour tenter de communiquer avec Jacques de Molay. Lors de leur ultime altercation, Élise réussit à endommager l’artefact au point de lui faire perdre son pouvoir ; s’ensuivit une explosion qui tua Élise et blessa Germain. Arno l’assassinat alors. Dans son dernier souffle, Germain expliqua à son Assassin son raisonnement et ce qui avait motivé ses actes..</p>
        <div class="citation">
            <p><strong>Alliés principaux :</strong> Maximilien Robespierre, Chrétien Lafrenière et Aloys la Touche</p>
            <p><strong>Ennemis principaux :</strong> Arno Dorian et Élise de la Serre</p>
            <q>Pensez un peu : la marche du progrès est lente, mais elle est aussi inévitable qu’un glacier. Tout ce que vous avez fait, c’est reculer l’inévitable. Une mort n’arrêtera pas la marée. Ce ne seront peut-être pas mes mains qui guideront l’Humanité à la place qu’elle mérite – mais se seront les mains d’un autre.</q>
            <cite> - François-Thomas Germain</cite>
        </div>
    </article>
</section>