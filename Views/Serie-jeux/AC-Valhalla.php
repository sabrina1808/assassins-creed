<h2>Assassin's Creed Valhalla</h2>

<section class="contexte">
    <h3>Contexte</h3>
    <img src="<?= ASSETS ?>img/logo/Logo AC Valhalla.png" alt="Logo AC Valhalla">
    <p>Dans le présent, on incarne toujours Layla Hassan, membre de la Confrérie des Assassins qui exploire la mémoire des premiers Assassins.</p>
    <p>Le jeu prend place à l'ère des Viking, au IXème siècle de notre ère. Les ressources de la Norvège glaciale diminuent et les Vikings se dirigent vers les terres verdoyantes de l'Angleterre, dans le but de les conquérir.</p>
</section>

<section class="perso">
    <h3>Personnages Principaux</h3>
    <article>
        <h4>Eivor</h4>
        <div class="info valhalla">
            <img src="<?= ASSETS ?>img/perso/AC Valhalla - Eivor (Femme).png" alt="Eivor (Femme)">
            <img src="<?= ASSETS ?>img/perso/AC Valhalla - Eivor (Homme).png" alt="Eivor (Homme)">
            <div>
                <p><strong>Dates :</strong> IXème siècle - inconnu</p>
                <p><strong>Lieu :</strong> Norvège, Angleterre</p>
                <p><strong>Période Historique :</strong> Âge des Vikings</p>
                <p><strong>Affiliation :</strong> Clan du corbeau</p>
            </div>
        </div>
        <p>Eivor est un(e) guerrier(re) Viking né(e) en Norvège qui menera un raid pour conquérir les terrres d'Angleterre, contre le Roi Alfred de Wessex. Il/Elle est respecté(e) par les siens et est considéré(e) comme un(e) vrai(e) leader(euse).</p>
    </article>

    <article>
        <h4>Alfred le Grand</h4>
        <div class="info">
            <img src="<?= ASSETS ?>img/perso/AC Valhalla - Alfred le Grand.png" alt="Alfred le Grand">
            <div>
                <p><strong>Dates :</strong> vers 848 - 899</p>
                <p><strong>Lieu :</strong> Angleterre</p>
                <p><strong>Période Historique :</strong> Âge des Vikings</p>
                <p><strong>Affiliation :</strong> Souverain de Wessex</p>
            </div>
        </div>
        <p>Alfred le Grand est le Roi du Wessex, connu pour avoir défendu le royaume d'Angleterre contre les Vinking.</p>
        <div class="citation">
            <q>Ils sont sans pitié, ces barbares païens. Ils tuent et massacrent aveuglément. Ils souillent le sol d'Angleterre, une terre qu'ils ne défendront jamais. Maintenant est venu le temps de leur parler la seule langue qu'ils puissent comprendre</q>
            <cite> - Alfred le Grand</cite>
        </div>
    </article>
</section>