<h2>Mot de passe oublié ?</h2>

<p>Si vous avez oublié votre mot de passe, merci de remplir le champ ci-dessous en indiquant votre adresse mail. Un mail vous sera alors envoyé pour vous permettre d'effectuer le changement de mot de passe.</p>

<?php if($response == "yes") { ?>
    <p class="goodEmail">Un mail a été envoyé à l'adresse que vous avez renseigné pour effectué le changement de mot de passe.</p>
<?php } else if($response == "no") { ?>
    <p class="wrongEmail">L'adresse mail que vous avez renseigné est inconnue. Merci d'en renseigné une autre.</p>
<?php } ?>

<form class="formulaire" action="<?= HOST ?>ForgotPassword" method="post">
    <fieldset>
        <legend><i class="fa fa-at"></i> Utilisateur ?</legend>
        <ul>
            <li>
                <label for="Email">Email<span class="asterisk">*</span> :</label>
                <input type="email" name="Email" id="email" placeholder="exemple@exemple.com" value="<?= $email ?>">
                <span class="error" id="error-email"></span>
            </li>
            <li>
                <p><span class="asterisk">*</span> : Champ requis</p>
            </li>
        </ul>
    </fieldset>
    
    <ul>
        <li>
            <button type="submit" class="send">Envoyer</button>
            <a href="<?= HOST ?>Login" title="Connexion" class="cancel">Annuler</a>
        </li>
    </ul>
</form>