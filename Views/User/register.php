<h2>Création d'un compte utilisateur</h2>

<p>Pour vous inscrire, merci de remplir le formulaire ci-dessous.</p>

<?php if($message == "error") { ?>
    <p class="wrongEmail">L'adresse mail que vous avez renseignée est déjà associée à un compte. Merci de bien vouloir choisir une autre adresse ou vous connectez avec celle-ci.</p>
<?php } ?>

<form class="formulaire" action="<?= HOST ?>Register" method="post">
    <fieldset>
        <legend><i class="fa fa-info-circle"></i> Identité</legend>
        <ul>
            <li>
                <label for="FirstName">Prénom<span class="asterisk">*</span> :</label>
                <input type="text" name="FirstName" id="FirstName" placeholder="Votre prénom" value="<?= $firstname ?>">
                <span class="error" id="error-firstname"></span>
            </li>
            <li>
                <label for="LastName">Nom<span class="asterisk">*</span> :</label>
                <input type="text" name="LastName" id="LastName" placeholder="Votre nom" value="<?= $lastname ?>">
                <span class="error" id="error-lastname"></span>
            </li>
            <li>
                <label for="Address">Adresse<span class="asterisk">*</span> :</label>
                <input type="text" name="Address" id="Address" placeholder="Votre addresse" value="<?= $address ?>">
                <span class="error" id="error-address"></span>
            </li>
            <li>
                <label for="City">Ville<span class="asterisk">*</span> :</label>
                <input type="text" name="City" id="City" placeholder="Votre ville" value="<?= $city ?>">
                <span class="error" id="error-city"></span>
            </li>
            <li>
                <label for="zipCode">Code Postal<span class="asterisk">*</span> :</label>
                <input type="text" name="zipCode" id="zipCode" placeholder="Votre code postal" value="<?= $zipCode ?>">
                <span class="error" id="error-zipcode"></span>
            </li>
            <li>
                <label for="Country">Pays<span class="asterisk">*</span> :</label>
                <input type="text" name="Country" id="Country" placeholder="Votre pays" value="<?= $country ?>">
                <span class="error" id="error-country"></span>
            </li>
            <li>
                <label for="Phone">Téléphone :</label>
                <input type="text" name="Phone" id="Phone" placeholder="Votre téléphone" value="<?= $phone ?>">
                <span class="error" id="error-phone"></span>
            </li>
        </ul>
    </fieldset>

    <fieldset>
        <legend><i class="fa fa-check-circle"></i> Informations de connexion</legend>
        <ul>
            <li>
                <label for="Email">Email<span class="asterisk">*</span> :</label>
                <input type="email" name="Email" id="Email" placeholder="exemple@exemple.com" value="<?= $email ?>">
                <span class="error" id="error-email"></span>
            </li>
            <li>
                <label for="Password">Mot de passe<span class="asterisk">*</span> :</label>
                <input type="password" name="Password" id="Password" placeholder="Mot de passe" value="<?= $password ?>">
                <span class="error" id="error-password"></span>
            </li>
            <li>
                <p><span class="asterisk">*</span> : Champ requis</p>
            </li>
        </ul>
    </fieldset>

    <ul>
        <li>
            <button type="submit" class="send">Création du compte</button>
            <a href="<?= HOST ?>Home" title="Accueil" class="cancel">Annuler</a>
        </li>
    </ul>
</form>