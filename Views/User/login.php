<h2>Connexion à son compte utilisateur</h2>

<p>Pour vous connecter à votre espace, merci de bien vouloir renseigné votre adresse mail et votre mot de passe.</p>

<?php if($message == "error") { ?>
    <p class="wrongEmail">L'adresse mail ou le mot de passe ne correspondent pas.</p>
<?php } ?>

<form class="formulaire" action="<?= HOST ?>Login" method="post">
    <fieldset>
        <legend><i class="fa fa-check-circle"></i> Connexion</legend>
        <ul>
            <li>
                <label for="Email">Email :</label>
                <input type="email" name="Email" id="Email" placeholder="exemple@exemple.com" value="<?= $email ?>">
                <span class="error" id="error-email"></span>
            </li>
            <li>
                <label for="Password">Mot de passe :</label>
                <input type="password" name="Password" id="Password" placeholder="Mot de passe" <?= $password ?>>
                <span class="error" id="error-password"></span>
            </li>
        </ul>
    </fieldset>

    <ul>
        <li>
            <button type="submit" class="send">Connexion</button>
            <a href="<?= HOST ?>Home" title="Accueil" class="cancel">Annuler</a>
        </li>
    </ul>
</form>

<hr>
<h3>Mot de passe oublié ?</h3>
<p>Si vous avez oublié votre passe, merci de cliquez <a href="<?= HOST ?>ForgotPassword" title="Mot de passe oublié ?">ici</a>.</p>

<hr>
<h3>Nouvel utilisateur ?</h3>
<p>Si vous n'avez pas de compte utilisateur, merci de cliquer <a href="<?= HOST ?>Register" title="Création de compte">ici</a>.</p>