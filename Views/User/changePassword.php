<h2>Changement de mot de passe</h2>

<p>Pour changer le mot de passe, merci de remplir les champs ci-dessous.</p>

<?php if($response == "yes") { ?>
    <p class="goodpassword">Votre mot de passe a bien été modifié.</p>
<?php } else if($response == "no") { ?>
    <p class="wrongpassword">Les deux mots de passe ne correpondent pas.</p>
<?php } ?>

<form class="formulaire" action="<?= HOST ?>ChangePassword/userId/<?= $userId ?>" method="post">
    <fieldset>
        <legend><i class="fa fa-key"></i> Nouveau mot de passe</legend>
        <ul>
            <li>
                <label for="psw1">Nouveau mot de passe<span class="asterisk">*</span> :</label>
                <input type="password" name="psw1" id="psw1" placeholder="Mot de passe" value="<?= $psw1 ?>">
                <span class="error" id="error-psw1"></span>
            </li>
            <li>
                <label for="psw2">Confirmation du mot de passe<span class="asterisk">*</span> :</label>
                <input type="password" name="psw2" id="psw2" placeholder="Confirmation" value="<?= $psw2 ?>">
                <span class="error" id="error-psw2"></span>
            </li>
            <li>
                <p><span class="asterisk">*</span> : Champ requis</p>
            </li>
        </ul>
    </fieldset>

    <ul>
        <li>
            <button type="submit" class="send">Modifier</button>
            <a href="<?= HOST ?>Home" title="Accueil" class="cancel">Annuler</a>
        </li>
    </ul>
</form>