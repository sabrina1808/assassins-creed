<h2>Mon compte</h2>

<form class="formulaire" action="<?= HOST ?>Account-update/userId/<?= $_SESSION['Id'] ?>" method="post">
    <fieldset>
        <legend><i class="fa fa-edit"></i> Mise à jour des informations</legend>
        <ul>
            <li>
                <label for="Email">Email<span class="asterisk">*</span> :
                </label>
                <input type="email" name="Email" id="email" value="<?= $userInfo['Email'] ?>">
                <span class="error" id="error-email"></span>
            </li>
            <li>
                <label for="Phone">Téléphone :</label>
                <input type="text" name="Phone" id="phone" value="<?= $userInfo['Phone'] ?>">
                <span class="error" id="error-phone"></span>
            </li>
            <li>
                <label for="Address">Adresse<span class="asterisk">*</span> :</label>
                <input type="text" name="Address" id="address" value="<?= $userInfo['Address'] ?>">
                <span class="error" id="error-address"></span>
            </li>
            <li>
                <label for="ZipCode">Code Postal<span class="asterisk">*</span> : </label>
                <input type="text" name="ZipCode" id="zipCode" value="<?= $userInfo['ZipCode'] ?>">
                <span class="error" id="error-zipcode"></span>
            </li>
            <li>
                <label for="City">Ville<span class="asterisk">*</span> :</label>
                <input type="text" name="City" id="city" value="<?= $userInfo['City'] ?>">
                <span class="error" id="error-city"></span>
            </li>
            <li>
                <label for="Country">Pays<span class="asterisk">*</span> :</label>
                <input type="text" name="Country" id="country" value="<?= $userInfo['Country'] ?>">
                <span class="error" id="error-country"></span>
            </li>
            <li>
                <p><span class="asterisk">*</span> : Champ requis</p>
            </li>
        </ul>
    </fieldset>
    <ul>
        <li>
            <button type="submit" class="send">Mettre à jour</button>
            <a href="<?= HOST ?>Account/userId/<?= $_SESSION['Id'] ?>" title="Mon compte" class="cancel">Annuler</a>
        </li>
    </ul>
</form>