<h2>Mon compte</h2>

<section class="account">
   <h3>Bienvenue <?= $userInfo['FirstName'] ?></h3>
   <p>Voici vos informations actuelles :</p>
   <ul>
      <li><p><strong>Nom :</strong> <?= $userInfo['LastName'] ?></p></li>
      <li><p><strong>Prénom :</strong> <?= $userInfo['FirstName'] ?></p></li>
      <li><p><strong>Email :</strong> <?= $userInfo['Email'] ?></p></li>
      <?php if($userInfo['Phone'] != null) { ?>
         <li><p><strong>Téléphone :</strong> <?= $userInfo['Phone' ] ?></p></li>
      <?php } else { ?>
         <li><p><strong>Téléphone :</strong> non renseigné</p></li>
      <?php } ?>
      <li><p><strong>Adresse :</strong> <?= $userInfo['Address'] ?>, <?= $userInfo['ZipCode'] ?> <?= $userInfo['City'] ?></p></li>
      <li><p><strong>Pays :</strong> <?= $userInfo['Country'] ?></p></li>
      <li><p><strong>Rôle :</strong> <?= $userInfo['Role'] ?></p></li>
   </ul>
</section>

<hr>

<h3>Mettre à jour les informations</h3>
<p>Pour mettre à jour vos informations, merci de cliquez <a href="<?= HOST ?>Account-update/userId/<?= $_SESSION['Id'] ?>" title="Mettre à jour mes informations">ici.</a></p>
<p>Pour changer de mot de passe, merci de cliquez <a href="<?= HOST ?>ChangePassword/userId/<?= $_SESSION['Id']?>" title="Changement de mot de passe">ici.</a></p>

<hr>

<h3>Suppression de votre compte</h3>
<p>Pour supprimer votre compte utilisation, merci de cliquez <a href="<?= HOST ?>Account-delete/userId/<?= $_SESSION['Id'] ?>" title="Supprimer mon compte">ici</a>. <span class="warning">Attention cette action est irréversible.</span></p>