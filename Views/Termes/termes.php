<h2>Termes présents dans la série Assassin's Creed</h2>

<section class="termes">
    <article>
        <h3>Animus</h3>
        <img src="<?= ASSETS ?>img/Animus Omega.png" alt="Animus Oméga">
        <q>Notre ADN fonctionne comme des archives. Il contient non seulement des instructions génétiques transmises par les générations précédentes, mais aussi des souvenirs. Les souvenirs de nos ancêtres.</q>
        <cite> - Warren Vidic</cite>
        <q>Là, vous êtes dans l’Animus. […] C’est une sorte de projecteur restituant la mémoire génétique en trois dimensions.</q>
        <cite> - Dr Warren Vidic, présentant l'Animus à Desmond Miles</cite>
        <p>L’Animus est l’un des outils les plus importants utilisé à la fois par la Confrérie des Assassins et l’Ordre des Templiers. L’accès aux mémoires génétiques révèle des renseignements précieux concernant les Fragments d’Éden – de puissants artefacts datant de la civilisation des Précurseurs – ainsi que les sites des Précurseurs. Les Templiers cherchent à utiliser ces artefacts et ces renseignements pour étendre leur pouvoir et leur contrôle sur le monde ; de leur côté, les Assassins font tout pour les empêcher de mettre la main dessus afin de protéger l’indépendance de l’Humanité.</p>
        <p>Il est désormais confirmé que, comme envisagé par certaines théories, l’Animus est basé sur la technologie des Précurseurs.</p>
        <p>A l’origine, l’Animus permet à l’utilisateur de lire les mémoires génétiques d’un sujet, de la projeter sur un écran en trois dimensions et d’immerger l’utilisateur dans l’expérience de cette mémoire.</p>
        <p>L’espace virtuel d’Abstergo – et ses produits de loisir, qui invitent le public à considérer l’Histoire comme un terrain de jeu – coordonné à l’Helix (un logiciel cloud basé sur la technologie Animus), permet à quiconque de revivre les mémoires des célèbres figures de l’Histoire proposées par cette plateforme. Au-delà de l’usage de cette technologie à des fins exclusivement commerciales, ses champs de recherche et d’analyse ont été rapidement enrichis par la mise en place d’applications plus complexes. La technologie a notamment connu certains développements récents sidérants : désormais, des modèles d’Animus portables permettent d’utiliser des fragments d’ADN incomplets ou endommagés pour reconstituer une mémoire génétique. Grâce aux dernières technologies permettant de scanner des sources historiques, les possibilités sont désormais infinies.</p>
    </article>

    <article>
        <h3>Effet de transfert</h3>
        <img src="<?= ASSETS ?>img/Effet de transfert.png" alt="Effet de transfert">
        <q>Une exposition prolongée à l’Animus a provoqué un « effet de transfert » dans la structure générique du sujet 16. Le résultat a été un mélange de mémoire génétique et de mémoire en temps réel.</q>
        <cite> - E-mail de Lucy Stillman à Warren Vidic</cite>
        <p>L’effet de transfert est un état psychosomatique durant lequel le sujet revit les souvenirs d’un ancêtre par le biais de violentes hallucinations. Il est la conséquence d’une surexposition à l’Animus, et tire son nom de la tendance qu’ont les souvenirs ancestraux à se transférer à la conscience d’un sujet. Il peut provoquer des visions incontrôlables, et dans les cas les plus graves, des dommages psychologiques irréversibles.</p>
    </article>
</section>

<section class="termes">
    <article>
        <h3>Saut de la foi</h3>
        <img src="<?= ASSETS ?>img/AC - Saut de la foi.jpg" alt="Saut de la foi">
        <q>Ils ne craignent pas la mort, ils l’appellent de leur vœu ! Elle est leur récompense ! Montrez […] que vous ne connaissez pas la peur ! Dieu vous appel.</q>
        <cite> - Al Mualim, à prorpos du Saut de la Foi</cite>
        <p>Le Saut de la Foi est un mouvement de course libre qui permet à un Assassin de sauter d’une haute structure, d’exécuter une roulade en l’air et d’atterrir sur le dos, sain et sauf, sur un tas de foin, dans de l’eau ou d’autres surfaces molles. Il peut être exécuté à partir d’un mur, d’une poutre, en s’élançant ou en inclinant simplement son corps dans le vide. Le mouvement est utilisé pour échapper à ses poursuivants. Mais plus symboliquement, il célèbre aussi la fin de l’entraînement d’un apprenti et de son ascension au rang d’Assassin. </p>
        <p>Bien que le Saut de la Foi ne soit plus autant utilisé au XXème siècle qu’il l’était autrefois, certains Assassins modernes comme Charlotte de la Cruz le pratiquaient encore en adjoignant un équipement moderne – comme un wingsuit – à la pratique, ce qui modifiait considérablement l’étendue du pouvoir et son usage.</p>
        <p>C’est un saut quasi sacré et mythique que les Assassins effectuent du haut d’un promontoire élevé en atterrissant dans une botte de foin ou dans l’eau. Comme son nom l’indique, c’est en quelque sort un acte de foi de l’Assassin qui démontre entre autres qu’il ne craint pas la mort.</p>
    </article>

    <article>
        <h3>La Vision d'aigle</h3>
        <img src="<?= ASSETS ?>img/AC - Vision aigle.jpg" alt="Vision d'aigle">
        <q>J’ai déjà vu des choses semblables. Une lueur, comme la lune sur l’océan. Ah ! Je comprends. Je connais ça. Je le fais depuis l’enfance. C’est comme si j’utilisais tous mes sens à la fois. Voir les bruits et entendre les formes. Quel mélange étonnant.</q>
        <cite> - Edward Kenway à propos de la Vision d’aigle</cite>
        <p>La Vision d’aigle est un sixième sens dormant que possèdent les Humains, un don qu’ils ont hérité de leur création par la Première Civilisation et aux croisements entre certains des premiers humains avec ces Précurseurs.</p>
        <p>Ceux qui possèdent ce don peuvent sentir instinctivement comment les personnes et les objets autour d’eux réagissent, qui se manifeste par une lueur colorée, semblable à une aura. Chaque couleur a une signification particulière : le rouge indique les ennemis ou le sang versé, le bleu indique les alliés, le blanc indique les sources d’informations ou les cachettes et le doré indique les cibles ou les objets d’intérêts.</p>
        <p>Quand un individu maîtrise la Vision d’aigle, cette capacité peut évoluer sous une forme plus avancée appelée Sens de l’aigle. Cette forme augmente tous les sens de son possesseur, qui peut dès lors sentir le rythme cardiaque d’une cible ou voir le chemin emprunté par une personne.</p>
    </article>
</section>

<section class="termes">
    <article>
        <h3>Les Précurseurs</h3>
        <img src="<?= ASSETS ?>img/Les Precurseurs.png" alt="Les Précurseurs">
        <p>Les Isus, appelés Précurseurs, Ceux qui étaient là avant ou la Première Civilisation, sont une race humanoïde ancestrale, vraisemblablement la première à avoir vécue sur Terre.</p>
        <p>Contrairement à la plupart des espèces peuplant la planète bleue, les Isus étaient dotés d’une structure ADN en triple hélice. Ils sont à l’origine des Fragments d’Éden et de l’espèce humaine, qui leur faisait office de main-d’œuvre. Les premiers hommes ignorant leur véritable nature, les Précurseurs furent bientôt vénérés tels des dieux par ces derniers.</p>
    </article>
    
    <article>
        <h3>Les Fragments d'Éden</h3>
        <img src="<?= ASSETS ?>img/Pomme Eden.png" alt="Pomme d'Éden">
        <p>Les Fragments d’Éden sont les reliques technologiques des Précurseurs. Ils sont aussi anciens que sophistiquer sur le plan technologique et leur création précède les premières civilisations humaines. Les informations sur les Fragments d’Éden sont rares, mais ils semblent pourtant évident que certains d’entre eux ont été conçus pour interagir et manipuler la physiologie humaine. Les vestiges de cette technologie complexe ont continué à influencé l’histoire de l’Humanité jusqu’aux temps modernes.</p>
    </article>
</section>