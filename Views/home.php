<section id="slider">
    <img src="" alt="">
    <a href="" title=""></a>
    <ul id="dots"></ul>
</section>

<section class="apropos">
    <h2>A propos de la série</h2>
    <p>Assassin’s Creed (« Le Credo des Assassins » en français) est une série de jeux vidéo historique d’action-aventure/infiltration puis d’action-aventure/RPG en monde ouvert développée et éditée par Ubisoft.</p>
    <p>Le scénario de la franchise repose sur le conflit millénaire et idéologique opposant la Confrérie des Assassins à l’Ordre des Templiers, deux organisations secrètes œuvrant pour la liberté pour les uns, pour la stabilité mondiale pour les autres, dans l’espoir de forger un monde meilleur.</p>
    <p>Le protagoniste principal de la première moitié de la série est Desmond Miles, un jeune Américain dont la lignée descend directement des membres de la Confrérie des Assassins On découvre ainsi dans le premier épisode Altaïr Ibn-La’Ahad, un jeune Maître Assassin ayant vécu en Terre Sainte durant le Troisième Croisade, dans le second volet et ses deux intermédiaires, Ezio Auditore da Firenze, un noble Italien de la Renaissance, suivi de Ratonhnhaké:ton dit Connor, un Assassin amérindien dans le troisième volet.</p>
    <p>Depuis Assassin’s Creed III, la série ne suit plus l’histoire de Desmond. Cette deuxième sage, qui pourrait être nommée « saga Hélix », débute avec Black Flag et Edward Kenway, un Assassin pirate. Suivront Arno Dorian pendant la Révolution Française et les jumeaux Jacob et Evie Frye dans le Londres de l’époque Victorienne.</p>
    <p>Depuis Assassin’s Creed Origins, on suit incarne Layla Hassan, une chercheuse américaine, ancienne employée d’Abstergo Indutries et membre de Confrérie des Assassins. On la suit en train de découvrir les mémoires génétiques de Bakek de Siwa et Aya d’Alexandrie en Égypte antique au cours du règne de Ptolémée XIII et d’Alexios/Kassandra en Grèce Antique lors de la guerre du Péloponnèse.</p>
</section>

<section class="chronologie">
    <h2>Chronologie de la série</h2>
    <article>
        <h3>Assassin's Creed Valhalla</h3>
        <a href="<?= HOST ?>AC-Valhalla" title="Assassin's Creed Valhalla"><img src="<?= ASSETS ?>img/jaquettes/AC Valhalla Jaquette.jpg" alt="Assassin's Creed Valhalla"></a>
        <p>Date de sortie : 17 Novembre 2020</p>
        <p>Plateforme : Xbox Series X, Xbox One, PS5, PS4, PC, Stadia</p>
    </article>
    <article>
        <h3>Assassin’s Creed Odyssey</h3>
        <a href="<?= HOST ?>AC-Odyssey" title="Assassin's Creed Odyssey"><img src="<?= ASSETS ?>img/jaquettes/AC Odyssey Jaquette.jpg" alt="Jaquette d'AC Odyssey"></a>
        <p>Date de sortie : 5 Octobre 2018</p>
        <p>Plateforme : Xbox One, PS4, PC</p>
    </article>
    <article>
        <h3>Assassin’s Creed Origins</h3>
        <a href="<?= HOST ?>AC-Origins" title="Assassin's Creed Origins"><img src="<?= ASSETS ?>img/jaquettes/AC Origins Jaquette.jpg" alt="Jaquette d'AC Origins"></a>
        <p>Date de sortie : 27 Octobre 2017</p>
        <p>Plateforme : Xbox One, PS4, PC</p>
    </article>
    <article>
        <h3>Assassin’s Creed</h3>
        <a href="<?= HOST ?>AC-I" title="Assassin's Creed I"><img src="<?= ASSETS ?>img/jaquettes/AC I Jaquette.jpg" alt="Jaquette d'AC I"></a>
        <p>Date de sortie : 16 Novembre 2007</p>
        <p>Plateforme : Xbox 360, PS3, PC</p>
    </article>
    <article>
        <h3>Assassin’s Creed II</h3>
        <a href="<?= HOST ?>AC-II-B-R" title="Assassin's Creed II"><img src="<?= ASSETS ?>img/jaquettes/AC II Jaquette.jpg" alt="Jaquette d'AC II"></a>
        <p>Date de sortie : 19 Novembre 2009</p>
        <p>Plateforme : Xbox 360, PS3, PC</p>
    </article>
    <article>
        <h3>Assassin’s Creed Brotherhood</h3>
        <a href="<?= HOST ?>AC-II-B-R" title="Assassin's Creed Brotherhood"><img src="<?= ASSETS ?>img/jaquettes/AC Brotherhood Jaquette.jpg" alt="Jaquette d'AC Brotherhood"></a>
        <p>Date de sortie : 18 Novembre 2010</p>
        <p>Plateforme : Xbox 360, PS3, PC</p>
    </article>
    <article>
        <h3>Assassin’s Creed Révélations</h3>
        <a href="<?= HOST ?>ACII-B-R" title="Assassin's Creed Revelations"><img src="<?= ASSETS ?>img/jaquettes/AC Revelations Jaquette.jpg" alt="Jaquette d'AC Revelations"></a>
        <p>Date de sortie : 15 Novembre 2011</p>
        <p>Plateforme : Xbox 360, PS3, PC</p>
    </article>
    <article>
        <h3>Assassin’s Creed IV Black Flag</h3>
        <a href="<?= HOST ?>AC-IV" title="Assassin's Creed IV Black Flag"><img src="<?= ASSETS ?>img/jaquettes/AC IV Jaquette.jpg" alt="Jaquette d'AC IV Black Flag"></a>
        <p>Date de sortie : 29 Octobre 2013</p>
        <p>Plateforme : Xbox360, PS3, Xbox One, PS4, Wii U, PC</p>
    </article>
    <article>
        <h3>Assassin’s Creed Rogue</h3>
        <a href="<?= HOST ?>AC-Rogue" title="Assassin's Creed Rogue"><img src="<?= ASSETS ?>img/jaquettes/AC Rogue Jaquette.jpg" alt="Jaquette d'AC Rogue"></a>
        <p>Date de sortie : 13 Novembre 2014</p>
        <p>Plateforme : Xbox 360, PS3, PC</p>
    </article>
    <article>
        <h3>Assassin’s Creed III</h3>
        <a href="<?= HOST ?>AC-III" title="Assassin's Creed III"><img src="<?= ASSETS ?>img/jaquettes/AC III Jaquette.jpg" alt="Jaquette d'AC III"></a>
        <p>Date de sortie : 31 Octobre 2012</p>
        <p>Plateforme : Xbox 360, PS3, PC</p>
    </article>
    <article>
        <h3>Assassin’s Creed Unity</h3>
        <a href="<?= HOST ?>AC-Unity" title="Assassin's Creed Unity"><img src="<?= ASSETS ?>img/jaquettes/AC Unity Jaquette.jpg" alt="Jaquette d'AC Unity"></a>
        <p>Date de sortie : 13 Novembre 2014</p>
        <p>Plateforme : Xbox One, PS4, PC</p>
    </article>
    <article>
        <h3>Assassin’s Creed Syndicate</h3>
        <a href="<?= HOST ?>AC-Syndicate" title="Assassin's Creed Syndicate"><img src="<?= ASSETS ?>img/jaquettes/AC Syndicate Jaquette.jpg" alt="Jaquette d'AC Syndicate"></a>
        <p>Date de sortie : 23 Octobre 2015</p>
        <p>Plateforme : Xbox One, PS4, PC</p>
    </article>
</section>

<script src="<?= ASSETS ?>js/slider.js" async></script>