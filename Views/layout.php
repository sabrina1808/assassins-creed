<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Site sur Assassin's Creed">
        <title><?= $title ?></title>
        <link rel="shortcut icon" href="<?= ASSETS ?>img/Logo.png" type="image/x-icon">
        <link rel="stylesheet" href="<?= ASSETS ?>css/font-awesome-4.3.0.min.css">
        <link rel="stylesheet" href="<?= ASSETS ?>css/normalize.css">
        <link rel="stylesheet" href="<?= ASSETS ?>css/main.css">
        <!--Librairie jQuery-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <!--Chargement des fichiers JS-->
        <script src="<?= ASSETS ?>js/utilities.js" async></script>
        <script src="<?= ASSETS ?>js/validationForm.js" async></script>
        <script src="<?= ASSETS ?>js/class/Basket.class.js" async></script>
    </head>

    <body>
        <header id="top">
            <a href="<?= HOST ?>index" title="Accueil"><img src="<?= ASSETS ?>img/Logo AC complet.png" alt="Logo AC"></a>

            <nav class="menu">
                <ul>
                    <li class="deroulant"><a href="#">Série de jeux</a>
                        <div id="sous-menu">
                            <ul>
                                <li><a href="<?= HOST ?>AC-I" title="Assassin's Creed I">Assassin's Creed I</a></li>
                                <li><a href="<?= HOST ?>AC-II-B-R" title="Assassin's Creed II, Brotherhood, Revelations">Assassin's Creed II, Brotherhood, Révélations</a></li>
                                <li><a href="<?= HOST ?>AC-III" title="Assassin's Creed III">Assassin's Creed III</a></li>
                                <li><a href="<?= HOST ?>AC-IV" title="Assassin's Creed IV">Assassin's Creed IV</a></li>
                                <li><a href="<?= HOST ?>AC-Rogue" title="Assassin's Creed Rogue">Assassin's Rogue</a></li>
                                <li><a href="<?= HOST ?>AC-Unity" title="Assassin's Creed Unity">Assassin's Creed Unity</a></li>
                                <li><a href="<?= HOST ?>AC-Syndicate" title="Assassin's Creed Syndicate">Assassin's Creed Syndicate</a></li>
                                <li><a href="<?= HOST ?>AC-Origins" title="Assassin's Creed Origins">Assassin's Creed Origins</a></li>
                                <li><a href="<?= HOST ?>AC-Odyssey" title="Assassin's Creed Odyssey">Assassin's Creed Odyssey</a></li>
                                <li><a href="<?= HOST ?>AC-Valhalla" title="Assassin's Creed Valhalla">Assassin's Creed Valhalla</a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="<?= HOST ?>Termes" title="Termes du jeu">Termes du jeu</a></li>
                    <li><a href="<?= HOST ?>Organisations" title="Organisations">Organisations</a></li>
                    <li><a href="<?= HOST ?>Videos" title="Les trailers de lancement">Les trailers</a></li>
                    <li><a href="<?= HOST ?>Shop" title="Boutique">Boutique</a></li>
                    <li><a href="<?= HOST ?>Contact" title="Contact">Contact</a></li>
                    <?php if(array_key_exists('Role', $_SESSION) == false) { ?>
                        <li><a href="<?= HOST ?>Login" title="Connexion">Connexion</a></li>
                    <?php } else { ?>
                        <?php if($_SESSION['Role'] == "admin") { ?>
                            <li><a href="<?= HOST ?>Administration-user" title="Administration">Administration</a></li>
                        <?php } ?>
                        <li><a href="<?= HOST ?>Account/userId/<?= $_SESSION['Id'] ?>" title="Mon compte">Mon compte</a></li>
                        <li>
                            <a href="<?= HOST ?>Logout" title="Déconnexion">Déconnexion</a>
                            <p class="welcome">Bienvenue <strong><?= $_SESSION['FirstName'] ?></strong></p>
                        </li>
                    <?php } ?>
                </ul>
            </nav>           

            <a href="#bottom" title="Pied de page"><i class="fa fa-angle-double-down"></i></a>
        </header>

        <main>
            <?= $contentPage ?>
        </main>

        <footer id="bottom">
            <a href="#top" title="Haut de page"><i class="fa fa-angle-double-up"></i></a>
            <p>Tous les droits de la franchise Assassin's Creed revienne à Ubisoft, la société qui à créer et éditée le jeu</p>
            <p><i class="fa fa-copyright"></i> 2020 - Projet personnel réalisée par Sabrina O.</p>
        </footer>

        <script src="<?= ASSETS ?>js/main.js" async></script>
    </body>
</html>