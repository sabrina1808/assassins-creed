<h2>Les Trailers de Lancement</h2>

<section class="trailer">
    <?php foreach($videos as $video): ?>
        <article>
            <h3><?= $video['Name'] ?></h3>
            <img src="<?= ASSETS ?>img/jaquettes/<?= $video['Jaquette'] ?>" alt="<?= $video['Name'] ?>">
            <p>Trailer video : <a href="<?= HOST ?>Videos-details/id/<?= $video['Id'] ?>" title="Trailer d'<?= $video['Name'] ?>">Video</a></p>
        </article>
    <?php endforeach; ?>
</section>