<h2>Les Trailer de lancement</h2>

<section class="video">
    <h3><?= $video['Name'] ?></h3>
    <video src="<?= ASSETS ?>videos/<?= $video['Link'] ?>" type="video/mp4" controls preload="metadata" poster="<?= ASSETS ?>img/slides/<?= $video['Poster'] ?>"></video>
</section>

<br>

<section class="game-page">
    <h3>Plus d'informations ?</h3>
    <p>Si vous souhaitez avoir plus d'informations concernant le jeu, cliquez <a href="<?= HOST.$video['GamePage'] ?>" title="Page d'<?= $video['Name'] ?>">ici</a></p>
</section>