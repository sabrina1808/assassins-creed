<h3>Administration des commandes</h3>

<h4>Liste des commandes</h4>
<section class="admin-order">
    <?php foreach($orders as $order): ?>
        <article>
            <h5>Numéro de la commande : <?= $order['Id'] ?></h5>
            <p><strong>Client :</strong> <?= $order['FirstName'] ?> <?= $order['LastName'] ?></p>
            <p><strong>Montant de la commande :</strong> <?= number_format($order['TotalAmount'], 2) ?> €</p>
            <p><strong>Commande passée le :</strong> <?= $order['CreationTimestamp'] ?></p>
            <a href="<?= HOST ?>Administration-order-details/orderId/<?= $order['Id'] ?>/userId/<?= $order['User_Id'] ?>" title="Détails de la commande">Voir le détail</a>
        </article>
    <?php endforeach; ?>
</section>
