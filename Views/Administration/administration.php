<h2>Panneau d'administration du site</h2>

<section class="administration">
    <a href="<?= HOST ?>Administration-user" title="Page d'administration des utilisateurs">Administration des utilisateurs</a>
    <a href="<?= HOST ?>Administration-shop" title="Page d'ajout d'article à la boutique">Ajout d'article à la boutique</a>
    <a href="<?= HOST ?>Administration-shop-list" ttile="Page de gestion des articles de la boutique">Gestion des articles de la boutique</a>
    <a href="<?= HOST ?>Administration-order" title="Page d'administration des commandes">Administration des commandes</a>
</section>