<h3>Gestion des articles de la boutique</h3>

<section class="admin-shop">
    <?php foreach($products as $product): ?>
        <article>
            <h5><?= htmlspecialchars($product['Name']) ?></h5>
            <img src="<?= ASSETS ?>img/boutique/<?= $product['Photo'] ?>" alt="<?= $product['Name'] ?>">
            <div class="caracteristques">
                <p><strong>Type :</strong> <?= htmlspecialchars($product['Type']) ?></p>
                <p><strong>Quantité en stock :</strong> <?= number_format($product['QuantityInStock']) ?></p>
                <p><strong>Prix de vente :</strong> <?= number_format($product['SalePrice'], 2) ?> €</p>
                <p>Supprimer l'article de la boutique : <a href="<?= HOST ?>Administration-shop-remove/productId/<?= $product['Id'] ?>" title="Supprimer l'article"><i class="fa fa-trash"></i></a></p>
            </div>
        </article>
    <?php endforeach; ?>
</section>
