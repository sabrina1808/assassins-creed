<h4>Ajout d'un produit à la boutique</h4>

<p>Pour ajouter un article à la boutique, remplissez le formulaire ci-dessous.</p>

<form class="formulaire" action="<?= HOST ?>Administration-shop" method="post">
    <fieldset>
        <legend><i class="fa fa-plus-circle"></i> Ajouter un produit</legend>
        <ul>
            <li>
                <label for="Name">Nom<span class="asterisk">*</span> :</label>
                <input type="text" name="Name" id="Name" value="<?= $name ?>">
                <span class="error" id="error-name"></span>
            </li>
            <li>
                <label for="Description">Description<span class="asterisk">*</span> :</label>
                <textarea name="Description" id="Description" cols="50" rows="10" placeholder="Ajouter la description..."><?= $description ?></textarea>
                <span class="error" id="error-textarea"></span>
            </li>
            <li>
            <label for="Type">Type de produit<span class="asterisk">*</span> :</label>
                <select name="Type" id="Type">
                    <option value="/" selected hidden>Selection</option>
                    <option value="Figurine">Figurine</option>
                    <option value="Mug">Mug</option>
                    <option value="Produit dérivé">Produit dérivé</option>
                    <option value="Vetements">Vetements</option>
                    <option value="Sac">Sac</option>
                </select>
                <span class="error" id="error-select"></span>
            </li>
            <li>
                <label for="Photo">Photo<span class="asterisk">*</span> :</label>
                <input type="file" name="Photo" id="Photo">
                <span class="error" id="error-photo"></span>
            </li>
        </ul>
    </fieldset>

    <fieldset>
        <legend><i class="fa fa-retweet"></i> Approvisionnement</legend>
        <ul>
            <li>
                <label for="QuantityInStock">Quantité<span class="asterisk">*</span> :</label>
                <input type="text" name="QuantityInStock" id="QuantityInStock" value="<?= $quantity ?>">
                <span class="error" id="error-quantity"></span>
            </li>
            <li>
                <label for="SalePrice">Prix de vente<span class="asterisk">*</span> :</label>
                <input type="text" name="SalePrice" id="SalePrice" value="<?= $price ?>">
                <label for="SalePrice">€</label>
                <span class="error" id="error-price"></span>
            </li>
            <li>
                <p><span class="asterisk">*</span> : Champ requis</p>
            </li>
        </ul>
    </fieldset>

    <ul>
        <li>
            <button type="submit" class="send">Ajouter</button>
            <a href="<?= HOST ?>Administration-user" title="Administration" class="cancel">Annuler</a>
        </li>
    </ul>
</form>