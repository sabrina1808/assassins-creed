<h3>Administration des utilisateurs</h3>

<h4>Liste des utilisateurs du site</h4>

<section class="admin-user">
    <?php foreach($users as $user): ?>
        <article>
            <h5><?= htmlspecialchars($user['FirstName']) ?> <?= htmlspecialchars($user['LastName']) ?></h5>
            <p><strong>Email :</strong> <?= htmlspecialchars($user['Email']) ?></p>
            <p><strong>Rôle :</strong> <?= htmlspecialchars($user['Role']) ?></p>
            <div class="gestion">
                <div>
                    <p>Mettre à jour le rôle</p>
                    <form action="<?= HOST ?>Administration-user-update/userId/<?= $user['Id'] ?>" method="post">
                    <ul>
                        <li>
                            <select name="Role" id="Role">
                                <option value="/" selected hidden>Rôle</option>
                                <option value="user">User</option>
                                <option value="admin">Admin</option>
                            </select>
                            <span class="error" id="error-select"></span>
                        </li>
                        <li>
                            <button type="submit" class="send">Mettre à jour</button>
                        </li>
                    </ul>
                    </form>
                </div>
                <div>
                    <p>Supprimer l'utilisateur</p>
                    <a href="<?= HOST ?>Administration-user-remove/userId/<?= $user['Id'] ?>" title="Supprimer l'utilisateur"><i class="fa fa-trash"></i></a>
                </div>
            </div>
        </article>
    <?php endforeach; ?>
</section>
