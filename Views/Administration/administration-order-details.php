<h3>Détails de la commande n° <?= $orderId ?></h3>

<section class="order-customer">
    <h4>Client</h4>
    <p><?= $userDetails['FirstName'] ?> <?= $userDetails['LastName'] ?></p>
    <?php if($userDetails['Phone'] != null) { ?>
        <p><?= $userDetails['Phone'] ?></p>
    <?php } else { ?>
        <p>Téléphone non renseigné</p>
    <?php } ?>
    <p><?= $userDetails['Address'] ?>, <?= $userDetails['ZipCode'] ?> <?= $userDetails['City'] ?></p>
    <p><?= $userDetails['Country'] ?></p>
</section>

<hr>

<section class="order-details">
    <h4>Commande</h4>
    <table>
        <thead>
            <th>Produit</th>
            <th>Taille</th>
            <th>Quantité</th>
            <th>Prix unitaire</th>
            <th>Prix Total</th>
        </thead>
        <tbody>
            <?php foreach($orderDetails as $details): ?>
                <tr>
                    <td><?= htmlspecialchars($details['Name']) ?></td>
                    <td><?= htmlspecialchars($details['Size']) ?></td>
                    <td><?= number_format($details['QuantityOrdered'], 0) ?></td>
                    <td><?= number_format($details['PriceEach'], 2) ?> €</td>
                    <td><?= number_format($details['QuantityOrdered']*$details['PriceEach'], 2) ?> €</td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">Montant HT :</td>
                <td><?= number_format($details['TotalAmount']/1.2, 2) ?> €</td>
            </tr>
            <tr>
                <td colspan="4">TVA (20%) :</td>
                <td><?= number_format($details['TaxAmount'], 2) ?> €</td>
            </tr>
            <tr>
                <td colspan="4">Montant TTC :</td>
                <td><?= number_format($details['TotalAmount'], 2) ?> €</td>
            </tr>
            <tr>
                <td colspan="5" class="validate-order">Validé le : <?= $details['CompleteTimestamp'] ?></td>
            </tr>
        </tfoot>
    </table>
</section>
