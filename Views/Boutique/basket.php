<h2>Votre panier</h2>

<h3>Récapitulatif de la commande</h3>
<div id="order-summary">
    <table class="basket-table">
        <thead>
            <th>Nom</th>
            <th>Taille</th>
            <th>Quantité</th>
            <th>Prix unitaire</th>
            <th>Prix total</th>
            <th></th>
        </thead>

        <tbody>
        </tbody>
        
        <tfoot>
            <tr>
                <td>Prix Total</td>
                <td></td>
                <td></td>
                <td></td>
                <td><span id="totalPrice">0.00</span> €</td>
                <td></td>
            </tr>
        </tfoot>
    </table>
</div>
<form class="formulaire basket-form" action="<?= HOST ?>Validation/userId/<?= $_SESSION['Id'] ?>" method="post">
    <input type="hidden" name="basketItems" id="basketItems" value="">
    <button type="submit" class="send">Valider la commande</button>
    <a href="<?= HOST ?>Shop" title="Boutique" class="cancel">Annuler</a>
</form>