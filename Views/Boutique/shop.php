<h2>Boutique</h2>

<section class="basket">
    <div class="basket-view">
        <h4>Votre panier</h4>
        <a href="<?= HOST ?>Basket" title="Votre panier"><i class="fa fa-shopping-cart"></i></a>
        <em class="hide" id="basket-items"></em>
    </div>
</section>

<section class="shop">
    <h3>Liste des articles disponibles</h3>

    <?php foreach($products as $product): ?>
        <article>
            <h4><?= htmlspecialchars($product['Name']) ?></h4>
            <img src="<?= ASSETS ?>img/boutique/<?= $product['Photo'] ?>" alt="<?= $product['Name'] ?>">
            <p><?= htmlspecialchars($product['Description']) ?></p>
            <p class="price">Prix : <strong><?= number_format($product['SalePrice'], 2) ?> €</strong></p>
            <form class="formulaire">
                <ul>
                    <li>
                        <?php if($product['Type'] == "Vetements"): ?>
                        <label for="size">Taille :</label>
                        <select name="size" id="size-<?= $product['Id'] ?>">
                            <option value="/" disabled selected hidden>Taille</option>
                            <option value="S">S</option>
                            <option value="M">M</option>
                            <option value="L">L</option>
                            <option value="XL">XL</option>
                        </select>
                        <span class="error" id="alert-select-<?= $product['Id'] ?>"></span>
                        <?php endif; ?>
                    </li>
                    <li>
                        <label for="quantity">Quantité :</label>
                        <input type="text" name="quantity" id="quantity-<?= $product['Id'] ?>">
                        <span class="error" id="alert-quantity-<?= $product['Id'] ?>"></span>
                    </li>
                    <li>
                        <button class="addBasket" data-name="<?= $product['Name'] ?>" data-price="<?= $product['SalePrice'] ?>" data-id="<?= $product['Id'] ?>" data-type="<?= $product['Type'] ?>">Ajouter au panier</button>
                    </li>
                </ul>
            </form>
        </article>
    <?php endforeach; ?>
</section>