<h2>Commande validée</h2>

<p>Nous vous remerçions pour la commande que vous venez de passez. Elle vous sera livrée dans les meilleurs délais à l'addresse indiquée ci-dessous.</p>

<h3>Livraison</h3>
<p><?= $userInfo['FirstName'] ?> <?= $userInfo['LastName']?></p>
<p><?= $userInfo['Email'] ?></p>
<p><?= $userInfo['Address'] ?>, <?= $userInfo['ZipCode'] ?> <?= $userInfo['City'] ?></p>
<p><?= $userInfo['Country'] ?></p>
<?php if($userInfo['Phone'] != null) { ?>
    <p><?= $userInfo['Phone'] ?></p>
<?php } else { ?>
    <p>Téléphone non renseigné</p>
<?php } ?>