<h2>Les Organisations</h2>

<section class="organisations">
    <h3>Le Culte de Kosmos</h3>
    <img src="<?= ASSETS ?>img/Culte de Kosmos.jpg" alt="Culte de Kosmos">
    <p>Cette société secrète présente dans la Grèce antique cherchait à récupérer, à protéger et à contrôler les Fragments D’Éden qui lui offraient le pouvoir de manipuler l’Histoire. Elle fut initialement formée par les membres d’un groupe d’adeptes du célèbre mathématicien Pythagore. Ce dernier dédia sa vie à l’étude des chiffres et des formules à même d’expliquer les mystères de l’univers.</p>
    <p>Pythagore avait découvert l’existence des artefacts Isus et des traces de savoir de la Première Civilisation qu’ils portaient. Hermès lui-même lui transmit son Bâton, lui offrant une durée de vie amplement prolongée. Conscient du pouvoir que pourraient conférer les artefacts à leur propriétaire, Pythagore faisait preuve d’une discipline exemplaire et en attendait autant de ses élèves. Mais un certain nombre d’entre eux fut de plus en plus obsédé par leur désir de posséder un pouvoir illimité et forma leur propre organisation.</p>
    <p>La symbolique du Culte de Kosmos emprunte au mythe de Python, un serpent géant antique qui protégeait une pierre au cœur du monde, à Delphes. Apollon élimina le serpent et l’Oracle de Delphes fut installée dans le Temple d’Apollon, bâti à cette occasion. Le Culte savait qu’en contrôlant la Pythie, il contrôlait le destin de la population grecque, qui se fiait à ses prédictions. Les 42 membres du Culte de Kosmos se réunissaient dans la grotte de Gaïa, située sous le Temple, pour consulter un artefact qu’ils baptisèrent la Pyramide et accomplir leurs rituels. Parfaitement anonymes, les membres du Culte portaient une longue robe noire et des masques de théâtre grecs décorés.</p>
    <p>La Pyramide offrait des visions du passé, du présent et de futurs hypothétiques. Deimos et Kassandra perçurent ainsi les souvenirs de Kassandra lors de la nuit où Deimos, encore appelé Alexios, fut jeté du Mont Taygète.</p>
    <p>La Pyramide pouvait également projeter des vois mémorisées afin d’attirer à elle les descendants des Isus. C’est ainsi que le son de la voix de sa mère permit à Kassandra de trouver la Pyramide. Par ailleurs, celle-ci ne fonctionnait qu’avec les descendants des membres de la Première Civilisation. Les cultistes voulaient – et ils finirent par y arriver – prendre le contrôle d’Athènes en faisant tomber Périclès, le père de la démocratie, pour le remplacer par Cléon, seigneur de guerre et membre du Culte.</p>
</section>

<section class="organisations">
    <h3>Medjaÿ d'Egypte</h3>
    <img src="<?= ASSETS ?>img/AC Origins -  Badge de medjaÿ.png" alt="Badge de Medjaÿ">
    <q>En tant que Medjaÿ, tu ne seras pas seulement tenant de ces principes. Tu deviendras ces principes. Tu comprends ?</q>
    <cite> - Nitokris, Haute Prêtresse de Thèbes</cite>
    <p>Entre les 18e et 19e dynasties (l’ère du Nouveau Royaume), les Medjaÿ d’Egypte agissaient comme protecteurs des intérêts du pharaon – les Cités capitales et les tombeaux entre autres. Les Medjaÿ étaient nomades – se rendant là où on avait besoin d’eux – et le titre se transmettait par le sang.</p>
    <p>Entre autres fonctions, ils pouvaient être éclaireurs, gardiens de palais, employés de temples ou soldats. Au moment de la dynastie lagide, les Medjaÿ avaient été en grande partie remplacés par des troupes militaires de type romaines au service du pharaon. Le code des Medjaÿ les obligeait à voler à l’aide de ceux qui en avaient besoin et d’assassiner les mécréants. Ils juraient de protéger les innocents, de ne pas compromettre l’intégrité de l’organisation des Medjaÿ et ne trahir ni les dieux, ni la justice. Quand l’Egypte et Rome s’unirent, Cléopâtre démit Bayek de ses fonctions. Les Medjaÿ appartenaient désormais au passé, mais leur philosophie imprégna celle de la Confrérie.</p>
</section>

<section class="organisations">
    <h3>"Ceux que l'on ne voit pas"</h3>
    <img src="<?= ASSETS ?>img/logo/Logo AC Origins.png" alt="Logo de Assassin's Creed Origins">
    <p>Aya trouva que l’alliance entre Cléopâtre et Rome compromettait certaines des choses qui lui étaient précieuses : la sécurité et la liberté de l’Egypte et de son peuple. Elle comprit que les manigances de l’Ordre des Anciens – qui manipulait le pouvoir dans l’ombre plutôt que d’œuvrer pour le bien du peuple d’Egypte. – dépassaient les frontières du royaume et s’étendaient à Rome et au-delà.</p>
    <p>L’amertume d’Aya et de Bayek leur permit d’offrir un sens à leur lutte – défendre la liberté des peuples et résister aux dictatures – et d’en faire un objectif concret. Leur engagement dans la lutte et les méthodes d’action poussèrent à fonder une nouvelle organisation. Des idéaux communs – plutôt qu’une trace héréditaire – assureraient à ses membres une place dans cette organisation. C’est ainsi que naquirent « Ceux qu’on ne voit pas », le groupe de combattant finit par donner naissance à la Confrérie des Assassins.</p>
    <p>Aya s’exila à Rome pour œuvrer avec Brutus et Cassius à la destitution de César. L’assassinat qu’ils fomentèrent – une action publique qui engendra une panique généralisée chez les citoyens qui auraient dû au contraire se sentir libérés à la mort de leur dictateur – confirma à Aya que « Ceux qu’on ne voit pas » devait agir dans l’ombre. Ce constat donna naissance à l’un des principes fondateurs de l’organisation. En éliminant les oppresseurs anonymement, ses membres permettaient au peuple de choisir plus facilement sa voie. Afin de se montrer fidèle à ce principe, Aya changea de nom et se rebaptisa Amunet, du nom de la déesse égyptienne de l’invisibilité.</p>
    <p>Amunet et Bayek réalisèrent qu’ils devaient définir les objectifs qu’ils attribuèrent à « Ceux qu’on ne voit pas », pour que les membres ne dévoilent pas l’existence de leur groupe et de son but, et qu’ils puissent se concentrer sur la protection des innocents sans se sentir eux-mêmes attirés par le pouvoir. Ils avaient conscience qu’avec leur expansion, « Ceux qu’on ne voit pas » se propagèrent sur de nouveaux territoires. Afin de permettre aux différents groupes de conserver les mêmes principes, Amunet et Bayek codifièrent les bases de la philosophie et des objectifs de l’organisation qu’ils avaient créée.</p>
</section>

<section class="organisations">
    <h3>L'Ordre des Anciens</h3>
    <img src="<?= ASSETS ?>img/Ordre des Anciens.jpg" alt="Ordre des Anciens">
    <q>Vous connaissez l’Ordre ? Avez-vous entendu parler de notre travail ? […] Nous sommes une société secrète dont la force et la stature sont en pleine expansion. Notre but est de fonder une nouvelle société plus moderne. Une société qui s’éloignerait des modèles établis.</q>
    <p>Formé, il y a des millénaires, l’Ordre des Anciens gagna rapidement en influence dans la Perse et l’Egypte ancienne avant de s’insinuer en Grèce pendant et après la guerre du Péloponnèse au Ve siècle av. JC.</p>
    <p>En Egypte, pendant la période de la dynastie lagide, l’Ordre des Anciens tenait en réalité les rênes du pouvoir. Groupe aussi discret que sinistre, l’Ordre des Anciens cherchaient à contrôler l’Egypte (et au-delà). Pour ce faire, ils se hissèrent à des postes importants au sein des appareils militaires et étatiques du pharaon, mais aussi dans des temples et au cœur des gouvernements étrangers. Ils œuvraient pour le maintien de la paix et de l’ordre en soumettant le peuple égyptien à une oppression politique, sociale, religieuse et économique. L’Ordre contrôlait les faibles et n’eut aucun mal à influence le pharaon Ptolémée XIII. Quand ce dernier fut détrôné par sa sœur, Cléopâtre, qui était soutenue par Jules César, l’Ordre décida de changer de tactique.</p>
    <p>Afin de pouvoir consolider son pouvoir, l’Ordre collectait des objets sacrés. Le mystère du Temple de Siwa était censé débloquer l’accès à des informations qui permettraient de contrôler le temps et maîtriser le pouvoir de l’esprit humain. Le monument devint la première cible de l’Ordre. Pour l’atteindre, ils étaient prêts à tuer, quiconque se mettrait sur leur chemin.</p>
    <p>Les membres de l’Ordre des Anciens portaient des masques lors de leurs réunions. Il s’agissait d’une démonstration symbolique de leur loyauté. Les membres dirigeants de l’Ordre des Anciens portaient des noms de code basés sur les animaux de l’Egypte antique comme l’Hippopotame, l’Ibis et le Crocodile. L’Ordre choisit de symboliser son organisation par un serpent, ce qui lui fit gagner le surnom du « Serpent ».</p>
</section>

<section class="organisations">
    <h3>Les Assassins</h3>
    <img src="<?= ASSETS ?>img/Credo des Assassins.gif" alt="Crédo des Assassins">
    <q>Nous agissons dans l’ombre pour éclairer le monde. Nous sommes des Assassins.</q>
    <p>La Confrérie des Assassins est une organisation qui a juré de protéger le libre-arbitre de l’Humanité et de s’assurer que cette liberté s’applique sans interférence. Ils se battent pour ceux qui n’ont pas l’opportunité de changer la société ou de lever la voix contre les abus de pouvoir, œuvrant stratégiquement pour neutraliser et éliminer les oppresseurs et les tyrans.</p>
    <p>Les Assassins s’opposent aux Templiers, qui manipulent les masses pour changer le cours de l’Histoire.</p>
    <p>Les valeurs de la Confrérie plongent leur racine dans la Grèce antique, et l’organisation qui l’a précédée a été fondée en Égypte, vers la fin de la dynastie lagide. Elle était alors baptisée « Ceux qu’on ne voit pas ». La Confrérie des Assassins est toujours active aujourd’hui.</p>

    <article>
        <h4>Le Crédo des Assassins</h4>
        <q>Le Crédo de la Confrérie des Assassins nous apprend que rien ne nous est interdit. À une époque, je pensais que ça sous-entendait que nous étions libres d’agir comme nous l’entendions. De poursuivre un idéal coûte que coûte. Aujourd’hui, je comprends. Ça ne nous donne aucun droit. Le Crédo est un avertissement. Les idéaux laissent facilement la place au dogme. Le dogme mène au fanatisme. Il n’est pas de pouvoir supérieur qui juge nos actes. Il n’est pas d’être suprême qui nous observe et nous punit car nous avons péché. Au final, nous sommes les seuls gardiens de nos propres obsessions. Nous seuls pouvons décider si le tribut de nos choix est trop cher à payer. Nous nous prenons pour des rédempteurs, des justiciers, des sauveurs. Nous menons une guerre contre ceux qui s’opposent à nous, et ils mènent une guerre contre nous. Nous rêvons de laisser notre empreinte sur le monde…même si nous nous livrons à un combat qui ne sera répertorié dans aucun livre d’histoire. Tout ce que nous faisons, tout ce que nous sommes, commence et termine avec nous.</q>
        <cite> - Arno Dorian</cite>
        <p>Au cœur de la philosophie des Assassins résident les Trois Principes du Crédo des Assassins. Ceux-ci se reposent sur les règles écrites par Bayek de Siwa et Aya (Amunet) quand ils décidèrent de fonder « Ceux qu’on ne voit pas ».</p>
        <p>Les Trois Préceptes :</p>
        <ol>
            <li>Ta lame épargnera la chair de l’innocent</li>
            <li>Tu te cacheras au cœur de la foule</li>
            <li>Tu ne mettras jamais en danger la Confrérie</li>
        </ol>
        <p>Le <em>Premier Précepte</em> assure l’honneur de la Confrérie : un Assassin doit faire preuve de retenue et ne tuer que si c’est nécessaire.</p>
        <p>Le <em>Deuxième Précepte</em> confère toute sa force à la Confrérie : un Assassin doit toujours faire preuve de finesse et savoir se mêler discrètement à la foule pour ne faire qu’un avec elle.</p>
        <p>Le <em>Troisième Précepte</em> assure la survie de la Confrérie : un Assassin doit être intègre et ne jamais constituer une menace pour la Confrérie.</p>
        <p>Les deux premiers Préceptes permettent aux Assassins de se différencier de leurs ennemis, les Templiers. Si ces derniers cherchent apparemment à faire régner l’ordre et la paix en contrôlant les masses, les Assassins respectent avant tout la force du peuple, la force du plus grand nombre. La symbiose entre le peuple et la Confrérie lui permet d’agir parmi le peuple et grâce au peuple. La foule, souvent sans le savoir, fournit aux Assassins le moyen idéal de se cacher des Templiers. En retour, les Assassins respectent la vie des innocents. Une adhésion sans faille à ces « Préceptes » a permis à la Confrérie de traverser les siècles et les Assassins qui ne respectent pas subissent rapidement les lourdes conséquences de leur affront. C’est en cela que le <em>Troisième Précepte</em> est le plus important. L’allégeance à la Confrérie prédomine sur toutes les allégeances personnelles.</p>
    </article>

    <article>
        <h4>Les Trois Ironies :</h4>
        <p>Au vu de cette discordance, Altaïr créa les Trois Ironies, une série d’assertions supposées répondre aux Trois Préceptes.</p>
        <ol>
            <li>Nous cherchons la paix, mais nous l'atteindrons par le meurtre</li>
            <li>Nous cherchons à éveiller les consciences, mais nous obéissons à un maître et à des règles strictes</li>
            <li>Nous cherchons à révéler les dangers d’une foi aveugle, mais nous en sommes nous-même victimes</li>
        </ol>
        <p>Avec ces trois assertions, Altaïr soulignait la nature contradictoire des méthodes des Assassins. Comme le fait remarquer le Codex d’Altaïr, les Assassins adaptent toujours leurs principes pour œuvrer pour le bien de tous. De nombreux Assassins ont du mail à opérer dans le cadre d’une structure aussi paradoxale et certains se sont déjà rebellés en exprimant leur insatisfaction.</p>
        <p>Ces contradictions inhérentes, alliées au respect des Trois Principes, poussent un Assassin à remettre systématiquement en question ses méthodes d’intervention. Rien n’est jamais acquis et aucune situation ne devra être traitée comme la précédente, même si de prime abord, elles se ressembleraient. Il faut toujours déterminer la meilleure voie à suivre, et les raisons de la suivre.</p>
    </article>

    <article>
        <h4>Structure</h4>
        <p>Au moment de la Troisième Croisade, la Confrérie la plus importante se trouvait à Masyaf, jusqu’à Altaïr la démantèle au XIIIe siècle pour disséminer des Guildes aux quatre coins du monde. Chaque Guilde était supervisée par un Maître Assassin et organisée selon une hiérarchie où les recrues progressaient d’apprenti à Assassin confirmé.</p>
        <p>La Confrérie moderne est organisée en plus petites cellules d’Assassins. Chaque ville – ou villes stratégiquement placées – possède sa propre cellule. Le concept de Guildes plus centralisées e été écarté. Les cellules fonctionnent comme des petits groupes isolés de résistants, un type d’organisation qui s’oppose à la structure très rigide de l’Ordre des Templiers. Certaines cellules sont des camps isolés, des communautés d’Assassins s’entrainant sans relâche loin des villes et de la proximité des Templiers. C’est dans ce genre de camp de Desmond Miles a grandi.</p>
        <p>Le chef de la Confrérie s’appelle le Mentor. À une époque, chaque Guilde possédait son propre Mentor, mais à partir du XXe siècle, un seul Mentor fut chargé de superviser la Confrérie entière. L’identité et la localisation du Mentor devaient rester secrètes, même pour les Assassins. Pourtant, le dernier Mentor fut démasqué et assassiné à Dubaï par Daniel Cross, un agent Templier dormant.</p>
        <p>Gavin Banks et William Miles se partagèrent les responsabilités de la tâche après la Grande Purge alors que la Confrérie peinait à se remettre et à se réorganiser après ce coup fatal. Depuis 2015, Miles est le nouveau Mentor de la Confrérie mondiale des Assassins.</p>
    </article>
</section>

<section class="organisations">
    <h3>Les Templiers</h3>
    <img src="<?= ASSETS ?>img/Croix des Templiers.png" alt="Croix des Templiers">
    <q>Que le père de la sagesse nous guide.</q>
    <p>L’Ordre des Templiers fut d’abord une société secrète baptisée l’Ordre des Anciens. Le groupe fut formé, il y a des millénaires et prit le pouvoir en Perse, en Egypte et au-delà. Quand Kassandra affaiblit puis finit par démanteler le Culte de Kosmos au Ve siècle av. JC, elle permit malgré elle à l’Ordre des Anciens de s’infiltrer en Grèce. Au Moyen-Age, l’Ordre des Templiers se transforma officiellement en ordre religieux avant de retrouver l’anonymat après avoir été accusé d’hérésie. Les Templiers réapparaîtront publiquement au XXe siècle derrière la façade d’Abstergo Industries.</p>
    <p>Les Templiers cherchent à imposer à l’Humanité leur vision d’un monde parfait. Un monde d’ordre et d’autorité. L’organisation s’est ainsi fixé des objectifs précis, et prône la discipline et le contrôle des masses pour les atteindre. Leur approche les oppose radicalement aux idéaux de la Confrérie des Assassins.</p>
    <p>L’Ordre des Templiers plébiscite la centralisation du commandement et du gouvernement, en manipulant dans l’ombre les révolutions et les soulèvements afin de transformer la société dans leur propre intérêt. Pour les Templiers, le libre-arbitre et le développement désordonné sont synonymes de chaos. Seule une direction consciencieuse pourra apporter au monde paix et équilibre.</p>

    <article>
        <h4>L'ordre contre le chaos</h4>
        <p>L’idéologie des Templiers se base sur l’idée que la structure et l’ordre permettent à l’Humanité de transcender ses instincts les plus primaires, et que le libre-arbitre et une autonomie trop développée ne peuvent aboutir qu’à une société chaotique et disharmonieuse. Les masses ne peuvent se gouverner elles-mêmes et des individus éveillés doivent les guider afin d’établir une société œuvrant dans l’intérêt de tous. À l’instar des Assassins, les Templiers aspirent à la paix ; mais ce sont les moyens que ces groupes mettent en œuvre pour l’obtenir qui les différencient.</p>
    </article>

    <article>
        <h4>Abstergo Industries</h4>
        <img src="<?= ASSETS ?>img/Logo Abstergo Industries.png" alt="Logo d'Abstergo Industries">
        <p>Abstergo Industries représente la façade publique de l’Ordre des Templiers. Fondée en 1937, pour faciliter l’influence sur l’économie mondiale Abstergo, est présente au XXIe siècle dans plusieurs secteurs, comme l’industrie pharmaceutique ou les communications. Abstergo est à l’origine de recherches et développements majeurs dans le monde occidental. En finançant les gouvernements étrangers et en investissant dans d’importantes sociétés, Abstergo a peu à peu renforcé son influence sur la propagation du savoir et des ressources et sur la collecte de données qui permettront à l’Ordre des Templiers d’atteindre ses objectifs.</p>
    </article>

    <article>
        <h4>Recherches sur les Précurseurs</h4>
        <p>Les recherches des Templiers se concentrent sur la civilisation des Précurseurs, aussi connus sous le nom d’Isus. Cette civilisation a précédé l’Humanité et des informations récoltées par les Templiers révèlent qu’il s’agissait d’une société très structurée, analytique et ordonnée, à l’intelligence très avancée. L’Ordre des Templiers utilise la technologie de l’Animus, développée par Abstergo, pour recueillir les informations et les trouver les artefacts légués à l’Histoire par la civilisation des Précurseurs. Ils cherchent ainsi à localiser les Fragments d’Éden crée par les Précurseurs afin d’exploiter leur pouvoir pour imposer leur volonté sur l’Humanité entière.</p>
    </article>
</section>