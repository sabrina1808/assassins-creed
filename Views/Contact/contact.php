<h2>Contact</h2>

<p>Si vous souhaitez laisser un message, merci de remplir le formulaire ci-dessous.</p>

<?php if($response != null): ?>
    <p class="mail-send"><?= $response ?></p>
<?php endif; ?>

<form class="formulaire" method="post">
    <fieldset>
        <legend><i class="fa fa-paper-plane"></i> Envoyer un message</legend>
        <ul>
            <li>
                <label for="FirstName">Prénom<span class="asterisk">*</span> :</label>
                <input type="text" name="FirstName" id="FirstName" placeholder="Votre prénom">
                <span class="error" id="error-firstname"></span>
            </li>
            <li>
                <label for="LastName">Nom<span class="asterisk">*</span> :</label>
                <input type="text" name="LastName" id="LastName" placeholder="Votre nom">
                <span class="error" id="error-lastname"></span>
            </li>
            <li>
                <label for="Email">Email<span class="asterisk">*</span> :</label>
                <input type="email" name="Email" id="Email" placeholder="Votre email">
                <span class="error" id="error-email"></span>
            </li>
            <li>
                <label for="Message">Votre message<span class="asterisk">*</span> :</label>
                <textarea name="Message" id="Message" cols="50" rows="10" placeholder="Tapez votre message ici..."></textarea>
                <span class="error" id="error-textarea"></span>
            </li>
            <li>
                <p><span class="asterisk">*</span> : Champ requis</p>
            </li>
        </ul>
    </fieldset>
    <ul>
        <li>
            <button type="submit" class="send">Envoyer</button>
            <a href="<?= HOST ?>Home" title="Accueil" class="cancel">Annuler</a>
        </li>
    </ul>
</form>