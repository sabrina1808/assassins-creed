<?php

//Classe qui va permettre de gérer l'affichage des pages phtml
class View {
    //Définition des variables permettant de définir le contenu de la page
    private $title;
    private $folder;
    private $template;
    private $content;

    //Définition du constructeur de la classe
    public function __construct($title, $folder, $template, $content = null) {
        //On affecte les valeurs des variables $title, $folder, $template et $content aux varaibles de la classe View
        $this->title = $title;
        $this->folder = $folder;
        $this->template = $template;
        $this->content = $content;
    }

    //Méthode permettant d'afficher la page
    public function renderView($params = array()) {
        //On utilise la fonction extract pour créer de manière dynamique la variable qui correspond au paramètre que l'on a renseigné
        extract($params);

        //On affecte les valeurs de title, folder, template et content aux variables qui vont permettre d'affiché la page
        $title = $this->title;
        $folder = $this->folder;
        $template = $this->template;
        $content = $this->content;

        //On commence la mise en tampon du contenu de la page
        ob_start();
        //Si la variable $folder est différente de "", on include le template qui se trouve dans le dossier indiqué par la variable $folder
        if($folder != "") {
            //On include le contenu de la page
            include_once VIEWS.$folder.'/'.$template.".php";
            //Si la variable $content est non nulle, on fait un include de son contenu (valable uniquement pour les pages d'administration)
            if($content != null) {
                include_once VIEWS.$folder.'/'.$content.".php";
            }
        }
        else {
            //Si la variable $folder est égale "", on include le template qui se trouve à la racine du dossier VIEWS
            include_once VIEWS.$template.".php";
        }
        
        //On stocke le contenu de la page dans la variable $contentPage lorsque l'on vide le contenu tampon et on l'affiche dans le layout
        $contentPage = ob_get_clean();

        //On include le layout
        include_once VIEWS."layout.php";
    }
}