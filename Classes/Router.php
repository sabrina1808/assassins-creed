<?php

//Classe qui va permettre de créer les routes et de trouver le controller qui correspond
class Router {
    //Déclaration d'une variable privée qui contiendra la requête
    private $request;
    //Déclaration d'un tableau qui contiendra toutes les routes (nom de la requête => ["dossier" => nom du dossier, "controlleur" => nom du controller, "method" => nom de la méthode])
    private $routes = [
        "Home"                          => ["folder" => "", "controller" => "Home", "method" => "view"],
        "AC-I"                          => ["folder" => "Serie-jeux", "controller" => "AC_I", "method" => "view"],
        "AC-II-B-R"                     => ["folder" => "Serie-jeux", "controller" => "AC_II_B_R", "method" => "view"],
        "AC-III"                        => ["folder" => "Serie-jeux", "controller" => "AC_III", "method" => "view"],
        "AC-IV"                         => ["folder" => "Serie-jeux", "controller" => "AC_IV", "method" => "view"],
        "AC-Odyssey"                    => ["folder" => "Serie-jeux", "controller" => "AC_Odyssey", "method" => "view"],
        "AC-Origins"                    => ["folder" => "Serie-jeux", "controller" => "AC_Origins", "method" => "view"],
        "AC-Rogue"                      => ["folder" => "Serie-jeux", "controller" => "AC_Rogue", "method" => "view"],
        "AC-Syndicate"                  => ["folder" => "Serie-jeux", "controller" => "AC_Syndicate", "method" => "view"],
        "AC-Unity"                      => ["folder" => "Serie-jeux", "controller" => "AC_Unity", "method" => "view"],
        "AC-Valhalla"                   => ["folder" => "Serie-jeux", "controller" => "AC_Valhalla", "method" => "view"],
        "Organisations"                 => ["folder" => "Organisations", "controller" => "Organisations", "method" => "view"],
        "Termes"                        => ["folder" => "Termes", "controller" => "Termes", "method" => "view"],
        "Videos"                        => ["folder" => "Videos", "controller" => "Videos", "method" => "videoList"],
        "Videos-details"                => ["folder" => "Videos", "controller" => "VideosDetails", "method" => "videoSelected"],
        "Shop"                          => ["folder" => "Boutique", "controller" => "Shop", "method" => "showProductList"],
        "Basket"                        => ["folder" => "Boutique", "controller" => "Basket", "method" => "showBasket"],
        "Validation"                    => ["folder" => "Boutique", "controller" => "Validation", "method" => "validateOrder"],
        "Contact"                       => ["folder" => "Contact", "controller" => "Contact", "method" => "sendMail"],
        "Account"                       => ["folder" => "Account", "controller" => "Account", "method" => "myInformations"],
        "Account-update"                => ["folder" => "Account", "controller" => "Account_Update", "method" => "updateInformations"],
        "Account-delete"                => ["folder" => "Account", "controller" => "Account_Delete", "method" => "deleteAccount"],
        "Administration-user"           => ["folder" => "Administration", "controller" => "Administration_User", "method" => "userList"],
        "Administration-user-update"    => ["folder" => "Administration", "controller" => "Administration_User_Update", "method" => "updateRole"],
        "Administration-user-remove"    => ["folder" => "Administration", "controller" => "Administration_User_Remove", "method" => "removeUser"],
        "Administration-shop"           => ["folder" => "Administration", "controller" => "Administration_Shop", "method" => "addProductOnShop"],
        "Administration-shop-list"      => ["folder" => "Administration", "controller" => "Administration_Shop_List", "method" => "productsList"],
        "Administration-shop-remove"    => ["folder" => "Administration", "controller" => "Administration_Shop_Remove", "method" => "removeProduct"],
        "Administration-order"          => ["folder" => "Administration", "controller" => "Administration_Order", "method" => "orderList"],
        "Administration-order-details"  => ["folder" => "Administration", "controller" => "Administration_Order_Details", "method" => "orderDetails"],
        "Login"                         => ["folder" => "User", "controller" => "Login", "method" => "connection"],
        "Register"                      => ["folder" => "User", "controller" => "Register", "method" => "newUser"],
        "Logout"                        => ["folder" => "User", "controller" => "Logout", "method" => "disconnect"],
        "ChangePassword"                => ["folder" => "User", "controller" => "ChangePassword", "method" => "modifyPassword"],
        "ForgotPassword"                => ["folder" => "User", "controller" => "ForgotPassword", "method" => "sendMailForModifyPassword"]
    ];

    //Définition du controlleur de la classe qui servira uniquement pour l'affectation de request
    public function __construct($request) {
        //On affecte à la variable request de la classe Router le contenu de la variable $request
        $this->request = $request;
    }

    //Méthode permettant de récupérer la route demandée
    public function getRoute() {
        //On utilise la fonction explode pour scindé la chaine de caractère à partir du "/"
        $elements = explode('/', $this->request);

        //On renvoie le premier élément du tableau $elements
        return $elements[0];
    }

    //Méthode permettant de récupérer les paramètres de la route demandée
    public function getParams() {
        //On crée une variable $params et on la définit comme null 
        $params = null;

        //Extraction des données passée en GET dans l'url
        //On utilise la fonction explode pour scindé la chaine de caractère à partir du "/"
        $elements = explode('/', $this->request);
        //On utilise la fonction unset pour retirer le premier paramètre (qui correspond à la route);
        unset($elements[0]);

        //On parcours le tableau $elements pour faire correspondre les deux éléments restants
        for($i = 1; $i < count($elements); $i++) {
            $params[$elements[$i]] = $elements[$i+1];
            $i++;
        }

        //Extraction des paramètres envoyées en POST
        //Si on envoye des valeurs à l'aide de $_POST
        if($_POST) {
            //Pour chaque valeur, on affecte la clé qui correspond à la valeur dans le tableau $params
            foreach($_POST as $key => $value) {
                $params[$key] = $value;
            }
        }

        //On renvoie le résultat de la variable $params
        return $params;
    }

    //Méthode permettant de faire appel au bon controller lors de la requête
    public function renderController() {
        //On récupere la route à l'aide de la fonction getRoute
        $route = $this->getRoute();
        //On recupère les paramètres de la route à l'aide de la fonction getParams
        $params = $this->getParams();

        //On vérifie si la requête correspond à une des clé du tableau $routes
        if(array_key_exists($route, $this->routes)) {
            //Si la clé existe, on récupère le controlleur dans le sous-dossier et on affecte celui qui correspond à la requête à la variable $controller ainsi que la méthode que l'on souhaite utilisé à la variable $method
            $folder = $this->routes[$route]['folder'];
            $controller = $this->routes[$route]["controller"];
            $method = $this->routes[$route]["method"];

            //On gére ici l'include du controller plutôt que le dossier config pour pouvoir prendre ne compte les sous-dossiers présent dans le dossier Controllers
            include_once CONTROLLERS.$folder.'/'.$controller.'.php';

            //On instancie le controlleur et on appel la méthode que l'on souhaite utilisé
            $currentController = new $controller();
            $currentController->$method($params);
        }
        else {
            //Affichage d'une erreur 404
            header('HTTP/1.0 404 Not Found');
            exit();
        }
    }
}