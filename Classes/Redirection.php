<?php

//Classe permettant de gérer les redirections
class Redirection {
    //Méthode permettant de faire la redirection vers la page spécifiée
    public function redirect($route) {
        //Redirection vers la page demandée avec la variable $route
        header('Location: '.HOST.$route);
        exit();
    }
}