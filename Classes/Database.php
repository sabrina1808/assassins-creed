<?php

//Classe permettant de gérer la connexion et les requètes à la base de donnée
class Database {
	//Déclaration d'une variable privée pour le pdo
	private $pdo;

	public function __construct()
	{
		//Déclaration du PDO pour la connexion à la base de donnée
		$this->pdo = new PDO('mysql:host=localhost;dbname=assassins-creed', 'root', '');

		$this->pdo->exec('SET NAMES UTF8');

		$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	//Fonction permettant d'effectuer les requêtes SQL d'ajout, de suppression, ...
    //La variable $sql va permettre d'effectuer les requêtes tandis que la variable $values va récupérer les valeurs entrés dans le formulaire
	public function executeSql($sql, array $values = array()) {
		$query = $this->pdo->prepare($sql);

		$query->execute($values);
	}

	//Fonction permettant d'afficher toutes les informations de la requête SQL
    public function query($sql, array $criteria = array()) {
        $query = $this->pdo->prepare($sql);

        $query->execute($criteria);

        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

	//Fonction permettant d'afficher une information de la requête SQL
    public function queryOne($sql, array $criteria = array()) {
        $query = $this->pdo->prepare($sql);

        $query->execute($criteria);

        return $query->fetch(PDO::FETCH_ASSOC);
    }
}