<?php

//Classe permettant de vérifié les informations entrées dans le formulaire
class ValidateForm {
    //Méthode permettant de faire un "nettoyage" des informations fournies par l'utilisateur lors de l'insertion dans la base de donnée
    public function validateInfo($values) {
        //Utilisation de trim() pour supprimer les caractères inutiles (espaces en trop, tabulation, nouvelle ligne) dans les informations fournies par l'utilisateur
        $values = trim($values);
        //Utilisation de striplashes() pour supprimer les \ dans les données fournies (qui peuvent être utiliser pour échappé des caracètres spéciaux)
        $values = stripslashes($values);
        //Utilisation de htlmspecialchars() pour convertir les caractères spéciaux en entités html (principalement pour éviter les injections de code Javascript à l'aide des balises <script></script>)
        $values = htmlspecialchars($values);
        //On retourne la variable "nettoyé" pour l'insertion en base de donnée
        return $values;
    }
}