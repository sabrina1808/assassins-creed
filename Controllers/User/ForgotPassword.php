<?php

//Classe permettant de gérer la page "Mot de passe oublié"
class ForgotPassword {
    //Méthode permettant d'afficher le contenu de la page et d'envoyé un mail pour le changement de mot de passe
    public function sendMailForModifyPassword($params) {
        //Déclaration d'une variable vide qui contiendra le message qui sera affiché pour prévenir que le mail a bien été envoyé ou que l'adresse mail est inconnue dans la base de donnée
        $response = "";

        //Déclaration de la variable qui serva lors du remplissage du formulaire
        $email = "";

        //Si on envoie des informations à l'aide du formulaire, on exécute la méthode permettant d'envoyé le mail
        if(!empty($_POST)) {
            //Déclaration d'une variable qui permettra de déterminé le nombre d'erreur présente dans le formulaire, que l'on initialise à 0
            $error = 0;

            //Appel de la classe ValidateForm
            $validateForm = new ValidateForm();

            //On utilise la vérification de formulaire uniquement pour la clé "Email"
            /*On incrémente de 1 la valeur de la variable $error si un des cas suivant est vérifié :
                - le champ est vide (car il est requis)
                - la longeur de la chaîne de caractère entrée ne correspond au filtre  permettant de validé le format de l'adresse mail
                - la chaîne de caractère entrée contient des caractères qui n'appartiennent pas à l'expression régulière définie dans la condition
            */
            if($_POST['Email'] == "" || !filter_var($_POST['Email'], FILTER_VALIDATE_EMAIL) || !preg_match("/^(([^<>()\[\]\\.,;:\s@\"]+(\.[^<>()\[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $_POST['Email'])) {
                $error++;
            }
            //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
            else {
                //On affecte la valeur du champ Email à la variable $email pour pouvoir affiché celle-ci dans le formulaire
                $email = $validateForm->validateInfo($_POST['Email']);
            }

            //Exécution de l'envoi du mail uniquement s'il n'y a pas d'erreur dans le formulaire
            if($error == 0) {
                //Appel du model UserModel
                $user = new UserModel();

                //Exécution de la méthode permettant d'envoyé un mail pour permettre le changement de mot de passe
                $response = $user->sendMailForChangePassword($_POST);
            }
        }

        //Affiche du contenu de la page
        $myView = new View("Mot de passe oublié", "User", "forgotPassword");
        $myView->renderView(array("response" => $response, "email" => $email));
    }
}