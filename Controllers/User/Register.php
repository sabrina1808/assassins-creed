<?php

//Classe permettant de gérer l'affichage de la page "S'enregistrer"
class Register {
    //Méthode permettant d'afficher le contenu de la page et d'enregistrer l'utilisateur
    public function newUser($params) {
        $message = null;
        //Déclaration des variables qui serviront lors du remplissage du formulaire
        $firstname = $lastname = $email = $password = $address = $city = $zipCode = $country = $phone = "";

        //On effectue l'ajout du nouvel utilisateur dans la base de données uniquement si des informations sont renseignés dans le formulaire
        if(!empty($_POST)) {
            //Déclaration d'une variable qui permettra de déterminé le nombre d'erreur présente dans le formulaire, que l'on initialise à 0
            $error = 0;

            //Appel de la classe ValidateForm
            $validateForm = new ValidateForm();

            //Utilisation d'une boucle foreach pour parcourir le tableau récupérer lors de la soumission du formulaire
            foreach($_POST as $key => $values) {
                //Utilisation d'une condition switch pour permettre de vérifié chaque cas de figure pour $values en fonction de sa clé $key
                switch($key) {
                    //Pour la clé "FirstName"
                    case "FirstName":
                        /*On incrémente de 1 la valeur de la variable $error si un des cas suivant est vérifié :
                            - le champ est vide (car il est requis)
                            - la longeur de la chaîne de caractère entrée possède une longeur supérieur à 20
                            - la chaîne de caractère entrée contient des caractères qui n'appartiennent pas à l'expression régulière définie dans la condition
                        */
                        if($values == "" || strlen($values) > 20 || !preg_match("/^[A-Za-zéèêïîÉÊËÏÎ\s-]+$/", $values)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ Prénom à la variable $firstname pour pouvoir afficher celle-ci dans le formulaire
                            $firstname = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "LastName"
                    case "LastName":
                        /*On incrémente de 1 la valeur de la variable $error si un des cas suivant est vérifé :
                            - le champ est vide (car il est requis)
                            - la longeur de la chaîne de caractère entrée possède une longeur supérieur à 20
                            - la chaîne de caractère entrée contient des caractères qui n'appartiennent pas à l'expression régulière définie dans la condition
                        */
                        if($values == "" || strlen($values) > 20 || !preg_match("/^[A-Za-zéèêïîÉÊËÏÎ\s-]+$/", $values)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ Nom à la variable $lastname pour pouvoir afficher celle-ci dans le formulaire
                            $lastname = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "Email"
                    case "Email":
                        /*On incrémente de 1 la valeur de la variable $error si un des cas suivant est vérifié :
                            - le champ est vide (car il est requis)
                            - la longeur de la chaîne de caractère entrée ne correspond au filtre  permettant de validé le format de l'adresse mail
                            - la chaîne de caractère entrée contient des caractères qui n'appartiennent pas à l'expression régulière définie dans la condition
                        */
                        if($values == "" || !filter_var($values, FILTER_VALIDATE_EMAIL) || !preg_match("/^(([^<>()\[\]\\.,;:\s@\"]+(\.[^<>()\[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $values)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ Email à la variable $email pour pouvoir afficher celle-ci dans le formulaire
                            $email = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "Password"
                    case "Password":
                        //On incrémente de 1 la valeur de la variable $error si le champ est vide (car il est requis)
                        if($values == "") {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ Mot de passe à la variable $password pour pouvoir afficher celle-ci dans le formulaire
                            $password = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "Address"
                    case "Address":
                        //On incrémente de 1 la valeur de la variable $error si le champ est vide et qu'il contient des caractères qui n'appartiennent pas à l'expression regulière définie dans la condition
                        if($values == "" && !preg_match("/^[A-Za-zéèêàçïîÉÊËÏÎ0-9\s-]+$/", $values)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ Adresse à la variable $address pour pouvoir afficher celle-ci dans le formulaire
                            $address = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "City"
                    case "City":
                        //On incrémente de 1 la valeur de la variable $error si le champ est vide et qu'il contient des caractères qui n'appartiennent pas à l'expression regulière définie dans la condition
                        if($values == "" && !preg_match("/^[A-Za-zéèêïîÉÊËÏÎ\s-]+$/", $values)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ Ville à la variable $city pour pouvoir afficher celle-ci dans le formulaire
                            $city = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "zipCode"
                    case "zipCode":
                        /*On incrémente de 1 la valeur de la variable $error si le champ est vide et qu'un des cas suivants est vérifié :
                            - la chaîne de caractère conteint autre chose que des chiffres
                            - la longeur de la chaîne de caractère est différente de 5    
                        */
                        if($values == "" && (!preg_match("/^[0-9]+$/", $values) || strlen($values) != 5)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ Code postal à la variable $zipCode pour pouvoir afficher celle-ci dans le formulaire
                            $zipCode = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "Country"
                    case "Country":
                        //On incrémente de 1 la valeur de la variable $error si le champ est vide et qu'il contient des caractères qui n'appartiennent pas à l'expression regulière définie dans la condition
                        if($values == "" && !preg_match("/^[A-Za-zéèêïîÉÊËÏÎ\s-]+$/", $values)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ Pays à la variable $country pour pouvoir afficher celle-ci dans le formulaire
                            $country = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "Phone"
                    case "Phone":
                        /*On incrémente de 1 la valeur de la variable $error si le champ n'est pas vide et qu'un des cas suivants est vérifié :
                            - la chaîne de caractère conteint autre chose que des chiffres
                            - la longeur de la chaîne de caractère est différente de 10    
                        */
                        if($values != "" && (!preg_match("/^[0-9]+$/", $values) || strlen($values) != 10)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ Numéro de téléphone à la variable $phone pour pouvoir afficher celle-ci dans le formulaire
                            $phone = $validateForm->validateInfo($values);
                        }
                    break;
                }
            }

            //Exécution de l'ajout de l'utilisateur dans la base de donnée uniquement s'il n'y a pas d'erreur dans le formulaire
            if($error == 0) {
                //Appel du model UserModel
                $user = new UserModel();

                //Exécution de la méthode permettant d'ajouter un utilisateur dans la base de donnée
                $message = $user->addUser($_POST);

                //Redirection vers la page d'accueil
                $redirection = new Redirection();
                $redirection->redirect("Login");
            }
        }

        //Affichage du contenu de la page
        $myView = new View("S'enregistrer", "User", "register");
        $myView->renderView(array("message" => $message, "firstname" => $firstname, "lastname" => $lastname, "email" => $email, "password" =>$password, "address" => $address, "city" => $city, "zipCode" => $zipCode, "country" => $country, "phone" => $phone));
    }
}