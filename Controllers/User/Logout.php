<?php

//Classe permettant de gérer la déconnexion de l'utilisateur
class Logout {
    //Méthode permettant de déconnecter un utilisateur
    public function disconnect($params) {
        //Destruction des données de la session en cours
        session_destroy();

        //Redirection vers la page d'accueil
        $redirection = new Redirection();
        $redirection->redirect("Home");
    }
}