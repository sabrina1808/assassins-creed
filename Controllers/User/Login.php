<?php

//Classe permettant de gérer la page "Connexion"
class Login {
    //Méthode permettant d'affiché le contenu de la page et de connecter un utilisateur
    public function connection($params) {
        //Déclaration des variables qui serviront lors du remplissage du formulaire
        $email = $password = "";

        //Déclaration d'une variable qui permettra d'affiché le message d'erreur en cas de mauvaise adresse mail ou mauvais mot de passe
        $message = null;

        //On effectue la connexion de l'utilisateur uniquement si celui-ci à bien renseigné les informations demandé dans le formulaire
        if(!empty($_POST)) {
            //Déclaration d'une variable qui permettra de déterminé le nombre d'erreur présente dans le formulaire, que l'on initialise à 0
            $error = 0;

            //Appel de la classe ValidateForm
            $validateForm = new ValidateForm();

            //Utilisation d'une boucle foreach pour parcourir le tableau récupérer lors de la soumission du formulaire
            foreach($_POST as $key => $values) {
                //Utilisation d'une condition switch pour permettre de vérifié chaque cas de figure pour $values en fonction de sa clé $key
                switch($key) {
                    //Pour la clé "Email"
                    case "Email":
                        /*On incrémente de 1 la valeur de la variable $error si un des cas suivant est vérifié :
                            - le champ est vide (car il est requis)
                            - la chaîne de caractère entrée ne correspond au filtre permettant de validé le format de l'adresse mail
                            - la chaîne de caractère entrée contient des caractères qui n'appartiennent pas à l'expression régulière définie dans la condition
                        */
                        if($values == "" || !filter_var($values, FILTER_VALIDATE_EMAIL) || !preg_match("/^(([^<>()\[\]\\.,;:\s@\"]+(\.[^<>()\[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $values)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ Email à la variable $email pour pouvoir affiché celle-ci dans le formulaire
                            $email = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "Password"
                    case "Password":
                        //On incrémente de 1 la valeur de la variable $error si le champ est vide (car il est requis)
                        if($values == "") {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ Mot de passe à la variable $password pour pouvoir affiché celle-ci dans le formulaire
                            $password = $validateForm->validateInfo($values);
                        }
                    break;
                }
            }

            //Connexion de l'utilisateur uniquement s'il n'y a pas d'erreur dans le formulaire
            if($error == 0) {
                //Appel du model UserModel
                $user = new UserModel();

                //Exécution de la méthode permettant de connecter un utilisateur
                $message = $user->connectUser($_POST);

                if($message == "connected") {
                    //Redirection vers la page d'accueil
                    $redirection = new Redirection();
                    $redirection->redirect("Home");
                }
                else {
                    $message = "error";
                }
                
            }
        }

        //Affichage du contenu de la page
        $myView = new View("Connexion", "User", "login");
        $myView->renderView(array("email" => $email, "password" => $password, "message" => $message));
    }
}