<?php

//Classe permettant de gérer la page "Changement de mot de passe"
class ChangePassword {
    //Méthode permettant d'afficher le contenu de la page et de faire le changement de mot de passe
    public function modifyPassword($params) {
        //Déclaration d'une variable null qui contiendra le message qui sera affiché pour prévenir que le mot de passe a bien été modifié
        $response = null;

        //Si l'userId n'existe pas dans le tableau $params ou que celui-ci ne correspond pas à celui enregistré dans les données de sessions, on renvoie vers la page de connexion
        if(!array_key_exists('userId', $params)) {
            $redirection = new Redirection();
            $redirection->redirect("Login");
        }
        
        //On utilise la fonction extract pour créer de manière dynamique la variable qui correspond au paramètre que l'on a renseigné
        extract($params);

        //Attribution à la variable $userId, la valeur de l'userId récupérer dans le tableau $params
        $userId = $params['userId'];

        //Déclaration des variables qui serviront lors du remplissage du formulaire
        $psw1 = $psw2 = "";

        //Si on envoie des informations à l'aide du formulaire, on fait la mise à jour du mot de passe
        if(!empty($_POST)) {
            //Déclaration d'une variable qui permettra de déterminé le nombre d'erreur présente dans le formulaire, que l'on initialise à 0
            $error = 0;

            //Appel de la classe ValidateForm
            $validateForm = new ValidateForm();

            //Utilisation d'une boucle foreach pour parcourir le tableau récupérer lors de la soumission du formulaire
            foreach($_POST as $key => $values) {
                //Utilisation d'une condition switch pour permettre de vérifié chaque cas de figure pour $values en fonction de sa clé $key
                switch($key) {
                    //Pour la clé "psw1"
                    case "psw1":
                        //On incrémente de 1 la valeur de la variable $error si le champ est vide (car il est requis)
                        if($values == "") {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ Nouveau mot de passe à la variable $psw1 pour pouvoir affiché celle-ci dans le formulaire
                            $psw1 = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "psw2"
                    case "psw2":
                        //On incrémente de 1 la valeur de la variable $error si le champ est vide (car il est requis)
                        if($values == "") {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ Confirmation du mot de passe à la variable $psw2 pour pouvoir affiché celle-ci dans le formulaire
                            $psw2 = $validateForm->validateInfo($values);
                        }
                    break;
                }
            }

            //Exécution de la modification du mot de passe dans la base de donnée uniquement s'il n'y a pas d'erreur dans le formulaire
            if($error == 0) {
                //On vérifie que les deux mots de passes renseigné sont identiques
                if($_POST['psw1'] === $_POST['psw2']) {
                    //Appel du model UserModel
                    $user = new UserModel();

                    //Exécution de la méthode permettant de changer le mot de passe
                    $user->changePassword($_POST['psw1'], $userId);

                    //Définition du message qui sera affiché sur la page
                    $response = "yes";
                }
                else {
                    $response = "no";
                }
            }
        }
        
        //Affiche du contenu de la page
        $myView = new View("Changement de mot de passe", "User", "changePassword");
        $myView->renderView(array("userId" => $userId, "response" => $response, "psw1" => $psw1, "psw2" => $psw2));
    }
}