<?php

//Classe permettant de gérer la page "Détails de la commande"
class Administration_Order_Details {
    //Méthode permettant de voir les détails de la commande
    public function orderDetails($params) {
        //On utilise la fonction extract pour créer de manière dynamique la variable qui correspond au paramètre que l'on a renseigné
        extract($params);

        //Si l'orderId ou l'userId que l'on récupere n'existe pas, on renvoie vers la page d'administration des commandes
        if(!array_key_exists('orderId', $params) || !array_key_exists('userId', $params)) {
            //Redirection vers la page d'administration des commandes
            $redirection = new Redirection();
            $redirection->redirect("Administration-order");
        }

        //Appel des models UserModel et OrderModel
        $user = new UserModel();
        $order = new OrderModel();

        //Exécution de la fonction permettant d'obtenir les informations de l'utilisateur qui a passé la commande
        $userDetails = $user->userInfo($userId);

        //Exécution de la fonction permettant d'obtenir les détails de la commande sélectionné
        $orderDetails = $order->viewOrderDetails($orderId);

        //Affichage du contenu de la page
        $myView = new View("Détails de la commande", "Administration", "administration", "administration-order-details");
        $myView->renderView(array("orderId" => $orderId, "userDetails" => $userDetails, "orderDetails" => $orderDetails));
    }
}