<?php

//Classe permettant de gérer la page "Administration des utilisateurs"
class Administration_User_Update {
    //Méthode permettant de mettre à jour le rôle d'un utilisateur
    public function updateRole($params) {
        //Appel de la classe Redirection
        $redirection = new Redirection();

        //Si l'utilisateur n'est pas administrateur, il n'a pas accès à cette page
        if($_SESSION['Role'] !== "admin") {
            //Redirection vers la page d'accueil
            $redirection->redirect("Home");
        }

        //On utilise la fonction extract pour créer de manière dynamique la variable qui correspond au paramètre que l'on a renseigné
        extract($params);

        //Si l'userId que l'on récupere n'existe pas ou qu'il ne s'agit pas d'un entier, on renvoie vers la page d'administration
        if(!array_key_exists('userId', $params) || !ctype_digit($params['userId'])) {
            //Redirection vers la page d'administration des utilisateurs
            $redirection->redirect("Administration-user");
        }

        //Si la valeur du rôle est différente de "/", on fait la mise à jour du rôle de l'utilisateur sélectionné
        if($_POST['Role'] != "/") {
            //Appel du model UserModel
            $user = new UserModel();

            //Exécution de la méthode permettant de mettre à jour le rôle de l'utilisateur
            $user->updateUser($_POST, $userId);
        }

        //Redirection vers la page d'administration des utilisateurs
        $redirection->redirect("Administration-user");
    }
}