<?php

//Classe permettant de gérer la page "Administration des commandes"
class Administration_Order {
    //Méthode permettant de faire la liste de toutes les commandes passées sur le site
    public function orderList($params) {
        //Si l'utilisateur n'est pas admin, il n'a pas accès à cette page
        if($_SESSION['Role'] !== "admin") {
            //Redirection vers la page d'accueil
            $redirection = new Redirection();
            $redirection->redirect("Home");
        }

        //Appel du model OrderModel
        $order = new OrderModel();

        //Exécution de la méthode permettant d'avoir la liste des commandes
        $orders = $order->listAllOrder();

        //Affichage du contenu de la page
        $myView = new View("Administration des commandes", "Administration", "administration", "administration-order");
        $myView->renderView(array("orders" => $orders));
    }
}