<?php

//Classe permettant de gérer la page "Administration de la boutique"
class Administration_Shop {
    //Méthode permettant d'afficher le contenu de la page et d'ajouter un produit à la boutique
    public function addProductOnShop($params) {
        //Déclaration des variables qui serviront lors du remplissage du formulaire
        $name = $description = $quantity = $price = "";

        //Appel de la classe Redirection
        $redirection = new Redirection();

        //Si l'utilisateur n'est pas admin, il n'a pas accès à cette page
        if($_SESSION['Role'] !== "admin") {
            //Redirection vers la page d'accueil
            $redirection->redirect("Home");
        }

        //Si on envoie des informations à l'aide du formulaire, on ajoute le produit à la base de donnée
        if(!empty($_POST)) {
            //Déclaration d'une variable qui permettra de déterminé le nombre d'erreur présente dans le formulaire, que l'on initialise à 0
            $error = 0;

            //Appel de la classe ValidateForm
            $validateForm = new ValidateForm();

            //Utilisation d'une boucle foreach pour parcourir le tableau récupérer lors de la soumission du formulaire
            foreach($_POST as $key => $values) {
                //Utilisation d'une condition switch pour permettre de vérifié chaque cas de figure pour $values en fonction de sa clé $key
                switch($key) {
                    //Pour la clé "Name"
                    case "Name":
                        /*On incrémente de 1 la valeur de la variable $error si un des cas suivant est vérifié :
                            - le champ est vide (car il est requis)
                            - la chaîne de caractère entrée contient des caractères qui n'appartiennent pas à l'expression régulière définie dans la condition
                        */
                        if($values == "" || !preg_match("/^[A-Za-zéèêïîÉÊËÏÎ\s-]+$/", $values)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ Name à la variable $name pour pouvoir afficher celle-ci dans le formulaire
                            $name = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "Description"
                    case "Description":
                        /*On incrémente de 1 la valeur de la variable $error si un des cas suivant est vérifié :
                            - le champ est vide (car il est requis)
                            - la longeur de la chaîne de caractère entrée possède une longeur supérieur à 1000
                            - la chaîne de caractère entrée contient des caractères qui n'appartiennent pas à l'expression régulière définie dans la condition
                        */
                        if($values == "" || strlen($values) > 1000 || !preg_match("/^[A-Za-zéèêàçïîÉÊËÏÎ0-9\s\-\.'\"()&,]+$/", $values)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ Description à la variable $description pour pouvoir afficher celle-ci dans le formulaire
                            $description = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "Type"
                    case "Type":
                        //On incrémente de 1 la valeur de la variable $error si la valeur de l'option sélectionné est "/"
                        if($values == "/") {
                            $error++;
                        }
                    break;
                    //Pour la clé "Photo"
                    case "Photo":
                        //On incrémente de 1 la valeur de la variable $error si le champ est vide
                        if($values == "") {
                            $error++;
                        }
                    break;
                    //Pour la clé "QuantityInStock"
                    case "QuantityInStock":
                        /*On incrémente de 1 la valeur de la variable $error si un des cas suivant est vérifié :
                            - le champ est vide (car il est requis)
                            - la longeur de la chaîne de caractère entrée possède une longeur supérieur à 3
                            - la chaîne de caractère entrée contient des caractères qui n'appartiennent pas à l'expression régulière définie dans la condition
                        */
                        if($values == "" || strlen($values) > 3 || !preg_match("/^[0-9]+$/", $values)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ QuantityInStock à la variable $quantity pour pouvoir afficher celle-ci dans le formulaire
                            $quantity = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "SalePrice"
                    case "SalePrice":
                        /*On incrémente de 1 la valeur de la variable $error si un des cas suivant est vérifié :
                            - le champ est vide (car il est requis)
                            - la longeur de la chaîne de caractère entrée possède une longeur supérieur à 5
                            - la chaîne de caractère entrée contient des caractères qui n'appartiennent pas à l'expression régulière définie dans la condition
                        */
                        if($values == "" || strlen($values) > 5 || !preg_match("/^[0-9]+$/", $values)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            //On affecte la valeur du champ SalePrice à la variable $price pour pouvoir afficher celle-ci dans le formulaire
                            $price = $validateForm->validateInfo($values);
                        }
                    break;
                }
            }

            //Si aucune erreur n'est détecté lors de la soumission du formulaire, on ajoute le produit à la base de donnée et on est redirigé vers la page d'administration des articles
            if($error == 0) {
                //Appel du model ShopModel
                $shop = new ShopModel();

                //Exécution de la méthode permettant d'ajouter un produit à la base de donnée
                $shop->addProduct($_POST);

                //Redirection vers la page d'administration de gestion des articles
                $redirection->redirect("Administration-shop-list");
            }
        }

        //Affichage du contenu de la page
        $myView = new View("Administration de la boutique", "Administration", "administration", "administration-shop");
        $myView->renderView(array("name" => $name, "description" => $description, "quantity" => $quantity, "price" => $price));
    }
}