<?php

//Classe permettant de gérer la page "Administration des utilisateurs"
class Administration_User {
    //Méthode permettant de d'afficher le contenu de la page et la liste des utilisateurs
    public function userList($params) {
        //Si l'utilisateur n'est pas administrateur, il n'a pas accès à cette page
        if($_SESSION['Role'] !== "admin") {
            //Redirection vers la page d'accueil
            $redirection = new Redirection();
            $redirection->redirect("Home");
        }

        //Appel du model UserModel
        $user = new UserModel();

        //Exécution de la méthode permettant de faire la liste de tous les utilisateurs enregistrés
        $users = $user->listAllUsers();

        //Affichage du contenu de la page
        $myView = new View("Administration des utilisateurs", "Administration", "administration", "administration-user");
        $myView->renderView(array("users" => $users));
    }
}