<?php

//Classe permettant de gérer la page "Gestion des articles de la boutique"
class Administration_Shop_List {
    //Méthode permettant de gérer les articles de la boutique
    public function productsList($params) {
        //Si l'utilisateur n'est pas admin, il n'a pas accès à cette page
        if($_SESSION['Role'] !== "admin") {
            //Redirection vers la page d'accueil
            $redirection = new Redirection();
            $redirection->redirect("Home");
        }

        //Appel du model ShopModel
        $shop = new ShopModel();

        //Exécution de la méthode permettant de lister tous les articles de la boutique
        $products = $shop->listAll();

        //Affichage du contenu de la page
        $myView = new View("Gestion des articles de la boutique", "Administration", "administration", "administration-shop-list");
        $myView->renderView(array("products" => $products));
    }
}