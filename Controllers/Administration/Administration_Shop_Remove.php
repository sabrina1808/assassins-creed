<?php

//Classe permettant de gérer la page "Gestion des produits de la boutique"
class Administration_Shop_Remove {
    //Méthode permettant de supprimer un article de la boutique
    public function removeProduct($params) {
        //Appel de la classe Redirection
        $redirection = new Redirection();

        //Si l'utilisateur n'est pas admin, il n'a pas accès à cette page
        if($_SESSION['Role'] !== "admin") {
            //Redirection vers la page d'accueil
            $redirection->redirect("Home");
        }

        //Si le productId que l'on récupere n'existe pas, on renvoie vers la page de gestion des articles
        if(!array_key_exists('productId', $params)) {
            //Redirection vers la page de gestion des articles
            $redirection->redirect("Administration-shop-list");
        }

        //On utilise la fonction extract pour créer de manière dynamique la variable qui correspond au paramètre que l'on a renseigné
        extract($params);

        //Appel du model ShopModel
        $shop = new ShopModel();

        //Exécution de la méthode permettant de supprimé un article de la boutique
        $shop->deleteProduct($productId);

        //Redirection vers la page de gestion des articles
        $redirection->redirect("Administration-shop-list");
    }
}