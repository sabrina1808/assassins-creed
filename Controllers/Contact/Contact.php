<?php

//Classe permettant de gérer la page "Contact"
class Contact {
    //Méthode permettant d'envoyer un mail à partir du formulaire de la page contact
    public function sendMail($params) {
        //Déclaration d'une variable null qui contiendra le message qui sera affiché pour prévenir que l'on a bien envoyé le mail
        $response = null;

        //On exécute la fonction permettant d'envoyé le mail uniquement si des informations sont renseignés dans le formulaire
        if(!empty($_POST)) {
            //Récupération des informations contenues dans le formulaire
            $firstName = $_POST['FirstName'];
            $lastName = $_POST['LastName'];
            $email = $_POST['Email'];
            $message = $_POST['Message'];

            //Définition du message qui sera envoyé par mail à l'administrateur du site
            $contentEmail = "Un nouveau message vous a été envoyé.<br>
            Nom Prénom : ".$firstName." ".$lastName."<br>
            Email : <a href='mailto:".$email."'>".$email."</a><br>
            Message : ".$message."<br>";

            //Envoi du mail
            mail("blabla@blabla.com", "Message d'un utilisateur du site Assassin's Creed", $contentEmail);

            //Définition du message qui sera affiché sur la page
            $response = "Le mail a bien été envoyé. Nous vous remercions pour votre message.";
        }

        //Affichage du contenu de la page
        $myView = new View("Contact", "Contact", "contact");
        $myView->renderView(array("response" => $response));
    }
}