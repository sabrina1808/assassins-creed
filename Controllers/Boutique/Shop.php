<?php

//Classe permettant de gérer l'affichage de la page "Boutique"
class Shop {
    //Méthode permettant d'afficher le contenu de la page
    public function showProductList($params) {
        //Appel du model ShopModel
        $shop = new ShopModel();

        //Exécution de la méthode permettant de faire la liste de tous les produits
        $products = $shop->listAll();

        //Appel de la classe View pour l'affichage du contenu de la page
        $myView = new View("Boutique", "Boutique", "shop");
        $myView->renderView(array("products" => $products));
    }
}