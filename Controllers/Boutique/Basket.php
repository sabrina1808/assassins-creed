<?php

//Classe permettant de gérer la page "Panier"
class Basket {
    //Méthode permettant d'affiché le contenu du panier
    public function showBasket($params) {
        //Si l'utilisateur n'est pas connecté, il est redirigé vers la page de connexion
        if($_SESSION['FirstName'] == null) {
            //Redirection vers la page de connexion
            $redirection = new Redirection();
            $redirection->redirect("Login");
        }

        //Affichage du contenu de la page
        $myView = new View("Panier", "Boutique", "basket");
        $myView->renderView();
    }
}