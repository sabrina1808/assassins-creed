<?php

//Classe permettant de gérer la page "Valider la commande"
class Validation {
    //Méthode permettant de valider la commande
    public function validateOrder($params) {
        //Si l'utilsiteur n'est pas connecté, il est redirigé vers la page de connexion
        if($_SESSION['FirstName'] == null) {
            //Redirection vers la page de connexion
            $redirection = new Redirection();
            $redirection->redirect("Login");
        }

        //On utilise la fonction extract pour créer de manière dynamique la variable qui correspond au paramètre que l'on a renseigné
        extract($params);

        //Appel des models UserModel et OrderModel
        $user = new UserModel();
        $order = new OrderModel();

        //Exécution de la méthode permettant de récupérer toutes les informations de l'utilisateur
        $userInfo = $user->userInfo($userId);

        //Exécution de la méthode permettant de valider et d'enregister la commande dans la base de donnée
        $order->validateOrder($userId, $_POST["basketItems"]);

        //Affichage du contenu de la page
        $myView = new View("Valider la commande", "Boutique", "validation");
        $myView->renderView(array("userInfo" => $userInfo));
    }
}