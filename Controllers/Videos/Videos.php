<?php

//Classe permettant de gérer l'affichage de la page "Les trailers"
class Videos {
    //Méthode permettant d'afficher le contenu de la page
    public function videoList($params) {
        //Appel du model VideosModel
        $videosModel = new VideosModel();

        //Exécution de la méthode permettant de faire la liste de toutes les videos
        $videos = $videosModel->listAllVideo();

        //Appel de la classe View pour l'affichage du contenu de la page
        $myView = new View("Videos", "Videos", "videos");
        $myView->renderView(array("videos" => $videos));
    }
}