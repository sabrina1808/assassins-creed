<?php

//Classe permettant de gérer les détails de la page "Videos"
class VideosDetails {
    //Méthode permettant d'afficher le contenu de la page
    public function videoSelected($params) {
        //On utilise la fonction extract pour créer de manière dynamique la variable qui correspond au paramètre que l'on a renseigné
        extract($params);

        //Appel du model VideosModel
        $videosModel = new VideosModel();

        //Exécution de la méthode permettant de sélectionner la video demandée
        $video = $videosModel->selectVideo($id);

        //Appel de la classe View pour l'affichage du contenu de la page
        $myView = new View("Videos", "Videos", "videos-details");
        $myView->renderView(array("video" => $video));
    }
}