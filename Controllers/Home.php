<?php

//Classe permettant de gérer la page "Accueil"
class Home {
    //Méthode permettant d'afficher le contenu de la page
    public function view($params) {
        //Appel de la classe View pour l'affichage du contenu de la page
        $myView = new View("Assassin's Creed - Accueil", "", "home");
        $myView->renderView();
    }
}