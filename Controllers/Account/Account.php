<?php

//Classe permettant de gérer la page "Mon compte"
class Account {
    //Méthode permettant d'afficher la page ainsi que les informations de l'utilisateur
    public function myInformations($params) {
        //Si l'userId n'existe pas dans le tableau $params ou que celui-ci ne correspond pas à celui enregistré dans les données de sessions, on renvoie vers la page de connexion
        if(!array_key_exists('userId', $params) || $params['userId'] != $_SESSION['Id']) {
            $redirection = new Redirection();
            $redirection->redirect("Login");
        }

        //On utilise la fonction extract pour créer de manière dynamique la variable qui correspond au paramètre que l'on a renseigné
        extract($params);

        //Appel du model UserModel
        $user = new UserModel();

        //Exécution de la méthode permettant de récupérer les informations de l'utilisateur
        $userInfo = $user->userInfo($userId);

        //Affichage du contenu de la page
        $myView = new View("Mon compte", "Account", "account");
        $myView->renderView(array("userInfo" => $userInfo));
    }
}