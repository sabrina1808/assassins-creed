<?php

//Classe permettant de gérer la suppression de son compte par un utilisateur
class Account_Delete {
    //Méthode permettant de supprimer le compte de l'utilisateur
    public function deleteAccount($params) {
        //Appel de la classe Redirection
        $redirection = new Redirection();

        //Si l'userId n'existe pas dans le tableau $params ou que celui-ci ne correspond pas à celui enregistré dans les données de sessions, on renvoie vers la page de connexion
        if(!array_key_exists('userId', $params) || $params['userId'] != $_SESSION['Id']) {
            $redirection->redirect("Login");
        }

        //On utilise la fonction extract pour créer de manière dynamique la variable qui correspond au paramètre que l'on a renseigné
        extract($params);

        //Appel du model UserModel
        $user = new UserModel();

        //Exécution de la méthode permettant de supprimé un utilisateur
        $user->deleteUser($userId);

        //Destruction des données de la session en cours
        session_destroy();

        //Redirection vers la page d'accueil
        $redirection->redirect("Home");
    }
}