<?php

//Classe permettant de gérer l'affichage de la page "Mise à jour des informations de mon compte"
class Account_Update {
    //Méthode permettant de gérer l'affichage de la page et la mise à jour des informations du compte de l'utilisateur
    public function updateInformations($params) {
        //Appel de la classe Redirection
        $redirection = new Redirection();

        //Si l'userId n'existe pas dans le tableau $params ou que celui-ci ne correspond pas à celui enregistré dans les données de session, on renvoie vers la page de connexion
        if(!array_key_exists('userId', $params) || $params['userId'] != $_SESSION['Id']) {
            $redirection->redirect("Login");
        }

        //On utilise la fonction extract pour créer de manière dynamique la variable qui correspond au paramètre que l'on a renseigné
        extract($params);

        //Appel du model UserModel
        $user = new UserModel();

        //Exécution de la méthode permettant d'afficher les informations de l'utilisateur
        $userInfo = $user->userInfo($userId);

        //Si on envoie des informations à l'aide du formulaire, on fait la mise à jour des informations
        if(!empty($_POST)) {
            //Déclaration d'une variable qui permettra de déterminé le nombre d'erreur présente dans le formulaire, que l'on initialise à 0
            $error = 0;

            //Appel de la classe ValidateForm
            $validateForm = new ValidateForm();

            //Utilisation d'une boucle foreach pour parcourir le tableau récupérer lors de la soumission du formulaire
            foreach($_POST as $key => $values) {
                //Utilisation d'une condition switch pour permettre de vérifié chaque cas de figure pour $values en fonction de sa clé $key
                switch($key) {
                    //Pour la clé "Email"
                    case "Email":
                        /*On incrémente de 1 la valeur de la variable $error si un des cas suivant est vérifié :
                            - le champ est vide (car il est requis)
                            - la longeur de la chaîne de caractère entrée ne correspond au filtre  permettant de validé le format de l'adresse mail
                            - la chaîne de caractère entrée contient des caractères qui n'appartiennent pas à l'expression régulière définie dans la condition
                        */
                        if($values == "" || !filter_var($values, FILTER_VALIDATE_EMAIL) || !preg_match("/^(([^<>()\[\]\\.,;:\s@\"]+(\.[^<>()\[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $values)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            $_POST['Email'] = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "Phone"
                    case "Phone":
                        /*On incrémente de 1 la valeur de la variable $error si le champ n'est pas vide et qu'un des cas suivants est vérifié :
                            - la chaîne de caractère conteint autre chose que des chiffres
                            - la longeur de la chaîne de caractère est différente de 10    
                        */
                        if($values != "" && (!preg_match("/^[0-9]+$/", $values) || strlen($values) != 10)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            $_POST['Phone'] = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "Address"
                    case "Address":
                        //On incrémente de 1 la valeur de la variable $error si le champ est vide et qu'il contient des caractères qui n'appartiennent pas à l'expression regulière définie dans la condition
                        if($values == "" && !preg_match("/^[A-Za-zéèêàçïîÉÊËÏÎ0-9\s-]+$/", $values)) {
                            $error++;
                        }
                        else {
                            $_POST['Address'] = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "zipCode"
                    case "zipCode":
                        /*On incrémente de 1 la valeur de la variable $error si le champ est vide et qu'un des cas suivants est vérifié :
                            - la chaîne de caractère conteint autre chose que des chiffres
                            - la longeur de la chaîne de caractère est différente de 5    
                        */
                        if($values == "" && (!preg_match("/^[0-9]+$/", $values) || strlen($values) != 5)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            $_POST['zipCode'] = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "City"
                    case "City":
                        //On incrémente de 1 la valeur de la variable $error si le champ est vide et qu'il contient des caractères qui n'appartiennent pas à l'expression regulière définie dans la condition
                        if($values == "" && !preg_match("/^[A-Za-zéèêïîÉÊËÏÎ\s-]+$/", $values)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            $_POST['City'] = $validateForm->validateInfo($values);
                        }
                    break;
                    //Pour la clé "Country"
                    case "Country":
                        //On incrémente de 1 la valeur de la variable $error si le champ est vide et qu'il contient des caractères qui n'appartiennent pas à l'expression regulière définie dans la condition
                        if($values == "" && !preg_match("/^[A-Za-zéèêïîÉÊËÏÎ\s-]+$/", $values)) {
                            $error++;
                        }
                        //Si aucune erreur n'est détecté, on utilise la méthode validateInfo de la classe ValidateForm pour faire un "nettoyage" de ce que l'utilisateur à renseigné
                        else {
                            $_POST['Country'] = $validateForm->validateInfo($values);
                        }
                    break;
                }
            }

            //Exécution de la mise à jour des informations de l'utilisateur dans la base de donnée uniquement s'il n'y a pas d'erreur dans le formulaire
            if($error == 0) {
                //Exécution de la méthode permettant de faire la mise des informations dans la base de donnée
                $user->updateInfo($_POST, $userId);

                //Redirection vers la page du compte utilisateur
                $redirection->redirect("Account/userId/".$userId);
            }
        }

        //Affichage du contenu de la page
        $myView = new View("Mettre à jour mes informations", "Account", "account-update");
        $myView->renderView(array("userInfo" => $userInfo));
    }
}