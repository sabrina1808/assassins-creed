<?php

//Classe permettant de gérer l'affichage de la page "Assassin's Creed Valhalla"
class AC_Valhalla {
    //Méthode permettant d'afficher le contenu de la page
    public function view($params) {
        //Appel de la classe View pour l'affichage du contenu de la page
        $myView = new View("Assassin's Creed Valhalla", "Serie-jeux", "AC-Valhalla");
        $myView->renderView();
    }
}