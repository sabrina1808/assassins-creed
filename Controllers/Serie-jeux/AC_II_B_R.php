<?php

//Classe permettant de gérer l'affichage de la page "Assassin's Creed II-B-R"
class AC_II_B_R {
    //Méthode permettant d'afficher le contenu de la page
    public function view($params) {
        //Appel de la classe View pour l'affichage du contenu de la page
        $myView = new View("Assassin's Creed II, Brotherhood, Révélations", "Serie-jeux", "AC-II-B-R");
        $myView->renderView();
    }
}