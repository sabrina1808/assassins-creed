<?php

//Classe permettant de gérer la page "Termes du jeu"
class Termes {
    //Méthode permettant d'afficher le contenu de la page
    public function view($params) {
        //Appel de la classe View pour l'affichage du contenu de la page
        $myView = new View("Termes du jeu", "Termes", "termes");
        $myView->renderView();
    }
}