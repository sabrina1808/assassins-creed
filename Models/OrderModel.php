<?php

//Classe permettant de gérer les commandes
class OrderModel {
    //Méthode permettant de valider et d'enregistrer les commandes dans la base de donnée
    public function validateOrder($userId, $post) {
        //Appel de la classe Database
        $database = new Database();

        //Mise en place de la requête d'insertion de la commande dans la base de donnée (dans la table orders)
        $sql = "INSERT INTO 
                    orders(
                        User_Id,
                        TaxRate,
                        CreationTimestamp
                        )
                VALUES
                    (?, 20.0, NOW())";
        
        //Exécution de la requête d'insertion
        //Cette commande servira de référence pour obtenir les détails de la commande (présent dans la table ordersline)
        $database->executeSql($sql, array(
            $userId
        ));

        //Initialisation du montant total de la commande
        $totalAmount = 0;

        //Récupération du contenu du panier dans l'input "#basketItems"
        //Transformation du contenu du format JSON en format tableau pour pouvoir l'utilisé par la suite dans la boucle foreach
        $basketItems = json_decode($_POST['basketItems'], true);

        //Utilisation d'une boucle foreach pour parcourir le tableau $basketItems (qui contient les différents articles commandés)
        foreach($basketItems as $basketItem) {
            //Mise à jour du prix total de la commande
            $totalAmount += $basketItem['quantity']*$basketItem['price'];

            //Sélection de l'Id de la commande et transformation du résultat en chaine de caractère
            $orderId = implode($database->queryOne("SELECT MAX(Id) FROM orders"));

            //Mise en place de la requête d'insertion des détails de la commande dans la table ordersline (détails qui vont varié selon la référence de la commande)
            $sql = "INSERT INTO
                        ordersline(
                            QuantityOrdered,
                            Product_Id,
                            Order_Id,
                            PriceEach,
                            Size)
                    VALUES
                        (?, ?, ?, ?, ?)";

            //Exécution de la requête d'insertion des détails de la commande dans la table ordersline
            $database->executeSql($sql, array(
                $basketItem['quantity'],
                $basketItem['productId'],
                $orderId,
                $basketItem['price'],
                $basketItem['size']
            ));

            //Mise en place de la requête de mise à jour de la commande dans la table orders en prenant en compte les détails de la commande
            $sql = "UPDATE
                        orders
                    SET
                        CompleteTimestamp = NOW(),
                        TotalAmount = ?,
                        TaxAmount = ? * TaxRate / 100
                    WHERE
                        Id = ?";
            
            //Exécution de la requête de mise à jour
            $database->executeSql($sql, array(
                $totalAmount,
                $totalAmount,
                $orderId
            ));
        }

        //Renvoi du resultat de la commande sélectionnée avec la mise à jour des détails de celle-ci
        return $orderId;
    }

    //Méthode permettant de listé toutes les commandes
    public function listAllOrder() {
        //Appel de la classe Database
        $database = new Database();

        //Mise en place de la requête de sélection
        $sql = "SELECT
                    orders.Id,
                    FirstName,
                    LastName,
                    User_Id,
                    TotalAmount,
                    TaxRate,
                    TaxAmount,
                    CreationTimestamp,
                    CompleteTimestamp
                FROM
                    orders
                INNER JOIN
                    user
                ON
                    orders.User_Id = user.Id
                ORDER BY
                    CreationTimestamp";
        
        //Exécution de la requête de sélection
        $orders = $database->query($sql);

        //Renvoi du resultat de la requête
        return $orders;
    }

    //Méthode permettant de voir les détails de la commande sélectionné
    public function viewOrderDetails($orderId) {
        //Appel de la classe Database
        $database = new Database();

        //Mise en place de la requête de sélection
        $sql = "SELECT
                    Name,
                    QuantityOrdered,
                    PriceEach,
                    Size,
                    TotalAmount,
                    TaxAmount,
                    CompleteTimestamp
                FROM
                    ordersline
                INNER JOIN
                    products
                ON
                    ordersline.Product_Id = products.Id
                INNER JOIN
                    orders
                ON
                    orders.Id = ordersline.Order_Id
                WHERE
                    Order_Id = ?";
        
        //Exécution de la requête de sélection
        $order = $database->query($sql, array(
            $orderId
        ));

        //Renvoi le résultat de la requête
        return $order;
    }
}