<?php

//Model permettant de gérer les demandes utilisateurs
class UserModel {
    //Méthode permettant d'ajouter un utilisateur dans la base de donnée
    public function addUser($post) {
        //Appel de la classe Database
        $database = new Database();

        //Mise en place de la sélection de l'id d'un utilisateur à partir de son adresse mail
        $sql = "SELECT 
                    Id 
                FROM 
                    user 
                WHERE Email = ?";
        
        //Exécution de la requête de sélection
        $user = $database->queryOne($sql, array($post['Email']));

        //Si la variable $user n'est pas vide, alors l'adresse mail est déjà utilisé par un autre utilisateur
        if(empty($user) == false) {
            //On renvoi un message d'erreur
            return "error";
        }

        //Mise en place de la requête d'insertion dans la base de donnée
        $sql = "INSERT INTO 
                    user
                        (FirstName, 
                        LastName, 
                        Email, 
                        Password, 
                        Address, 
                        City, 
                        ZipCode, 
                        Country, 
                        Phone, 
                        Role)
                VALUES
                    (?, ?, ?, ?, ?, ?, ?, ?, ?, 'user')";

        //Appel de la fonction permettant de hasher le mot de passe
        $hashPassword = $this->hashPassword($post['Password']);

        //Exécution de la requête d'insertion
        $database->executeSql($sql, array(
            $post['FirstName'],
            $post['LastName'],
            $post['Email'],
            $hashPassword,
            $post['Address'],
            $post['City'],
            $post['zipCode'],
            $post['Country'],
            $post['Phone']
        ));
    }

    //Méthode permettant de connecter un utilisateur à son compte grâce à son email
    public function connectUser($post) {
        //Appel de la classe Database
        $database = new Database();

        //Mise en place de la requête de sélection de l'utilisateur dans la base de donnée
        $sql = "SELECT
                    Id,
                    FirstName,
                    LastName,
                    Email,
                    Password,
                    Address,
                    City,
                    ZipCode,
                    Country,
                    Phone,
                    Role
                FROM
                    user
                WHERE
                    Email = ?";

        //Exécution de la requête de sélection
        $user = $database->queryOne($sql, array(
            $post['Email']
        ));
        
        //On vérifie que l'utilisateur est bien enregistré dans la base de donnée et on vérifie que les informations du formulaire correspondent à celle de la base de donnée
        if($user != false && $this->verifyPassword($post['Password'], $user['Password']) == true) {
            $_SESSION['Id'] = $user['Id'];
            $_SESSION['FirstName'] = $user['FirstName'];
            $_SESSION['LastName'] = $user['LastName'];
            $_SESSION['Email'] = $user['Email'];
            $_SESSION['Address'] = $user['Address'];
            $_SESSION['City'] = $user['City'];
            $_SESSION['ZipCode'] = $user['ZipCode'];
            $_SESSION['Country'] = $user['Country'];
            $_SESSION['Phone'] = $user['Phone'];
            $_SESSION['Role'] = $user['Role'];

            //On renvoi un message permettant de connecté l'utilisateur et de faire la redirection vers la page d'accueil
            return "connected";
        }
    }

    //Méthode qui permet de crypté le mot de passe
    private function hashPassword($password) {
        $salt = '$2y$11$'.substr(bin2hex(openssl_random_pseudo_bytes(32)), 0, 32);
        return crypt($password, $salt);
    }

    //Méthode qui permet de vérifier que le mot de passe retourné est bien identique à celui enregistré dans la base de donnée
    private function verifyPassword($password, $hashedPassword) {
        return crypt($password, $hashedPassword) == $hashedPassword;
    }

    //Méthode permettant de lister tous les utilisateurs
    public function listAllUsers() {
        //Appel de la classe Databse
        $database = new Database();

        //Mise en place de la requête
        $sql = "SELECT
                    Id,
                    FirstName,
                    LastName,
                    Email,
                    Role
                FROM
                    user";

        //Exécution de la requête
        $users = $database->query($sql);

        //Renvoi le résultat de la requête
        return $users;
    }

    //Méthode permettant de mettre à jour le role d'un utilisateur
    public function updateUser($post, $userId) {
        //Appel de la classe Database
        $database = new Database();

        //Mise en place de la requête de mise à jour
        $sql = "UPDATE
                    user
                SET
                    Role = ?
                WHERE
                    Id = ?";
        
        //EXécution de la requête de mise à jour
        $database->executeSql($sql, array(
            $post['Role'],
            $userId
        ));
    }

    //Méthode permettant de supprimer un utilisateur de la base de donnée
    public function deleteUser($userId) {
        //Appel de la classe Database
        $database = new Database();

        //Mise en place de la requête de suppression
        $sql = "DELETE FROM
                    user
                WHERE
                    Id = ?";
        
        //Exécution de la requête
        $database->executeSql($sql, array(
            $userId
        ));
    }

    //Méthode permettant de récupérer les informations de l'utilisateur
    public function userInfo($userId) {
        //Appel de la classe Databse
        $database = new Database();

        //Mise en place de la requête de sélection
        $sql = "SELECT
                    FirstName,
                    LastName,
                    Email,
                    Address,
                    City,
                    ZipCode,
                    Country,
                    Phone,
                    Role
                FROM
                    user
                WHERE
                    Id = ?";

        //Exécution de la requête
        $user = $database->queryOne($sql, array(
            $userId
        ));

        //Renvoi du résultat de la requête
        return $user;
    }

    //Méthode permettant de mettre à jour les informations des utilisateurs dans la base de donnée à partir de leur espace
    public function updateInfo($post, $userId) {
        //Appel de la classe Database
        $database = new Database();

        //Mise en place de la requête de mise à jour
        $sql = "UPDATE
                    user
                SET
                    Email = ?,
                    Address = ?,
                    City = ?,
                    ZipCode = ?,
                    Country = ?,
                    Phone = ?
                WHERE
                    Id = ?";

        //Exécution de la requête de mise à jour
        $database->executeSql($sql, array(
            $post['Email'],
            $post['Address'],
            $post['City'],
            $post['ZipCode'],
            $post['Country'],
            $post['Phone'],
            $userId
        ));
    }

    //Méthode permettant d'envoyer un mail pour le changement de mot de passe
    public function sendMailForChangePassword($post) {
        //Appel de la classe Database
        $database = new Database();

        //Mise en place de la requête de sélection de l'utilisateur à partir de son email
        $sql = "SELECT
                    Id,
                    FirstName,
                    LastName,
                    Email
                FROM
                    user
                WHERE
                    Email = ?";

        //Exécution de la requête
        $user = $database->queryOne($sql, array(
            $post['Email']
        ));

        //Si on a bien sélectionné un utilisateur, on envoi un mail avec un lien pour le changement de mot de passe
        if($user != false) {
            //Définition du message de changement de mot de passe
            $message = 'Merci de cliquez sur le lien suivant : <a href="'.HOST.'ChangePassword/userId/'.$user['Id'].'">ici</a> pour changer de mot de passe.';

            //Envoi du mail
            mail($user['Email'], "Changement de mot de passe", $message);

            //Renvoi "yes" pour afficher que l'on a bien envoyé le mail
            return "yes";
        }
        else {
            //Si le mail renseigné n'existe pas dans la base de donnée, on renvoi "no" pour affiché que l'email ne correspond à rien dans la base de donnée
            return "no";
        }
    }

    //Méthode permettant de modifier le mot de passe
    public function changePassword($password, $userId) {
        //Appel de la classe Database
        $database = new Database();

        //Appel de la fonction permettant de hasher le mot de passe
        $hashedPassword = $this->hashPassword($password);

        //Mise en place de la requête de mise à jour
        $sql = "UPDATE
                    user
                SET
                    Password = ?
                WHERE
                    Id = ?";
        
        //Exécution de la requête
        $database->executeSql($sql, array(
            $hashedPassword,
            $userId
        ));
    }
}