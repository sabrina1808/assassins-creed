<?php

//Model permettant de gérer tous ce qui concerne la boutique
class ShopModel {
    //Méthode permettant d'ajouter un produit à la base de donnée
    public function addProduct($post) {
        //Appel de la classe Database
        $database = new Database();

        //Mise en place de la requête d'insertion
        $sql = "INSERT INTO products
                    (Name,
                    Photo,
                    Description,
                    Type,
                    QuantityInStock,
                    SalePrice)
                VALUES
                    (?, ?, ?, ?, ?, ?)";
        
        //Exécution de la requête d'insertion
        $database->executeSql($sql, array(
            $post['Name'],
            $post['Photo'],
            $post['Description'],
            $post['Type'],
            $post['QuantityInStock'],
            $post['SalePrice']
        ));
    }

    //Méthode permettant de lister tous les articles de la base de donnée
    public function listAll() {
        //Appel de la classe Database
        $database = new Database();

        //Mise en place de la requête de sélection
        $sql = "SELECT
                    Id,
                    Name,
                    Photo,
                    Description,
                    Type,
                    QuantityInStock,
                    SalePrice
                FROM
                    products
                ORDER BY
                    Type";

        //Exécution de la requête de sélection
        $products = $database->query($sql);

        //Renvoie le résultat de la requête
        return $products;
    }

    //Méthode permettant de supprimer un article de la base de donnée
    public function deleteProduct($productId) {
        //Appel de la classe Database
        $database = new Database();

        //Mise en place de la requête de suppression
        $sql = "DELETE FROM
                    products
                WHERE
                    Id = ?";

        //Exécution de la requête de suppression
        $database->executeSql($sql, array(
            $productId
        ));
    }
}