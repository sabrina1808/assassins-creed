<?php

//Classe permettant de gérer les videos
class VideosModel {
    //Méthode permettant de faire la liste de toutes les videos présente sur le site
    public function listAllVideo() {
        //Appel de la classe Database
        $database = new Database();

        //Mise en place de la requête de sélection
        $sql = "SELECT
                    Id,
                    Jaquette,
                    Name,
                    Link
                FROM
                    videos";
        
        //Exécution de la requête
        $videos = $database->query($sql);

        //Renvoi le résultat de la requête
        return $videos;
    }

        //Méthode permettant de séléctionné une vidéo dans la base de donnée
        public function selectVideo($id) {
            //Appel de la classe Database
            $database = new Database();
    
            //Mise en place de la requête de sélection
            $sql = "SELECT
                        Id,
                        Name,
                        Link,
                        GamePage,
                        Poster
                    FROM
                        videos
                    WHERE
                        Id = ?";
    
            //Exécution de la requête
            $video = $database->queryOne($sql, array(
                $id)
            );
    
            //Renvoi du résultat de la requête
            return $video;
        }
}