<?php
//Ce fichier va permettre de définir et de gérer les liens absolu du site

//On modifie la valeur de l'option de configuration
ini_set('display_errors', 'on');
//On retourne toutes les erreurs PHP que l'on trouve
error_reporting(E_ALL);

//Classe permettant de gérer l'autoload des fichiers demandés lors des requêtes
class MyAutoload {
    //Méthode permettant de définir touts les liens absolus
    public static function start() {
        //Utilisation de la fonction spl_autoload_register pour enregister la liste des classes à chargé
        //On l'utilise sur notre classe en cours pour lancer autoload
        spl_autoload_register(array(__CLASS__, 'autoload'));

        //Définition des variables serveurs qui serviront pour la définition des liens absolus
        //DOCUMENT_ROOT permet de définir la racine sous laquelle le script est exécuté
        $root = $_SERVER['DOCUMENT_ROOT'];
        //HTTP_HOST permet de récupérer l'en-tête de la requête, si celle-ci existe
        $host = $_SERVER['HTTP_HOST'];

        //On définit des constantes pour les chemins absolu poir le serveur
        define('HOST', 'http://'.$host.'/Assassins-Creed/');
        define('ROOT', $root.'/Assassins-Creed/');

        //Définition des liens absolu pour les différents dossiers
        define('CONTROLLERS', ROOT.'Controllers/');
        define('MODELS', ROOT.'Models/');
        define('VIEWS', ROOT.'Views/');
        define('CLASSES', ROOT.'Classes/');

        define('ASSETS', HOST.'Assets/');
    }

    //Méthode permettant de charger les classes demandées par les fichiers
    public static function autoload($class) {
        //Si le fichier demandé existe dans le dossier Classes, on fait un include de ce fichier (on fait la même chose pour le dossier Models, le dossier Controlleur sera gére dans le Router pour prendre en compte les sous-dossiers)
        if(file_exists(CLASSES.$class.'.php')) {
            include_once CLASSES.$class.'.php';
        }
        else if(file_exists(MODELS.$class.'.php')) {
            include_once MODELS.$class.'.php';
        }
    }
}