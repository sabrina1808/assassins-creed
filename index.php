<?php

//Démarrage de la session
session_start();

//Inclusion du fichier config (qui contient tous les liens absolu)
include_once "Config/config.php";

//Appel de la méthode static start() de la classe MyAutoload pour permettre le chargement des classes nécessaire
MyAutoload::start();

//Si on ne passe pas de paramètre dans l'URL, on affiche la page d'accueil
if(!array_key_exists('r', $_GET)) {
    $request = "Home";
}
else {
    //Sinon on recupère le paramètre passé dans l'URL et on affiche la page demandé
    $request = $_GET['r'];
}

//Appel de la classe Router
$router = new Router($request);

//Exécution de la fonction permettant de faire appel au controller qui correspond à la demande $request
$router->renderController();