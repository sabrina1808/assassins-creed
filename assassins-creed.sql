-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 08 sep. 2020 à 15:03
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `assassins-creed`
--

-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `User_Id` int(11) NOT NULL,
  `TotalAmount` double DEFAULT NULL,
  `TaxRate` float DEFAULT NULL,
  `TaxAmount` double DEFAULT NULL,
  `CreationTimestamp` datetime DEFAULT NULL,
  `CompleteTimestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `User_Id` (`User_Id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `orders`
--

INSERT INTO `orders` (`Id`, `User_Id`, `TotalAmount`, `TaxRate`, `TaxAmount`, `CreationTimestamp`, `CompleteTimestamp`) VALUES
(44, 1, 96, 20, 19.2, '2020-05-20 16:34:52', '2020-05-20 16:34:52'),
(43, 1, 74, 20, 14.8, '2020-05-20 16:33:47', '2020-05-20 16:33:47'),
(42, 4, 62, 20, 12.4, '2020-05-20 10:06:34', '2020-05-20 10:06:34'),
(41, 1, 83, 20, 16.6, '2020-05-19 12:06:09', '2020-05-19 12:06:09'),
(40, 1, 92, 20, 18.4, '2020-05-19 12:03:51', '2020-05-19 12:03:51'),
(39, 1, 45, 20, 9, '2020-05-19 12:01:08', '2020-05-19 12:01:08');

-- --------------------------------------------------------

--
-- Structure de la table `ordersline`
--

DROP TABLE IF EXISTS `ordersline`;
CREATE TABLE IF NOT EXISTS `ordersline` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `QuantityOrdered` int(4) NOT NULL,
  `Product_Id` int(11) NOT NULL,
  `Order_Id` int(11) NOT NULL,
  `PriceEach` double NOT NULL,
  `Size` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Product_Id` (`Product_Id`),
  KEY `Order_Id` (`Order_Id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `ordersline`
--

INSERT INTO `ordersline` (`Id`, `QuantityOrdered`, `Product_Id`, `Order_Id`, `PriceEach`, `Size`) VALUES
(9, 1, 3, 41, 70, '/'),
(8, 1, 6, 41, 13, '/'),
(7, 1, 3, 40, 70, '/'),
(6, 1, 11, 40, 22, 'M'),
(5, 1, 9, 39, 45, '/'),
(10, 1, 11, 42, 22, 'M'),
(11, 1, 10, 42, 40, '/'),
(12, 2, 11, 43, 22, 'M'),
(13, 2, 12, 43, 15, 'M'),
(14, 2, 11, 44, 22, 'M'),
(15, 2, 12, 44, 15, 'M'),
(16, 1, 11, 44, 22, 'L');

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `Photo` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `Description` text COLLATE utf8_unicode_ci NOT NULL,
  `Type` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `QuantityInStock` tinyint(4) NOT NULL,
  `SalePrice` double NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `products`
--

INSERT INTO `products` (`Id`, `Name`, `Photo`, `Description`, `Type`, `QuantityInStock`, `SalePrice`) VALUES
(13, 'Figurine Alexios', 'Figurine Alexios.jpg', 'La figurine représente Alexios (un des deux personnages jouables d\'Assassin\'s Creed Odyssey) dans sa tenue complète comprenant un bouclier spartiate et l\'emblématique Lance Brisée de Léonidas. Alexios se tient sur un socle représentant une tête de méduse. Il est représenté dans une pose dynamique de combat. \r\nLes textures de la figurine sont fines. La figurine est en PVC et mesure environ 32 cm de hauteur.\r\nElle ravira tous les fans de la licence Assassin\'s Creed.', 'Figurine', 50, 50),
(14, 'Besace', 'Besace.jpg', 'Besace d\'Assassin\'s Creed avec le logo de la licence en rouge sur le rabat.\r\nCe sac est en coton avec une bandoulière réglable (dimension 23x27x8cm).\r\nIl possède des poches intérieures pour pourvoir rangés ses affaires. Il est donc idéal en cas de déplacement.\r\nC\'est un sac à avoir si vous êtes un fan.', 'Sac', 50, 20),
(3, 'Figurine d\'Altaïr', 'Figurine Altair.jpg', 'La figurine représente Altaïr (personnage jouable d\'Assassin\'s Creed I) dans sa tenue blanche d\'Assassin sur une cloche. Elle permet de voir Altaïr dans sa pose emblématique, prêt à sauter du haut de la cloche pour bondir sur sa proie, lame secrète prête à l\'emploi.\r\nLa figurine est en PVC et mesure environ 28 cm de hauteur.\r\nElle sera un cadeau parfait pour tous fans du légendaire Assassin qu\'est Altaïr.', 'Figurine', 50, 70),
(4, 'Figurine d\'Edward Kenway', 'Figurine Edward.jpg', 'La figurine représente Edaward Kenway (le personnage jouable d\'Assassin\'s Creed IV - Black Flag) sur le poste d\'observation de son navire, le JackDaw avec son emblématique drapeau de pirate décoré de l\'emblème des Assassins. Elle permet de voir Edward dans une pose très dynamique armé de son sabre et tenant la corde du mât, prêt à assaillir ses adversaires. Cette figurine souligne bien l\'esprit aventureux et conquérant d\'Edward.\r\nLa figurine est en PVC et mesure environ 45 cm de hauteur.\r\nElle représente le cadeau idéal pour tous fans de la licence Assassin\'s Creed.\r\n', 'Figurine', 50, 90),
(5, 'Figurine d\'Arno Dorian', 'Figurine Arno.jpg', 'La figurine représente Arno Dorian (le personnage jouable d\'Assassin\'s Creed Unity) posté devant des barricades épée à la main et pistolet caché derrière lui. Les emblèmes qu\'ils possèdent sur sa ceinture et sur sa capuche prouvent son appartenance à la Confrérie des Assassins.\r\nArno est représenté confiant et souriant sur cette figurine.\r\nElle est en PVC et mesure environ 24 cm de hauteur.\r\nElle sera à n\'en pas douté un cadeau très apprécié par les fans d\'Assassin\'s Creed.', 'Figurine', 50, 40),
(6, 'Mug ACIII', 'Mug AC III.jpg', 'Mug Assassin\'s Creed III sur lequel on peut voir Connor Kenway en position d\'attaque sur un soldat anglais sur une face et le logo d\'Assassin\'s Creed III en gris clair sur l\'autre face.\r\nLe mug est en céramique et possède une grande contenance (460 mL).', 'Mug', 50, 13),
(7, 'Mug AC Unity', 'Mug AC Unity.jpg', 'Mug d\'Assassin\'s Creed Unity sur lequel on peut voir le slogan \"La liberté ou la mort\" écrit en lettres rouge sang sur une face et Arno Dorian sur un fond bleu/blanc/rouge représentant le drapeau de la France sur l\'autre face.\r\nLe mug est en céramique et possède une contenance de 320 mL. ', 'Mug', 50, 10),
(8, 'Mug AC', 'Mug AC.jpg', 'Mug d\'Assassin\'s Creed représentant plusieurs des personnages principaux de la série (Altaïr Ibn La-Ahad, Connor Kenway, Ezio Auditore, Evye Frye, Jacob Frye, Edward Kenway, Aveline de Grandpré et Arno Dorian).\r\nLe mug est en céramique et possède une contenance de 330 mL.', 'Mug', 50, 10),
(9, 'Phantom Blade', 'Phantom Blade.jpg', 'Réplique grandeur nature de la lame fantôme d\'Arno Dorian dans Assassin\'s Creed Unity.\r\nCette réplique comprend la lame secrète et la mini arbalète rétractables en plastique finement sculpté ainsi que le gantelet en similicuir qui se fixer à l\'aide de sangles réglables.\r\nAffichez votre appartenance à la Confrérie des Assassins avec cette réplique de lame fantôme et complété ainsi votre look d\'Assassin.', 'Produit dérivé', 50, 45),
(10, 'Sac en bandoulière', 'Sac bandouliere.jpg', 'Sac en bandoulière noir et blanc avec un rabat sur lequel figure l\'emblème des Assassins.\r\nLe sac est en toile et similicuir avec une grande poche principale zippée, une poche à l\'avant et deux poches à l\'arrière ainsi qu\'une bandoulière réglable. Les dimensions du sac sont les suivantes : 32x12x33cm.\r\nLa conception du sac est telle qu\'il permet de réduire les charges sur l\'épaule et la colonne vertébrale.\r\nIl sera un sac idéal pour toutes personnes aimant la série Assassin\'s Creed.', 'Sac', 50, 40),
(11, 'T-shirt AC Blanc', 'T-shirt AC 2.jpg', 'T-shirt d\'Assassin\'s Creed blanc avec la devise des Assassins \"We work in the dark to serve the light\" (\"Nous travaillons dans l\'ombre pour servir la lumière\") en imprimé noir.\r\nLe t-shirt est 100% coton.', 'Vetements', 50, 22),
(12, 'T-shirt AC Noir', 'T-shirt AC.jpg', 'T-shirt d\'Assassin\'s Creed noir avec les divers logos de la licence en imprimé blanc.\r\nLe t-shirt est 100% coton.', 'Vetements', 50, 15);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `LastName` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `Address` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `City` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `ZipCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Country` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `Phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Role` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`Id`, `FirstName`, `LastName`, `Email`, `Password`, `Address`, `City`, `ZipCode`, `Country`, `Phone`, `Role`) VALUES
(1, 'Ezio', 'Auditore', 'test@test.com', '$2y$11$800ac710f9e7aa992e2c0en/3.ohjiyq.Leub4iovpDsF4ZThJeA.', '1 Place de la Paix', 'Epinay-sur-Seine', '93800', 'France', '0000000000', 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `videos`
--

DROP TABLE IF EXISTS `videos`;
CREATE TABLE IF NOT EXISTS `videos` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Jaquette` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `Link` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `GamePage` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `Poster` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `videos`
--

INSERT INTO `videos` (`Id`, `Jaquette`, `Name`, `Link`, `GamePage`, `Poster`) VALUES
(1, 'AC I Jaquette.jpg', 'Assassin\'s Creed I', 'Assassins Creed I Trailer de Lancement.mp4', 'AC-I', 'AC I - Fond Ecran.jpg'),
(2, 'AC II Jaquette.jpg', 'Assassin\'s Creed II', 'Assassins Creed II Trailer de Lancement.mp4', 'ACII-B-R', 'AC II - Fond Ecran.jpg'),
(3, 'AC Brotherhood Jaquette.jpg', 'Assassin\'s Creed Brotherhood', 'Assassins Creed Brotherhood Trailer de Lancement.mp4', 'ACII-B-R', 'AC Brotherhood - Fond Ecran.jpg'),
(4, 'AC Revelations Jaquette.jpg', 'Assassin\'s Creed Revelations', 'Assassins Creed Revelations Trailer de Lancement.mp4', 'ACII-B-R', 'AC Revelations - Fond Ecran.jpg'),
(5, 'AC III Jaquette.jpg', 'Assassin\'s Creed III', 'Assassins Creed III Trailer de Lancement.mp4', 'AC-III', 'AC III - Fond Ecran.jpg'),
(6, 'AC IV Jaquette.jpg', 'Assassin\'s Creed IV - Black Flag', 'Assassins Creed IV Black Flag Trailer de Lancement.mp4', 'AC-IV', 'AC IV Black Flag - Fond Ecran.jpg'),
(7, 'AC Rogue Jaquette.jpg', 'Assassin\'s Creed Rogue', 'Assassins Creed Rogue Trailer de Lancement.mp4', 'AC-Rogue', 'AC Rogue - Fond Ecran.jpg'),
(8, 'AC Unity Jaquette.jpg', 'Assassin\'s Creed Unity', 'Assassins Creed Unity Trailer de Lancement.mp4', 'AC-Unity', 'AC Unity - Fond Ecran.jpg'),
(9, 'AC Syndicate Jaquette.jpg', 'Assassin\'s Creed Syndicate', 'Assassins Creed Syndicate Trailer de Lancement.mp4', 'AC-Syndicate', 'AC Syndicate - Fond Ecran.jpg'),
(10, 'AC Origins Jaquette.jpg', 'Assassin\'s Creed Origins', 'Assassins Creed Origins Trailer de Lancement.mp4', 'AC-Origins', 'AC Origins - Fond Ecran.jpg'),
(11, 'AC Odyssey Jaquette.jpg', 'Assassin\'s Creed Odyssey', 'Assassins Creed Odyssey Trailer de Lancement.mp4', 'AC-Odyssey', 'AC Odyssey - Fond Ecran.jpg'),
(12, 'AC Valhalla Jaquette.jpg', 'Assassin\'s Creed Valhalla', 'Assassins Creed Valhalla Trailer de Lancement.mp4', 'AC-Valhalla', 'AC Valhalla - Fond Ecran');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
