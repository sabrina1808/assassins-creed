//Mode strict de Javascript
"use strict";

/* ----------------------------------------------------- */
/* ----------------------- DONNEES --------------------- */
/* ----------------------------------------------------- */
//Constante qui permet de gérer les images du slider grâce à des objets
const slides = [
    {image: "Assets/img/slides/AC I - Fond Ecran.jpg", lien: "Assassin's Creed I", href:"AC-I"},
    {image: "Assets/img/slides/AC II - Fond Ecran.jpg", lien: "Assassin's Creed II", href:"AC-II-B-R"},
    {image: "Assets/img/slides/AC Brotherhood - Fond Ecran.jpg", lien: "Assassin's Creed Brotherhood", href:"AC-II-B-R"},
    {image: "Assets/img/slides/AC Revelations - Fond Ecran.jpg", lien: "Assassin's Creed Révélations", href:"AC-II-B-R"},
    {image: "Assets/img/slides/AC III - Fond Ecran.jpg", lien: "Assassin's Creed III", href:"AC-III"},
    {image: "Assets/img/slides/AC IV Black Flag - Fond Ecran.jpg", lien: "Assassin's Creed IV Black Flag", href:"AC-IV"},
    {image: "Assets/img/slides/AC Rogue - Fond Ecran.jpg", lien: "Assassin's Creed Rogue", href:"AC-Rogue"},
    {image: "Assets/img/slides/AC Unity - Fond Ecran.jpg", lien: "Assassin's Creed Unity", href:"AC-Unity"},
    {image: "Assets/img/slides/AC Syndicate - Fond Ecran.jpg", lien: "Assassin's Creed Syndicate", href:"AC-Syndicate"},
    {image: "Assets/img/slides/AC Origins - Fond Ecran.jpg", lien: "Assassin's Creed Origins", href:"AC-Origins"},
    {image: "Assets/img/slides/AC Odyssey - Fond Ecran.jpg", lien: "Assassin's Creed Odyssey", href:"AC-Odyssey"},
    {image: "Assets/img/slides/AC Valhalla - Fond Ecran.jpg", lien: "Assassin's Creed Valhalla", href: "AC-Valhalla"}
];

//Création d'un objet qui va permettre de gérer l'état du slider
let state = {};

/* ----------------------------------------------------- */
/* ----------------------- FONCTIONS ------------------- */
/* ----------------------------------------------------- */
//Gestion du défilement de l'image vers la droite (affichage de l'image suivante)
function nextImage() {
    //On augmente l'index de l'image
    state.index++;

    //Si cet index est égal à la longueur du tableau slides, on revient au début (index = 0)
    if(state.index == slides.length) {
        state.index = 0;
    }

    //Appel de la fonction refreshSlider() pour changer l'image affiché sur le slider
    refreshSlider();
}

//Gestion de la mise en animation du slider
function play() {
    //Utilisation d'un setIntervalle qui va permettre de changer l'image affiché sur le slider au bout de 3 secondes
    state.timer = window.setInterval(nextImage, 3000);
}

//Gestion de l'image en cours du slider
function refreshSlider() {
    //Création de constantes qui vont permettre de mettre à jour l'image et les informations de la balise a
    const srcImage = document.querySelector("#slider img");
    const lienImage = document.querySelector("#slider a");

    //Mise à jour de la source de l'image
    srcImage.src = slides[state.index].image;
    //Mise à jour du alt de l'image
    srcImage.alt = slides[state.index].lien;
    //Mise à jour du lien avec le nom du jeu
    lienImage.textContent = slides[state.index].lien;
    //Mise à jour du title de la balise a en fonction du jeu
    lienImage.title = slides[state.index].lien;
    //Mise à jour du href de la balise avec le lien en fonction du jeu
    lienImage.href = slides[state.index].href;

    //Mise à jour de la puce qui est sélectionnée
    refreshPuces(state.index);
}

//Affichage des puces en-dessous du slider
//Mise en évidence de la puce qui correspond à la photo affiché
function refreshPuces(index) {
    //Déclaration d'une variable qui va permettre de trouvé la puce précédemment séléectionnée
    let previouslySelected = document.querySelector('.selected');

    //Si une puce était déjà sélectionnée, on la désélectionne
    if(previouslySelected != null){
        previouslySelected.classList.remove('selected');
    }

    //Et on sélectionne la nouvelle
    document.getElementById('dot-' + index).classList.add('selected');
}

//Ajoute les puces en-dessous du sliders en fonction du nombre de photos que celui-ci contient
function addPuces(element, index){
    //Déclaration des variables qui serviront pour la définition des puces, de la liste à puces et des liens
    let dot;
    let dotList;
    let link;

    //Sélection de l'élément <ul> qui contiendra la liste des puces
    dotList = document.querySelector('#dots');

    //Construction de l'élément <a> qui représentera la puce
    link = document.createElement("a");
    link.setAttribute('href', '#');

    //Gestionnaire d'événement au clic sur la puce
    link.addEventListener("click", function(e) {
        //Permet de na pas prendre en compte l'action par défaut du lien
        e.preventDefault();
        //Mise à jour de l'index
        state.index = index;
        //Mise à jour de l'image
        refreshSlider();
    });
    
    //Création de l'élément <li> qui contiendra le lien de la puce
    dot = document.createElement('li');
    //Ajout de la classe ".dot" aux éléments li
    dot.classList.add('dot');
    
    //Ajout d'un identifiant à la puce afin de pouvoir la sélectionner par la suite jouer avec l'index
    dot.id = "dot-" + index;
    
    //Insertion de la puce dans le code HTML
    dot.appendChild(link); //Les a sont des enfants de li
    dotList.appendChild(dot); //Les li sont des enfants de ul
}

/* ----------------------------------------------------- */
/* --------------------- CODE PRINCIPAL ---------------- */
/* ----------------------------------------------------- */
//Initialisation de l'index pour les images
state.index = 0;

//Initialisation du timer du slider
state.timer = null;

//Appel de la fonction pour animé le slider
play();

//Construction des puces avec une boucle forEach (cette boucle va permettre d'ajouter une puce pour chaque élément présent dans le tableau)
slides.forEach(addPuces);

//Appel de la fonction pour modifier l'image en cours du slider
refreshSlider();