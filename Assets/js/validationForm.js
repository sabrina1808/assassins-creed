//Mode strict de JavaScript
"use strict";

/* ----------------------------------------------------------------------------------------- */
/* ---------------------------------------- DONNÉES ---------------------------------------- */
/* ----------------------------------------------------------------------------------------- */
//Utilisation d'expression régulière pour la vérification des informations entrées dans les divers champs du formulaire
//Expression régulière qui servira pour la validation des champs tels que le nom  ou le prénom
const REGEX = /^[A-Za-zéèêïîÉÊËÏÎ\s-]+$/;
//Expression régulière qui servira pour la validation de l'addresse mail
const REGEXEMAIL = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//Expression régulière qui servira pour la validation des champs de texte (textarea ou addresse)
const REGEXTEXT = /^[A-Za-zéèêàçïîÉÊËÏÎ0-9\s-\.'"()&,]+$/;
//Expression régulière qui servira pour le validation des champs ne contenant que les chifffres (code postal ou numéro de téléphone)
const REGEXNUMBER = /^[0-9]+$/;

/* ----------------------------------------------------------------------------------------- */
/* --------------------------------------- FONCTIONS --------------------------------------- */
/* ----------------------------------------------------------------------------------------- */
//Définition de la fonction permettant de valider les inputs d'un formulaire
function validationInput() {
    //Définition d'une varaible form qui permet de sélectionné le formulaire sur la page
    let form = document.querySelector("form");
    //Définition d'une variable qui contiendra sous forme de tableau tous les inputs présents dans le formulaire
    let myInput = document.querySelectorAll('input');

    //Mise en place de la vérification du formulaire lors de sa soumission par l'utilisateur
    form.addEventListener('submit', function (e) {
        //Utilisation d'une boucle forEach sur la variable myInput pour permettre de faire une validation sur chacun d'eux
        myInput.forEach(input => {
            //Uilisation d'une condition switch sur le nom de l'input pour permettre des vérifacations différentes en fonction du champ séléctionné et de pouvoir affiché un message contenant l'erreur à côté de celui-ci
            switch(input.name){
                //Vérification de l'input FirstName (qui contient le prénom)
                case "FirstName":
                    //On vérifie que celui-ci n'est pas vide, car il est obligatoire
                    if(input.value.trim() == "") {
                        //On change le type de dispostion du message
                        $("#error-firstname").css("display", "inline-block");
                        //S'il est vide, on affiche un message d'erreur
                        $("#error-firstname").text("Le champ Prénom est obligatoire.");
                        //On empêche le formulaire d'être envoyé
                        e.preventDefault();
                    }
                    //On vérifie ensuite si le texte qu'il contient est validé par l'expression régulière définie dans les constantes
                    else if(!REGEX.test(input.value)) {
                        $("#error-firstname").css("display", "inline-block");
                        $("#error-firstname").text("Le champ Prénom ne doit pas contenir de chiffres ou de caractères spéciaux.");
                        e.preventDefault();
                    }
                    //On vérifie enfin si la longeur de cet input est supérieur à 20
                    else if(input.value.length > 20) {
                        $("#error-firstname").css("display", "inline-block");
                        $("#error-firstname").text("Le champ Prénom ne doit pas contenir plus de 20 caractères.");
                        e.preventDefault()
                    }
                    //Si on ne détecte pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
                    else {
                        $("#error-firstname").text("");
                    }
                    break;
                //Vérification de l'input LastName (qui contient le nom)
                case "LastName":
                    //On vérifie que celui-ci n'est pas vide, car il est obligatoire
                    if(input.value.trim() == "") {
                        $("#error-lastname").css("display", "inline-block");
                        $("#error-lastname").text("Le champ Nom est obligatoire.");
                        e.preventDefault();
                    }
                    //On vérifie ensuite si le texte qu'il contient est validé par l'expression régulière définie dans les constantes
                    else if(!REGEX.test(input.value)) {
                        $("#error-lastname").css("display", "inline-block");
                        $("#error-lastname").text("Le champ Nom ne doit pas contenir de chiffres ou de caractères spéciaux.");
                        e.preventDefault();
                    }
                    //On vérifie enfin si la longeur de cet input est supérieur à 20
                    else if(input.value.length > 20) {
                        $("#error-lastname").css("display", "inline-block");
                        $("#error-lastname").text("Le champ Nom ne doit pas contenir plus de 20 caractères.");
                        e.preventDefault()
                    }
                    //Si on ne détecte pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
                    else {
                        $("#error-lastname").text("");
                    }
                    break;
                //Vérification de l'input Address (qui contient l'addresse)
                case "Address":
                    //On vérifie que celui-ci n'est pas vide, car il est obligatoire
                    if(input.value.trim() == "") {
                        $("#error-address").css("display", "inline-block");
                        $("#error-address").text("Le champ Adresse est obligatoire.");
                        e.preventDefault();
                    }
                    //On vérifie ensuite si le texte qu'il contient est validé par l'expression régulière définie dans les constantes
                    else if(input.value != "" && !REGEXTEXT.test(input.value)) {
                        $("#error-address").css("display", "inline-block");
                        $("#error-address").text("Le champ Address n'est pas correct.");
                        e.preventDefault();
                    }
                    //Si on ne détecte pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
                    else {
                        $("#error-address").text("");
                    }
                    break;
                //Vérification de l'input City (qui contiendra la ville)
                case "City":
                    //On vérifie que celui-ci n'est pas vide, car il est obligatoire
                    if(input.value.trim() == "") {
                        $("#error-city").css("display", "inline-block");
                        $("#error-city").text("Le champ Ville est obligatoire.");
                        e.preventDefault();
                    }
                    //On vérifie ensuite si le texte qu'il contient est validé par l'expression régulière définie dans les constantes
                    else if(input.value != "" && !REGEX.test(input.value)) {
                        $("#error-city").css("display", "inline-block");
                        $("#error-city").text("Le champ Ville ne doit pas contenir de chiffres ou de caractères spéciaux.");
                        e.preventDefault();
                    }
                    //Si on ne détecte pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
                    else {
                        $("#error-city").text("");
                    }
                    break;
                //Vérification de l'input zipCode (qui contient le code postal)
                case "zipCode":
                    //On vérifie que celui-ci n'est pas vide, car il est obligatoire
                    if(input.value.trim() == "") {
                        $("#error-zipcode").css("display", "inline-block");
                        $("#error-zipcode").text("Le champ Code postal est obligatoire.");
                        e.preventDefault();
                    }
                    //On vérifie ensuite si le texte qu'il contient est validé par l'expression régulière définie dans les constantes
                    else if(!REGEXNUMBER.test(input.value)) {
                        $("#error-zipcode").css("display", "inline-block");
                        $("#error-zipcode").text("Le champ Code postal ne doit contenir que des chiffres.");
                        e.preventDefault();
                    }
                    //On vérifie enfin que la longeur de l'input est bien égale à 5
                    else if(input.value.length !== 5) {
                        $("#error-zipcode").css("display", "inline-block");
                        $("#error-zipcode").text("Le champ Code postal contenir 5 chiffres.");
                        e.preventDefault();
                    }
                    //Si on ne détecte pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
                    else {
                        $("#error-zipcode").text("");
                    }
                    break;
                //Vérification de l'input Country (qui contient le pays)
                case "Country":
                    //On vérifie que celui-ci n'est pas vide, car il est obligatoire
                    if(input.value.trim() == "") {
                        $("#error-country").css("display", "inline-block");
                        $("#error-country").text("Le champ Pays est obligatoire.");
                        e.preventDefault();
                    }
                    //On vérifie ensuite si le texte qu'il contient est validé par l'expression régulière définie dans les constantes
                    else if(!REGEX.test(input.value)) {
                        $("#error-country").css("display", "inline-block");
                        $("#error-country").text("Le champ Pays ne doit pas contenir de chiffres ou de caractères spéciaux.");
                        e.preventDefault();
                    }
                    //Si on ne détecte pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
                    else {
                        $("#error-country").text("");
                    }
                    break;
                //Vérification de l'input Phone (qui contient le numéro de téléphone)
                case "Phone":
                    //On vérifie si le texte qu'il contient est validé par l'expression régulière définie dans les constantes
                    if(input.value != "" && !REGEXNUMBER.test(input.value)) {
                        $("#error-phone").css("display", "inline-block");
                        $("#error-phone").text("Le champ Numéro de téléphone ne doit contenir que des chiffres.");
                        e.preventDefault();
                    }
                    //On vérifie ensuite que la longeur de l'input est bien égale à 10
                    else if(input.value != "" && input.value.length !== 10) {
                        $("#error-phone").css("display", "inline-block");
                        $("#error-phone").text("Le champ Numéro de téléphone doit contenir 10 chiffres.")
                        e.preventDefault();
                    }
                    //Si on ne détecte pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
                    else {
                        $("#error-phone").text("");
                    }
                    break;
                //Vérification de l'input Email (qui contient l'email)
                case "Email":
                    //On vérifie que celui-ci n'est pas vide, car il est obligatoire
                    if(input.value.trim() == "") {
                        $("#error-email").css("display", "inline-block");
                        $("#error-email").text("Le champ Email est obligatoire.");
                        e.preventDefault();
                    }
                    //On vérifie ensuite si le texte qu'il contient est validé par l'expression régulière définie dans les constantes
                    else if(!REGEXEMAIL.test(input.value)) {
                        $("#error-email").css("display", "inline-block");
                        $("#error-email").text("Le format de ce champ est incorrect. Merci de respecter un format de type \"exemple@exemple.com\".");
                        e.preventDefault();
                    }
                    //Si on ne détecte pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
                    else {
                        $("#error-email").text("");
                    }
                    break;
                //Vérification de l'input Password (qui contient le mot de passe)
                case "Password":
                    //On vérifie que celui-ci n'est pas vide, car il est obligatoire
                    if(input.value.trim() == "") {
                        $("#error-password").css("display", "inline-block");
                        $("#error-password").text("Le champ Mot de passe est obligatoire.");
                        e.preventDefault();
                    }
                    //Si on ne détecte pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
                    else {
                        $("#error-password").text("");
                    }
                    break;
                //Vérification de l'input Psw1 (qui contient le mot de passe)
                case "psw1":
                    //On vérifie que celui-ci n'est pas vide, car il est obligatoire
                    if(input.value.trim() === "") {
                        $("#error-psw1").css("display", "inline-block");
                        $("#error-psw1").text("Le champ Mot de passe est obligatoire.");
                        e.preventDefault();
                    }
                    //Si on ne détecte pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
                    else {
                        $("#error-psw1").text("");
                    }
                    break;
                //Vérification de l'input psw2 (qui contient la vérification du mot de passe)
                case "psw2":
                        //On vérifie que celui-ci n'est pas vide, car il est obligatoire
                        if(input.value.trim() == "") {
                            $("#error-psw2").css("display", "inline-block");
                            $("#error-psw2").text("Le champ Mot de passe est obligatoire.");
                            e.preventDefault();
                        }
                        //Si on ne détecte pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
                        else {
                            $("#error-psw2").text("");
                        }
                        break;
                //Vérification de l'input Name (qui contient le nom du produit)
                case "Name":
                    //On vérifie que celui-ci n'est pas vide, car il est obligatoire
                    if(input.value.trim() == "") {
                        $("#error-name").css("display", "inline-block");
                        $("#error-name").text("Le champ Nom est obligatoire.");
                        e.preventDefault();
                    }
                    //On vérifie ensuite si le texte qu'il contient est validé par l'expression régulière définie dans les constantes
                    else if(!Regex.test(input.value)) {
                        $("#error-name").css("display", "inline-block");
                        $("#error-name").text("Le champ Nom ne doit pas contenir de chiffres ou de caractères spéciaux.");
                        e.preventDefault();
                    }
                    //Si on ne détecté pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
                    else {
                        $("#error-name").text("");
                    }
                    break;
                //Vérification de l'input Photo (qui contient la photo du produit)
                case "Photo":
                    //On vérifie que celui-ci n'est pas vide, car il est obligatoire
                    if(input.value.trim() == "") {
                        $("#error-photo").css("display", "inline-block");
                        $("#error-photo").text("L'ajout d'une image est obligatoire.");
                        e.preventDefault();
                    }
                    //Si on ne détecté pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
                    else {
                        $("#error-photo").text("");
                    }
                    break;
                //Vérification de l'input QuantityInStock (qui renseigne sur la quantité de produit)
                case "QuantityInStock":
                    //On vérifie que celui-ci n'est pas vide, car il est obligatoire
                    if(input.value.trim() == "") {
                        $("#error-quantity").css("display", "inline-block");
                        $("#error-quantity").text("Ce champ Quantité est obligatoire.");
                        e.preventDefault();
                    }
                    //On vérifie ensuite si le texte qu'il contient est validé par l'expression régulière définie dans les constantes
                    else if(!RegexNumber.test(input.value)) {
                        $("#error-quantity").css("display", "inline-block");
                        $("#error-quantity").text("La quantité renseigné ne doit pas contenir que des chiffres.");
                        e.preventDefault();
                    }
                    //On vérifie enfin que la longeur de l'input n'est pas supérieur à 3
                    else if(input.value.length > 3) {
                        $("#error-quantity").css("display", "inline-block");
                        $("#error-quantity").text("La quantité renseigné ne doit pas contenir plus de 3 chiffres.");
                        e.preventDefault();
                    }
                    //Si on ne détecté pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
                    else {
                        $("#error-quantity").text("");
                    }
                    break;
                //Vérification de l'input SalePrice (qui renseigne sur le prix de vente)
                case "SalePrice":
                    //On vérifie que celui-ci n'est pas vide, car il est obligatoire
                    if(input.value.trim() == "") {
                        $("#error-price").css("display", "inline-block");
                        $("#error-price").text("Le champ Prix de vente est obligatoire.");
                        e.preventDefault();
                    }
                    //On vérifie ensuite si le texte qu'il contient est validé par l'expression régulière définie dans les constantes
                    else if(!RegexNumber.test(input.value)) {
                        $("#error-price").css("display", "inline-block");
                        $("#error-price").text("Le prix renseigné ne doit pas contenir que des chiffres.");
                        e.preventDefault();
                    }
                    //On vérifie enfin que la longeur de l'input n'est pas supérieur à 5
                    else if(input.value.length > 5) {
                        $("#error-price").css("display", "inline-block");
                        $("#error-price").text("Le prix renseigné ne doit pas contenir plus de 5 chiffres.");
                        e.preventDefault();
                    }
                    //Si on ne détecté pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
                    else {
                        $("#error-price").text("");
                    }
                    break;
            }
        });
    });
}

//Définition de la fonction permettant de valider les textarea du formulaire
function validationTextearea() {
    //Définition d'une varaible form qui permet de sélectionné le formulaire sur la page
    let form = document.querySelector("form");
    //Définition d'une variable qui contiendra le textarea présent dans le formulaire
    let textaera = document.querySelector('textarea');

    //Mise en place de la vérification du formulaire lors de sa soumission par l'utilisateur
    form.addEventListener('submit', function (e) {
        //On vérifie que celui-ci n'est pas vide, car il est obligatoire
        if(textaera.value.trim() == "") {
            $("#error-textarea").css("display", "inline-block");
            $("#error-textarea").text("Le champ de texte ne peut pas être vide.");
            e.preventDefault();
        }
        //On vérifie ensuite si le texte qu'il contient est validé par l'expression régulière définie dans les constantes
        else if(!REGEXTEXT.test(textaera.value)) {
            $("#error-textarea").css("display", "inline-block");
            $("#error-textarea").text("Votre message ne doit pas contenir de caractères spéciaux.");
            e.preventDefault();
        }
        //On vérifie enfin si la longeur de cet input est supérieur à 1000
        else if(textaera.value.length > 1000) {
            $("#error-textarea").css("display", "inline-block");
            $("#error-textarea").text("Merci de laisser un message qui contient moins de 1000 caractères.");
            e.preventDefault();
        }
        //Si on ne détecté pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
        else {
            $("#error-textarea").text("");
        }
    });
}

//Définition d'une fonction permettant de les select du formulaire
function validationSelect() {
    //Définition d'une varaible form qui permet de sélectionné le formulaire sur la page
    let form = document.querySelector("form");
    //Définition d'une variable qui contiendra le select présent dans le formulaire
    let select = document.querySelector("select");
    
    //Mise en place de la vérification du formulaire lors de sa soumission par l'utilisateur
    form.addEventListener('submit', function (e) {
        //On vérifie que celui-ci n'est pas vide, car il est obligatoire
        if(select.value === "/") {
            $("#error-select").css("display", "inline-block");
            $("#error-select").text("Merci de sélectionné une option dans la liste");
            e.preventDefault();
        }
        //Si on ne détecté pas d'erreur, on affiche aucune erreur et on n'empêche pas l'envoi du formulaire
        else {
            $("#error-select").text("");
        }
    });
}