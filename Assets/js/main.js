//Mode strict de Javascript
"use strict";

/* ----------------------------------------------------------------------------------------- */
/* ---------------------------------------- DONNÉES ---------------------------------------- */
/* ----------------------------------------------------------------------------------------- */
//Déclaration d'une variable qui va permettre de sélectionner l'élément avec l'id "sous-menu"
let sousMenu = document.getElementById("sous-menu");

//Appel de la classe Basket
let basket = new Basket();

/* ----------------------------------------------------------------------------------------- */
/* --------------------------------------- FONCTIONS --------------------------------------- */
/* ----------------------------------------------------------------------------------------- */
//Fonction permettant de faire apparaitre le sous-menu
function showSousMenu() {
    //Changement du style d'affichage de l'élément sousMenu de "none" à "block"
    sousMenu.style.display = "block";
}

//Fonction permettant de revenir en haut de la page au clic de la double-flèche
$(document).ready(function() {
    $('scroll-up').click(function(event) {
        event.preventDefault();
        $('html', 'body').animate({scrollTop: 0});
        return false;
    });
});

/* ---------------------------------------------------------------------------------------- */
/* ----------------------------------- CODE PRINCIPAL ------------------------------------- */
/* ---------------------------------------------------------------------------------------- */
//Gestionnaire d'évenements au click des boutons
//Évenement permettant de faire apparaître le sous-menu en cliquant sur le lien "Serie de jeux"
$('.deroulant').on("click", showSousMenu);

//Si on se trouve sur une autre page que celle de la boutique (car les erreurs du formulaire de la boutique sont gérées ci-dessous), on analyse l'analyse pour détecté la présence de formulaire
if(window.location.href.indexOf('/Shop') == -1) {
    //Si on détecté la présence d'un formulaire dans la page, on effectue la vérifaction de celui-ci
    if(document.querySelector("form")) {
        //Appel de la fonction permettant de valider les inputs
        validationInput();
    }

    //Si on détecte la présence d'une zone de texte, on effectue également sa vérification
    if(document.querySelector("textarea")) {
        //Appel de la fonction permettant de valider les texterea
        validationTextearea();
    }

    //Si on déctecte la présence d'un select dans le formulaire, on effectue également sa vérification
    if(document.querySelector("select")) {
        //Appel de la fonction permettant de valider les select
        validationSelect();
    }
}

//Évenement permettant d'ajouter des articles au panier
//Si on se trouve sur la page de la boutique, on affiche uniquement le nombre d'article dans le panier
if(window.location.href.indexOf('/Shop') != -1) {
    //Ajout de l'article dans le panier au click du bouton "Ajouter au panier"
    $(".addBasket").on("click", function (e) {
        //Déclaration des variables pour les articles
        let id = $(this).data('id');
        let name = $(this).data('name');
        let type = $(this).data('type');
        let size = $("#size-"+ id +" option:selected").val();
        //Si le type de produits est différents de "Vetements" et que le taille est égale à "undefined", on affecte la valeur "/" à la variable size
        if(type != "Vetements" && size == undefined) {
            size = "/";
        }
        let quantity = $("#quantity-"+id).val();
        let price = $(this).data('price');
        //si le type de produit est un "Vetements", on vérifie que l'utilisateur a bien choisie une taille, que la quantité entrée est un nombre, à l'aide de la méthode isNaN, et qu'il est supérieur à 0
        if(type == "Vetements" && size == "/") {
            //On change le type de dispostion du message
            $("#alert-select-"+id).css("display", "inline-block");
            //On affiche le message d'erreur
            $("#alert-select-"+id).text("Merci de choisir une taille.");
            //On empêche le formulaire d'être validé
            e.preventDefault();
        }
        else if(isNaN(quantity)) {
            $("#alert-quantity-"+id).css("display", "inline-block");
            $("#alert-quantity-"+id).text("La quantité doit être un nombre.");
            e.preventDefault();
        }
        else if(quantity <= 0) {
            $("#alert-quantity-"+id).css("display", "inline-block");
            $("#alert-quantity-"+id).text("La quantité minimale doit être de 1.");
            e.preventDefault();
        }
        else {
            //Exécution de la méthode permettant l'ajout au panier avec les paramètres ci-dessus
            basket.addToBasket(id, name, type, size, quantity, price);
        }
    });
}

//Si on se trouve sur la page de récapitulatif du panier, on affiche le contenu du panier
if(window.location.href.indexOf('/Basket') != -1) {
    //Affichage du contenu du panier
    basket.displayCompleteBasket();
    //Ajout de la possibilité de supprimer un article du panier en cliquant sur la corbeille
    $(document).on("click", ".fa-trash", function (e) {
        //Annulation du comportement par défaut du navigateur
        e.preventDefault();
        //Déclaration d'une variable qui permettra de récupérer l'id de l'article que l'on souhaite supprimé
        let id = $(this).data('index');
        //Exécution de la méthode permettant de supprimé l'article
        basket.removeToBasket(id);
    });
}

//Si on se trouve sur la page de validation de la commande, on supprime le contenu du panier dans le localStorage
if(window.location.href.indexOf('/Validation') != -1) {
    //Exécution de la méthode permettant de supprimer le contenu du panier présent dans le localStorage
    basket.clearBasket();
}