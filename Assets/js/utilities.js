//Mode strict de JavaScript
"user strict";

/* ----------------------------------------------------------------------------------------- */
/* --------------------------------------- FONCTIONS --------------------------------------- */
/* ----------------------------------------------------------------------------------------- */
//Fonction permettant de récupérer des informations dans le localStorage
function loadDataFromLocalStorage(name) {
    //Déclaration d'une variable qui permettra de récupérer la valeur souhaitée dans le localStorage
    let jsonData;
    //Récupération de l'information souhaitée dans le localStorage
    jsonData = window.localStorage.getItem(name);
    //On renvoie les données du localStorage après les avoir désérialisée
    return JSON.parse(jsonData);
}

//Fonction permettant de sauvegarder des informations dans le localStorage
function saveDataFromLocalStorage(name, data) {
    //Déclaration d'une variable qui permettra de sauvegarder les informations souhaitées dans le localStorage
    let jsonData;
    //Transformations des informations en format JSON avec la sérialisation
    jsonData = JSON.stringify(data);
    //Enregistrement des données dans le localStorage en indiquant la clé de sauvegarde (nom qui permettera de récupérer uniquement les informations contenus dans celle-ci)
    window.localStorage.setItem(name, jsonData);
}