//Classe permettant de gérer les actions du panier
class Basket {
    constructor() {
        //Chargement des articles du panier avec la méthode loadBasket
        this.items = this.loadBasket();
        //Affichage du nombre d'élément dans le panier avec la méthode displayItemsBasket
        this.displayItemsBasket();
    }

    //Méthode permettant d'ajouter un article au panier
    addToBasket(productId, name, type, size, quantity, price) {
        //Conversion des valeurs en nombres pour la quantité et le prix de vente
        productId = parseInt(productId);
        quantity = parseInt(quantity);
        price = parseFloat(price);

        //Déclaration de l'objet item qui contiendra toutes les informations lié à l'article
        let item = {
            productId: productId,
            name: name,
            type: type,
            size: size,
            quantity: quantity,
            price: price
        };

        //Utilisation d'une boucle pour parcourir le tableau items qui va permettre de cherché l'objet spécifié
        for(let index = 0; index < this.items.length; index++) {
            //Si l'index du tableau correspondant à l'item est trouvé, on met à jour la quantité commandée pour cet article
            if(this.items[index].productId == productId) {
                //Si la taille du produit est identique à celle qui se trouve déjà dans le panier, on met à jour la quantité sinon, on ajoute une nouvelle ligne au tableau avec la nouvelle taille
                if(this.items[index].size == size) {
                    this.items[index].quantity += quantity;
                    //On sauvegarde le contenu du panier
                    this.saveBasket();
                    return true;
                }
            }
        }

        //Mise à jour du tableau items avec les informations contenu dans l'objet item
        this.items.push(item);
        //Sauvegarde du contenu du panier
        this.saveBasket();
    }

    //Méthode permettant de faire apparaître le nombre d'éléments dans le panier
    displayItemsBasket() {
        //Si au moins un élément est présent dans le panier, on affiche ce nombre sur l'icône du panier
        if(this.items.length > 0) {
            $("#basket-items").removeClass('hide');
            $("#basket-items").text(this.items.length);
        }
        else {
            //Sinon, on affiche rien du tout
            $("#basket-items").addClass('hide');
            $("#basket-items").text('');
        }
    }

    //Méthode permettant d'afficher le contenu du panier
    displayCompleteBasket() {
        //Déclaration d'une variable qui permettra d'afficher le prix total de la commande et initialisation du montant
        let totalPrice = 0;

        //Si le tableau items contient des éléments, on les affiche dans le tableau de la page shop.phtml
        if(this.items.length > 0) {
            //On vide le contenu du tableau
            $(".basket-table tbody").empty();

            //Utilisation d'une boucle pour parcourir le tableau items et afficher les informations dans le html
            for(let index = 0; index < this.items.length; index++) {
                //Mise à jour du prix total de la commande
                totalPrice += parseFloat(this.items[index].quantity)*parseFloat(this.items[index].price);
                //Création d'une  ligne pour le tableau
                let tr = $("<tr>");
                //Ajout des informations que l'on souhaite dans cette ligne du tableau
                tr.append(
                    "<td>"+this.items[index].name+"</td>"+
                    "<td>"+this.items[index].size+"</td>"+
                    "<td>"+this.items[index].quantity+"</td>"+
                    "<td>"+this.items[index].price.toFixed(2)+" €</td>"+
                    "<td>"+(parseFloat(this.items[index].quantity)*parseFloat(this.items[index].price)).toFixed(2)+" €</td>"+
                    "<td><button class='remove' data-index='"+index+"'><i class='fa fa-trash'></i></button></td>"
                );
                //Ajout des lignes créer au tableau
                $(".basket-table tbody").append(tr);
            }

            //Mise à jour du prix total de la commande
            $("#totalPrice").text(totalPrice.toFixed(2));

            //Mise à jour du panier et chargement de son contenu dans l'input "#basketItems"
            this.loadBasketInInput("#basketItems");
            
            //Vérification du contenu de l'input "#basketItems"
            let basketItems = $('form #basketItems').val();
            console.log("basketItems", basketItems);
        }
        else {
            $(".basket-table tbody").html("<p>Votre panier est vide....</p>");
            $(".basket-form").css("display", "none");
        }
    }

    //Méthode permettant de récupére le contenu du panier à partir du localStorage
    loadBasket() {
        //Déclaration du tableau items qui contient tous les articles séléctionnés
        let items = loadDataFromLocalStorage('basketAC');

        //Si le tableau ne contient aucune donnée, on défini un tableau vide
        if(items == null) {
            items = [];
        }

        //On renvoie le tableau items
        return items;
    }

    //Méthode permettant de sauvegardé le contenu du panier dans le localStorage
    saveBasket() {
        //Sauvegarde du contenu dans le localStorage
        saveDataFromLocalStorage("basketAC", this.items);
        
        //Affichage du nombre de produit dans le panier
        this.displayItemsBasket();

        //Si on se trouve sur la page de récapitulatif du panier, on affiche le panier complet
        if(window.location.href.indexOf('/Basket') != -1) {
            this.displayCompleteBasket();
        }
    }

    //Méthode permettant de supprimer un article du panier
    removeToBasket(id) {
        //Utilisation de la méthode splice pour supprimé l'article sélectionné du panier
        this.items.splice(id, 1);

        //Chargement du panier
        let basket = this.loadBasket();
        //Si le panier est vide, on remet le montant total à zéro
        if(this.basket == null) {
            $("#totalPrice").text("0.00");
        }
        
        //Sauvegarde du panier
        this.saveBasket();
    }

    //Méthode permettant de supprimer le contenu du panier dans le localStorage
    clearBasket() {
        //Suppression du contenu du tableau items
        this.items = [];
        //Sauvegarde du panier vide dans le localStorage
        this.saveBasket();
    }

    //Fonction permettant de rafraîchir le panier et de charger les éléments de celui-ci dans un élément du DOM
    loadBasketInInput(element) {
        //Transformation en format JSON du contenu du panier
        $(element).val(JSON.stringify(this.items));
    }
}